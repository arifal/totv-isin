/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin;

import com.etradingsoftware.symbology.totvisin.app.TotvIsinApplication;
import com.etradingsoftware.symbology.totvisin.common.util.ExecutorServiceUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.ServerConfig;
import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {

    private static Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private static TotvIsinApplication totvIsinApp;
    private static Server jettyServer;
    private static ExecutorService executorService;

    private static volatile boolean stop;

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Illegal number of arguments. Usage: <configFile>");
            System.exit(1);
        }

        String configPath = args[0];

        ServerConfig config;
        try {
            LOGGER.info("Started.");

            config = new ServerConfig(configPath);
            totvIsinApp = new TotvIsinApplication(configPath);

            executorService = Executors.newFixedThreadPool(1);
            executorService.execute(totvIsinApp);

            jettyServer = new Server(config.getServerPort());
            jettyServer.start();

            while (!stop) {
                HelperUtil.sleep(100);
                //totvIsinApp.stop();
                //break;
            }
        } catch (Exception e) {
            LOGGER.error("Fatal error - {}", e.getMessage(), e);
        } finally {
            cleanUp();
            LOGGER.info("Exited.");
        }
    }

    private static void stop() {
        stop = true;
    }

    private static void cleanUp() {
        if (jettyServer != null) {
            try {
                jettyServer.stop();
            } catch (Exception e) {
                /* do nothing */
            }
        }

        if (totvIsinApp != null) {
            totvIsinApp.stop();
        }

        ExecutorServiceUtil.shutdownAndWait(executorService, LOGGER);
    }

}
