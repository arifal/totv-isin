/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.config;

import com.etradingsoftware.symbology.totvisin.common.config.TotvIsinConfig;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerConfig extends TotvIsinConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerConfig.class);

    private static final String TOTV_ISIN_SERVER_PORT = "totv-isin.server.port";

    static {
        addInt(TOTV_ISIN_SERVER_PORT, true);
    }

    public ServerConfig(String configPath) throws ConfigurationException {
        LOGGER.info("Loading server config file: {}", configPath);
        loadFromConfigFile(configPath);
    }

    public int getServerPort() {
        return getInt(TOTV_ISIN_SERVER_PORT);
    }

}
