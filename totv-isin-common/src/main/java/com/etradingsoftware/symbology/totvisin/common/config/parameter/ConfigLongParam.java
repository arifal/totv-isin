/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.config.parameter;

import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;

import java.text.MessageFormat;
import java.util.Properties;

public class ConfigLongParam implements ConfigParam {

    private String key;
    private long value;
    private boolean required;
    private Object defaultValue;

    public ConfigLongParam(String key, boolean required, Object defaultValue) {
        this.key = key;
        this.required = required;
        this.defaultValue = defaultValue;
    }

    public ConfigLongParam(String key, boolean required) {
        this(key, required, null);
    }

    public String getKey() {
        return key;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public void loadFromProperties(Properties prop) throws ConfigurationException {
        String value;
        if (defaultValue == null) {
            value = prop.getProperty(key);
        } else {
            value = prop.getProperty(key, String.valueOf(defaultValue));
        }

        if (required && value == null) {
            throw new ConfigurationException(MessageFormat.format("Property ''{0}'' is missing from the config file", key));
        }

        try {
            this.value = Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new ConfigurationException(MessageFormat.format("Unable to parse property ''{0}'' value ''{1}''", key, value), e);
        }
    }

}
