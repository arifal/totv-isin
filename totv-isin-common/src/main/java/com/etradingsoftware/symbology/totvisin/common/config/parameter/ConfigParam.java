/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.config.parameter;

import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;

import java.util.Properties;

public interface ConfigParam {

    void loadFromProperties(Properties properties) throws ConfigurationException;

    String getKey();

    Object getValue();

}
