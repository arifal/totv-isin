/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.util;

import java.text.MessageFormat;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class HelperUtil {

    public static void sleep(long timeout) {
        try {
            TimeUnit.MILLISECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            /* do nothing */
        }
    }

    public static List<String> csvToList(String commaSeparatedValues) {
        return Arrays.asList(commaSeparatedValues.split(","));
    }

    /**
     * Parse time string HH:mm as LocalTime
     * @param time Time string in HH:mm format
     * @return LocalTime
     */
    public static LocalTime toLocalTime(String time) {
        try {
            String[] timeParts = time.split(":");
            return LocalTime.of(Integer.parseInt(timeParts[0]), Integer.parseInt(timeParts[1]));
        } catch (Exception e) {
            String errorMessage = MessageFormat.format("Time {0} is not a valid format (format: HH:mm)", time);
            throw new RuntimeException(errorMessage);
        }
    }

}
