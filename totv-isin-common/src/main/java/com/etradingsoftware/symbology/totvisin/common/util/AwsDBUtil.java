package com.etradingsoftware.symbology.totvisin.common.util;

import com.amazonaws.services.applicationautoscaling.AWSApplicationAutoScaling;
import com.amazonaws.services.applicationautoscaling.model.*;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.UpdateTableRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

public final class AwsDBUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwsDBUtil.class);

    private static final ServiceNamespace DB_NAMESPACE = ServiceNamespace.Dynamodb;

    public static void registerScalableTarget(String tableName, ScalableDimension scalableDimension, int minCapacityUnits, int maxCapacityUnits, String roleArn, AWSApplicationAutoScaling client) {
        RegisterScalableTargetRequest rstRequest = new RegisterScalableTargetRequest()
                .withServiceNamespace(DB_NAMESPACE)
                .withResourceId(toResourceID(tableName))
                .withScalableDimension(scalableDimension)
                .withMinCapacity(minCapacityUnits)
                .withMaxCapacity(maxCapacityUnits)
                .withRoleARN(roleArn)
                ;
        client.registerScalableTarget(rstRequest);
    }

    public static void deregisterScalableTarget(String tableName, ScalableDimension scalableDimension, AWSApplicationAutoScaling client) {
        DeregisterScalableTargetRequest deregisterScalableTargetRequest = new DeregisterScalableTargetRequest()
                .withServiceNamespace(DB_NAMESPACE)
                .withScalableDimension(scalableDimension)
                .withResourceId(toResourceID(tableName))
                ;
        client.deregisterScalableTarget(deregisterScalableTargetRequest);
    }

    public static void configureScalingPolicy(String tableName, ScalableDimension scalableDimension, MetricType metricType, double targetUtilization, int scaleInCooldown, int scaleOutCooldown, AWSApplicationAutoScaling client) {
        TargetTrackingScalingPolicyConfiguration scalingPolicyConfig = new TargetTrackingScalingPolicyConfiguration()
                .withPredefinedMetricSpecification(new PredefinedMetricSpecification()
                        .withPredefinedMetricType(metricType)
                )
                .withTargetValue(targetUtilization)
                .withScaleInCooldown(scaleInCooldown)
                .withScaleOutCooldown(scaleOutCooldown)
                ;

        PutScalingPolicyRequest pspRequest = new PutScalingPolicyRequest()
                .withServiceNamespace(DB_NAMESPACE)
                .withScalableDimension(scalableDimension)
                .withResourceId(toResourceID(tableName))
                .withPolicyName(generateScalingPolicyName(tableName, scalableDimension))
                .withPolicyType(PolicyType.TargetTrackingScaling)
                .withTargetTrackingScalingPolicyConfiguration(scalingPolicyConfig)
                ;
        client.putScalingPolicy(pspRequest);
    }

    public static void deleteScalingPolicy(String tableName, ScalableDimension scalableDimension, AWSApplicationAutoScaling client) {
        DeleteScalingPolicyRequest deleteScalingPolicyRequest = new DeleteScalingPolicyRequest()
                .withServiceNamespace(DB_NAMESPACE)
                .withScalableDimension(scalableDimension)
                .withResourceId(toResourceID(tableName))
                .withPolicyName(generateScalingPolicyName(tableName, scalableDimension))
                ;
        client.deleteScalingPolicy(deleteScalingPolicyRequest);
    }

    public static void updateTableCapacityUnits(String tableName, long readCapacityUnits, long writeCapacityUnits, AmazonDynamoDB client) {
        UpdateTableRequest request = new UpdateTableRequest(
                tableName
                , new ProvisionedThroughput(readCapacityUnits, writeCapacityUnits)
        );
        client.updateTable(request);
    }

    public static void describeScalable(String tableName, AmazonDynamoDB amazonDynamoDB, AWSApplicationAutoScaling appAutoScalingClient, AmazonCloudWatch amazonCloudWatchClient) {
        LOGGER.info("Table {} DescribeScalableTargets  (WCU) result: {}", tableName, describeScalableTargets(tableName, ScalableDimension.DynamodbTableWriteCapacityUnits, appAutoScalingClient));
        LOGGER.info("Table {} DescribeScalableTargets  (RCU) result: {}", tableName, describeScalableTargets(tableName, ScalableDimension.DynamodbTableReadCapacityUnits, appAutoScalingClient));
        LOGGER.info("Table {} DescribeScalablePolicies (WCU) result: {}", tableName, describeScalingPolicies(tableName, ScalableDimension.DynamodbTableWriteCapacityUnits, appAutoScalingClient));
        LOGGER.info("Table {} DescribeScalablePolicies (RCU) result: {}", tableName, describeScalingPolicies(tableName, ScalableDimension.DynamodbTableReadCapacityUnits, appAutoScalingClient));
        LOGGER.info("Table {} DescribeAlarms result: {}", tableName, amazonCloudWatchClient.describeAlarms());
        LOGGER.info("Table {} DescribeTable result: {}", tableName, amazonDynamoDB.describeTable(tableName));
    }

    public static void showTableAutoscalingInfo(String tableName, AmazonDynamoDB client) {
        DescribeTableResult result = client.describeTable(tableName);
        LOGGER.info("Table: {}, RCU: {}, WCU: {}"
                , tableName
                , result.getTable().getProvisionedThroughput().getReadCapacityUnits()
                , result.getTable().getProvisionedThroughput().getWriteCapacityUnits()
        );
    }

    public static void listAllTableCapacityUnits(AmazonDynamoDB amazonDynamoDB) {
        ListTablesResult result = amazonDynamoDB.listTables();
        for (String tableName : result.getTableNames()) {
            DescribeTableResult describeTableResult = amazonDynamoDB.describeTable(tableName);
            LOGGER.info("Table: {}, RCU: {}, WCU: {}"
                    , tableName
                    , describeTableResult.getTable().getProvisionedThroughput().getReadCapacityUnits()
                    , describeTableResult.getTable().getProvisionedThroughput().getWriteCapacityUnits()
            );
        }
    }

    public static void listTableCapacityUnits(String tableName, AmazonDynamoDB client) {
        DescribeTableResult describeTableResult = client.describeTable(tableName);
        LOGGER.info("Table: {}, RCU: {}, WCU: {}, ItemCount: {}"
                , tableName
                , describeTableResult.getTable().getProvisionedThroughput().getReadCapacityUnits()
                , describeTableResult.getTable().getProvisionedThroughput().getWriteCapacityUnits()
                , describeTableResult.getTable().getItemCount()
        );
    }

    public static DescribeScalableTargetsResult describeScalableTargets(String tableName, ScalableDimension scalableDimension, AWSApplicationAutoScaling client) {
        DescribeScalableTargetsRequest request = new DescribeScalableTargetsRequest()
                .withServiceNamespace(DB_NAMESPACE)
                .withScalableDimension(scalableDimension)
                .withResourceIds(toResourceID(tableName))
                ;
        return client.describeScalableTargets(request);
    }

    public static DescribeScalingPoliciesResult describeScalingPolicies(String tableName, ScalableDimension scalableDimension, AWSApplicationAutoScaling client) {
        DescribeScalingPoliciesRequest request = new DescribeScalingPoliciesRequest()
                .withServiceNamespace(DB_NAMESPACE)
                .withScalableDimension(scalableDimension)
                .withResourceId(toResourceID(tableName));
        return client.describeScalingPolicies(request);
    }

    private static String generateScalingPolicyName(String tableName, ScalableDimension scalableDimension) {
        switch (scalableDimension) {
            case DynamodbTableWriteCapacityUnits:
                return generateWcuScalingPolicyName(tableName);
            case DynamodbTableReadCapacityUnits:
                return generateRcuScalingPolicyName(tableName);
            default:
                throw new RuntimeException(MessageFormat.format("Unsupported scalable dimension {0}", scalableDimension));
        }
    }

    private static String generateWcuScalingPolicyName(String tableName) {
        return MessageFormat.format("{0}_WCU_SCALING_POLICY", tableName.toUpperCase());
    }

    private static String generateRcuScalingPolicyName(String tableName) {
        return MessageFormat.format("{0}_RCU_SCALING_POLICY", tableName.toUpperCase());
    }

    private static String toResourceID(String tableName) {
        return MessageFormat.format("table/{0}", tableName);
    }

}
