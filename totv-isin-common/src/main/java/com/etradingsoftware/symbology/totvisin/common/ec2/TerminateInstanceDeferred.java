package com.etradingsoftware.symbology.totvisin.common.ec2;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;

public class TerminateInstanceDeferred implements Runnable {

    private AmazonAutoScaling amazonAutoScalingClient;
    private String autoscalingGroupName;
    private String instanceId;
    private boolean shouldDecrementDesiredCapacity;
    private long executeAfterMillis;

    public TerminateInstanceDeferred(AmazonAutoScaling amazonAutoScalingClient, String autoscalingGroupName, String instanceId, boolean shouldDecrementDesiredCapacity, long executeAfterMillis) {
        this.amazonAutoScalingClient = amazonAutoScalingClient;
        this.autoscalingGroupName = autoscalingGroupName;
        this.instanceId = instanceId;
        this.shouldDecrementDesiredCapacity = shouldDecrementDesiredCapacity;
        this.executeAfterMillis = executeAfterMillis;
    }

    @Override
    public void run() {
        HelperUtil.sleep(executeAfterMillis);
        AwsAutoScalingUtil.setAutoScalingInstanceProtection(autoscalingGroupName, instanceId, false, amazonAutoScalingClient);
        AwsAutoScalingUtil.terminateAutoScalingInstance(instanceId, shouldDecrementDesiredCapacity, amazonAutoScalingClient);
    }

}
