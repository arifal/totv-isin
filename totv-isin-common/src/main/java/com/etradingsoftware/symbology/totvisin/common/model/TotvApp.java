package com.etradingsoftware.symbology.totvisin.common.model;

public enum TotvApp {

    SETUP(0, "Setup")
    , CUCKOO_MAN(1, "cuckooman")
    , GHOST(2, "ghost")
    , TOTO(3, "toto")
    ;

    private int index;
    private String name;

    TotvApp(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
