package com.etradingsoftware.symbology.totvisin.common.util;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.identitymanagement.model.GetRoleResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AwsRoleUtil {

     private static final Logger LOGGER = LoggerFactory.getLogger(AwsAutoScalingUtil.class);

     public static GetRoleResult getRole(String roleName, AmazonIdentityManagement client) {
          GetRoleRequest request = new GetRoleRequest();
          request.setRoleName(roleName);
          return client.getRole(request);
     }

     public static ListAttachedRolePoliciesResult listAttachedRolePolicies(String roleName, AmazonIdentityManagement client) {
          ListAttachedRolePoliciesRequest listAttachedRolePoliciesRequest = new ListAttachedRolePoliciesRequest();
          listAttachedRolePoliciesRequest.setRoleName(roleName);
          return client.listAttachedRolePolicies(listAttachedRolePoliciesRequest);
     }

}
