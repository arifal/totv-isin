/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.util;

import org.slf4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

public final class ExecutorServiceUtil {

    public static void shutdownAndWait(ExecutorService executorService, Logger logger) {
        if (executorService == null) {
            return;
        }

        executorService.shutdown();
        while (!executorService.isTerminated()) {
            int activeThreads = ((ThreadPoolExecutor) executorService).getActiveCount();
            int queuedTasks = ((ThreadPoolExecutor) executorService).getQueue().size();
            if ((activeThreads + queuedTasks) > 0) {
                logger.info("Waiting for all tasks to be completed (activeThreads: {}, queuedTasks: {})...", activeThreads, queuedTasks);
                HelperUtil.sleep(5000);
            }
        }
    }

}
