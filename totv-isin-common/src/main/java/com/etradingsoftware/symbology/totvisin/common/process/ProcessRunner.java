/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.process;

import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.common.util.ProcessUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ProcessRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessRunner.class);

    private static final String PROCESS_COMMAND_PATTERN = "^\\s*DIRECTORY\\s*=\\s*(?<workingDirectory>.*?)\\s*,\\s*COMMAND\\s*=\\s*(?<command>.*?)\\s*$";
    private static final String PROCESS_PARAM_PATTERN = "(%param(?<paramindex>\\d+)%)";
    private static final String WORKING_DIRECTORY = "workingDirectory";
    private static final String COMMAND = "command";
    private static final String PARAM_INDEX = "paramindex";

    private String taskFilePath;
    private Pattern taskCommandPattern;
    private Pattern taskParamPattern;

    public ProcessRunner(String taskFilePath) {
        this.taskFilePath = taskFilePath;
        this.taskCommandPattern = Pattern.compile(PROCESS_COMMAND_PATTERN, Pattern.MULTILINE);
        this.taskParamPattern = Pattern.compile(PROCESS_PARAM_PATTERN, Pattern.CASE_INSENSITIVE);
    }

    /**
     * Execute processes from a task file. Params can be passed to a process by
     * using %PARAM1%, %PARAM2%, and so on...
     *
     * i.e.,
     *
     * To execute the following Java command under "/opt/my_app" directory:
     *
     *     java -jar bin/my_app.jar config.file %PARAM1% %PARAM2%
     *
     * Include the following line to a task file:
     *
     *     "/opt/my_app", "java", "-jar", "bin/my_app.jar", %PARAM1%, "config.file", %PARAM2%
     *
     * The first string is the WorkingDirectory while the rest is the
     * composition of the whole command line. %PARAM1% and %PARAM2% is then
     * replaced with arguments passed in the method.
     *
     * @param params Params that is mapped to %PARAM1%, %PARAM2%, etc...
     * @throws Exception
     */
    public void executeAll(String... params) throws Exception {
        String taskFileContent = FileUtil.readFromFile(taskFilePath);
        Matcher m = taskCommandPattern.matcher(taskFileContent);
        while (m.find()) {
            String workingDirectory = m.group(WORKING_DIRECTORY);
            String command = m.group(COMMAND);
            executeCommand(workingDirectory, command, params);
        }
    }

    private void executeCommand(String workingDirectory, String command, String... args) throws Exception {
        LOGGER.info("Parsing process (WorkingDirectory: {}, Command: {}, Args: {})", workingDirectory, command, args);

        // Parse params in commands (%PARAM1%, %PARAM2%, ..., %PARAMn%)
        Matcher m = taskParamPattern.matcher(command);
        while (m.find()) {
            String originalCommand = m.group(0);
            int paramIndex = Integer.parseInt(m.group(PARAM_INDEX));
            String arg = paramIndex <= args.length ? args[paramIndex - 1] : "";
            command = command.replace(originalCommand, arg);
        }

        LOGGER.info("Executing process: {} (WorkingDirectory: {})", command, workingDirectory);
        List<String> cleanedUpCommand = Arrays.asList(command.split("\\s")).stream().filter(item -> item.length() > 0).collect(Collectors.toList());
        int exitCode = ProcessUtil.executeAndWait(workingDirectory, cleanedUpCommand);
        if (exitCode == 0) {
            LOGGER.info("Process ran successfully (exit code = {})", exitCode);
        } else {
            LOGGER.warn("Process did not run successfully (exit code = {})", exitCode);
            throw new Exception(MessageFormat.format("Process has returned an exit code of {0}", exitCode));
        }
    }

}
