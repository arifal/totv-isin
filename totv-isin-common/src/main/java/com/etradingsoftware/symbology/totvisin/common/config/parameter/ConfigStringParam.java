/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.config.parameter;

import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;

import java.text.MessageFormat;
import java.util.Properties;

public class ConfigStringParam implements ConfigParam {

    private String key;
    private String value;
    private boolean required;
    private Object defaultValue;

    public ConfigStringParam(String key, boolean required, Object defaultValue) {
        this.key = key;
        this.required = required;
        this.defaultValue = defaultValue;
    }

    public ConfigStringParam(String key, boolean required) {
        this(key, required, null);
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void loadFromProperties(Properties prop) throws ConfigurationException {
        if (defaultValue == null) {
            value = prop.getProperty(key);
        } else {
            value = prop.getProperty(key, String.valueOf(defaultValue));
        }

        if (required && value == null) {
            throw new ConfigurationException(MessageFormat.format("Property ''{0}'' is missing from the config file", key));
        }
    }

}
