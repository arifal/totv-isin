/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Base64;

public final class DownloadUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadUtil.class);

    public static String getUriContent(String uri, String username, String password) throws IOException {
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(uri);

            String cookies = null;
            while (true) {
                connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(15000);
                connection.setReadTimeout(15000);
                connection.setInstanceFollowRedirects(false);
                connection.setRequestMethod("GET");

                if (username != null && password != null) {
                    connection.setRequestProperty("Authorization", MessageFormat.format("Basic {0}", getAuth(username, password)));
                }

                if (cookies != null) {
                    connection.setRequestProperty("Cookie", cookies);
                }

                // Handle URI redirection
                if (connection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM) {
                    String redirectUri = connection.getHeaderField("Location");
                    cookies = connection.getHeaderField("Set-Cookie");
                    url = new URL(redirectUri);
                } else {
                    break;
                }
            }

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                StringBuffer output = new StringBuffer();

                inputStream = connection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    output.append(MessageFormat.format("{0}\n", line));
                }

                return output.toString();
            } {
                LOGGER.warn("HTTP Response: {} - {}", connection.getResponseCode(), connection.getResponseMessage());
            }

            return null;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) { /* do nothing */ }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) { /* do nothing */ }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static String getUriContent(String uri) throws IOException {
        return getUriContent(uri, null, null);
    }

    private static String getAuth(String user, String password) throws UnsupportedEncodingException {
        String userPass = MessageFormat.format("{0}:{1}", user, password);
        return Base64.getEncoder().encodeToString(userPass.getBytes("UTF-8"));
    }

}
