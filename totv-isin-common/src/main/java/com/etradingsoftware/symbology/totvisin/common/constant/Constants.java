/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.constant;

import java.util.ArrayList;
import java.util.List;

public final class Constants {

    public static final String CONFIG_DELIM = "\\s*,\\s*";

    public static final List<String> ASSET_CLASSES = new ArrayList<>();

    static {
        ASSET_CLASSES.add("Rates");
        ASSET_CLASSES.add("Credit");
        ASSET_CLASSES.add("Foreign_Exchange");
        ASSET_CLASSES.add("Equity");
        ASSET_CLASSES.add("Commodities");
    }

}
