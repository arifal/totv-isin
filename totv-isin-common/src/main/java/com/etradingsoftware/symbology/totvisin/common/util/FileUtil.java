/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FileUtil {

    public static boolean makeDirs(String path, boolean isFile) {
        if (isFile) {
            return new File(path).getParentFile().mkdirs();
        } else {
            return new File(path).mkdirs();
        }
    }

    public static boolean makeDirs(String path) {
        return makeDirs(path, false);
    }

    public static String readFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public static void writeToFile(String content, String destinationPath) throws IOException {
        makeDirs(destinationPath, true);
        Files.write(Paths.get(destinationPath), content.getBytes());
    }

    public static List<String> listFiles(String path) throws IOException {
        List<String> fileNames;
        try (Stream<Path> stream = Files.list(Paths.get(path))) {
            fileNames = stream.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
        }
        return fileNames;
    }

    public static boolean isExists(String path) {
        return new File(path).exists();
    }

    public static String getFileName(String path) {
        return Paths.get(path).getFileName().toString();
    }

    public static String joinPath(String path1, String path2) {
        return MessageFormat.format("{0}/{1}", path1, path2);
    }

}
