package com.etradingsoftware.symbology.totvisin.common.util;

import com.etradingsoftware.symbology.totvisin.common.model.TotvApp;

import java.text.MessageFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public final class TotvUtil {

    private static final String APP_PARAM = "${app}";
    private static final String DATE_PARAM = "${date}";
    private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("yyyyMMdd");

    private static final String DATA_DIR_PATTERN = MessageFormat.format("/data/{0}/{1}", APP_PARAM, DATE_PARAM);
    private static final String LOCK_DIR_PATTERN = MessageFormat.format("/lock/{0}", APP_PARAM);
    private static final String PROCESS_DIR_PATTERN = MessageFormat.format("/process/{0}-processes.dat", APP_PARAM);

    private static final String TASK_PENDING_SUBDIR = "/pending";
    private static final String TASK_COMPLETED_SUBDIR = "/completed";
    private static final String TASK_FAILED_SUBDIR = "/failed";

    public static String getDataDir(TotvApp totvApp, String totvPath) {
        return MessageFormat.format("{0}{1}"
                , totvPath
                , DATA_DIR_PATTERN
                        .replace(APP_PARAM, totvApp.getName())
                        .replace(DATE_PARAM, toCurrentDate())
        );
    }

    public static String getDataPendingDir(TotvApp totvApp, String totvPath) {
        return MessageFormat.format("{0}{1}"
                , getDataDir(totvApp, totvPath)
                , TASK_PENDING_SUBDIR
        );
    }

    public static String getDataCompletedDir(TotvApp totvApp, String totvPath) {
        return MessageFormat.format("{0}{1}"
                , getDataDir(totvApp, totvPath)
                , TASK_COMPLETED_SUBDIR
        );
    }

    public static String getDataFailedDir(TotvApp totvApp, String totvPath) {
        return MessageFormat.format("{0}{1}"
                , getDataDir(totvApp, totvPath)
                , TASK_FAILED_SUBDIR
        );
    }

    public static String getLockDir(TotvApp totvApp, String totvPath) {
        return MessageFormat.format("{0}{1}"
                , totvPath
                , LOCK_DIR_PATTERN
                        .replace(APP_PARAM, totvApp.getName())
        );
    }

    public static String getProcessDir(TotvApp totvApp, String totvPath) {
        return MessageFormat.format("{0}{1}"
                , totvPath
                , PROCESS_DIR_PATTERN
                        .replace(APP_PARAM, totvApp.getName())
        );
    }

    private static CharSequence toCurrentDate() {
        return ZonedDateTime.now(ZoneOffset.UTC).format(DATE_PATTERN);
    }

    public static void main(String[] args) {
        String totvPath = "/efs/totv";
        for (TotvApp totvApp : TotvApp.values()) {
            System.out.println("data          : " + getDataDir(totvApp, totvPath));
            System.out.println("data pending  : " + getDataPendingDir(totvApp, totvPath));
            System.out.println("data completed: " + getDataCompletedDir(totvApp, totvPath));
            System.out.println("data failed   : " + getDataFailedDir(totvApp, totvPath));
            System.out.println("lock          : " + getLockDir(totvApp, totvPath));
            System.out.println("process       : " + getProcessDir(totvApp, totvPath));
        }
    }

}
