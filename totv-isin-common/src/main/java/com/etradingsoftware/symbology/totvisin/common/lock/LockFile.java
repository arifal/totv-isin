/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.lock;

import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LockFile {

    private static final Logger LOGGER = LoggerFactory.getLogger(LockFile.class);

    private RandomAccessFile randomAccessFile;
    private FileLock taskLock;
    private String lockPath;

    public boolean acquireLock(String lockPath) {
        try {
            this.lockPath = lockPath;

            // Create directory, as needed
            FileUtil.makeDirs(lockPath, true);

            randomAccessFile = new RandomAccessFile(lockPath, "rw");
            taskLock = randomAccessFile.getChannel().tryLock();
            if (taskLock != null) {
                LOGGER.info("Lock acquired: {}", lockPath);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to acquire lock file {} - {}", lockPath, e.getMessage());
            return false;
        }
    }

    public void releaseLock() {
        if (taskLock != null) {
            try {
                taskLock.release();
                LOGGER.info("Lock released :{}", lockPath);
            } catch (IOException e) {
                /* do nothing */
            }
        }
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (IOException e) {
                /* do nothing */
            }
        }
        try {
            LOGGER.info("Deleting lock file {}...", lockPath);
            Files.delete(Paths.get(lockPath));
        } catch (IOException e) {
            /* do nothing */
        }
    }

}
