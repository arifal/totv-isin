/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.util;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.model.*;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AwsAutoScalingUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwsAutoScalingUtil.class);

    public static InstanceMonitoring generateInstanceMonitoring(boolean enabled) {
        InstanceMonitoring instanceMonitoring = new InstanceMonitoring();
        instanceMonitoring.setEnabled(enabled);
        return instanceMonitoring;
    }

    public static Tag generateAutoScalingTag(String resourceId, String resourceType, String key, String value) {
        Tag tag = new Tag();
        tag.setResourceId(resourceId);
        tag.setResourceType(resourceType);
        tag.setKey(key);
        tag.setValue(value);
        return tag;
    }

    public static void deleteLaunchConfiguration(String launchConfigurationName, AmazonAutoScaling client) {
        DeleteLaunchConfigurationRequest request = new DeleteLaunchConfigurationRequest();
        request.setLaunchConfigurationName(launchConfigurationName);
        client.deleteLaunchConfiguration(request);
    }

    public static void deleteAutoScalingGroup(String autoScalingGroupName, AmazonAutoScaling client) {
        DeleteAutoScalingGroupRequest request = new DeleteAutoScalingGroupRequest();
        request.setAutoScalingGroupName(autoScalingGroupName);
        client.deleteAutoScalingGroup(request);
    }

    public static DescribeInstancesResult describeEC2InstancesByIds(List<String> instanceIds, AmazonEC2 client) {
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        request.setInstanceIds(instanceIds);
        return client.describeInstances(request);
    }

    public static void setAutoScalingGroupDesiredCapacity(String autoScalingGroupName, int size, AmazonAutoScaling client) {
        SetDesiredCapacityRequest request = new SetDesiredCapacityRequest();
        request.setAutoScalingGroupName(autoScalingGroupName);
        request.setDesiredCapacity(size);
        client.setDesiredCapacity(request);
    }

    public static void setAutoScalingInstanceProtection(String autoScalingGroupName, String instanceId, Boolean protectedFromScaleIn, AmazonAutoScaling client) {
        SetInstanceProtectionRequest request = new SetInstanceProtectionRequest();
        request.setAutoScalingGroupName(autoScalingGroupName);
        request.setInstanceIds(new ArrayList<String>() {{ add(instanceId); }});
        request.setProtectedFromScaleIn(protectedFromScaleIn);
        client.setInstanceProtection(request);
    }

    public static AutoScalingGroup describeAutoScalingGroup(String autoScalingGroupName, AmazonAutoScaling client) {
        DescribeAutoScalingGroupsRequest request = new DescribeAutoScalingGroupsRequest();
        request.setAutoScalingGroupNames(new ArrayList<String>() {{ add(autoScalingGroupName); }});
        DescribeAutoScalingGroupsResult result = client.describeAutoScalingGroups(request);
        if (result != null && result.getAutoScalingGroups().size() > 0) {
            return result.getAutoScalingGroups().get(0);
        } else {
            return null;
        }
    }

    public static void terminateAutoScalingInstance(String instanceId, boolean shouldDecrementDesiredCapacity, AmazonAutoScaling client) {
        TerminateInstanceInAutoScalingGroupRequest request = new TerminateInstanceInAutoScalingGroupRequest();
        request.setInstanceId(instanceId);
        request.setShouldDecrementDesiredCapacity(shouldDecrementDesiredCapacity);
        client.terminateInstanceInAutoScalingGroup(request);
    }

    /**
     * Display details and health status of instances
     */
    public static void displayInstancesInfo(AmazonAutoScaling amazonAutoScalingClient, AmazonEC2 amazonEc2Client) {
        List<String> instanceIds = new ArrayList<>();
        Map<String, String> instanceHealthStatusMap = new HashMap<>();
        Map<String, String> instanceLifecycleStatusMap = new HashMap<>();

        DescribeAutoScalingInstancesResult autoScalingInstancesResult = amazonAutoScalingClient.describeAutoScalingInstances();
        for (AutoScalingInstanceDetails asgInstance : autoScalingInstancesResult.getAutoScalingInstances()) {
            LOGGER.info("ASG Instance: {}", asgInstance);
            instanceIds.add(asgInstance.getInstanceId());
            instanceHealthStatusMap.put(asgInstance.getInstanceId(), asgInstance.getHealthStatus());
            instanceLifecycleStatusMap.put(asgInstance.getInstanceId(), asgInstance.getLifecycleState());
        }

        DescribeInstancesResult ec2InstancesResult = describeEC2InstancesByIds(instanceIds, amazonEc2Client);
        for (Reservation reservation : ec2InstancesResult.getReservations()) {
            for (Instance ec2Instance : reservation.getInstances()) {
                String healthStatus;
                String lifecycleStatus;

                if (!instanceHealthStatusMap.containsKey(ec2Instance.getInstanceId())
                        || !instanceLifecycleStatusMap.containsKey(ec2Instance.getInstanceId())) {
                    continue;
                }

                String privateIp = ec2Instance.getPrivateIpAddress() != null ? ec2Instance.getPrivateIpAddress() : "";
                healthStatus = instanceHealthStatusMap.get(ec2Instance.getInstanceId());
                lifecycleStatus = instanceLifecycleStatusMap.get(ec2Instance.getInstanceId());

                LOGGER.info("EC2 Instance: {}  {}  {}  {}  {}  {}  {}  {}  {}  {}  {}  {}"
                        , ec2Instance.getInstanceId()
                        , StringUtils.rightPad(privateIp, 15)
                        , StringUtils.rightPad(healthStatus, 9)
                        , StringUtils.rightPad(lifecycleStatus, 12)
                        , ec2Instance.getImageId()
                        , ec2Instance.getInstanceType()
                        , ec2Instance.getKeyName()
                        , ec2Instance.getSecurityGroups()
                        , ec2Instance.getSubnetId()
                        , ec2Instance.getVpcId()
                        , ec2Instance.getLaunchTime()
                        , ec2Instance.getTags()
                );
            }
        }
    }

}
