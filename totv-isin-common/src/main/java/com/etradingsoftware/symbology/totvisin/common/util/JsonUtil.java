/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonUtil {

    // This needs to be static for performance reasons
    private static final ObjectMapper mapper = new ObjectMapper();

    public static Object toJsonObject(String json, Class jsonClass) throws IOException {
        return mapper.readValue(json, jsonClass);
    }

    public static String toJsonString(Object json) throws JsonProcessingException {
        return mapper.writeValueAsString(json);
    }

}
