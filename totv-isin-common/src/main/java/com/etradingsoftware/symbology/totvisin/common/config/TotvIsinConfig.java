/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.common.config;

import com.etradingsoftware.symbology.totvisin.common.config.parameter.*;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public abstract class TotvIsinConfig {

    private static Map<String, ConfigParam> configParamMap;

    static {
        configParamMap = new HashMap<>();
    }

    protected void loadFromConfigFile(String configPath) throws ConfigurationException {
        try (InputStream in = new FileInputStream(configPath)) {
            Properties prop = new Properties();
            prop.load(in);

            for (ConfigParam configParam : configParamMap.values()) {
                configParam.loadFromProperties(prop);
            }
        } catch (ConfigurationException e) {
            throw e;
        } catch (Exception e) {
            throw new ConfigurationException(MessageFormat.format("Unable to load config file: {0}", configPath), e);
        }
    }

    protected static void addBoolean(String key, boolean required) {
        configParamMap.put(key, new ConfigBooleanParam(key, required));
    }

    protected static void addBoolean(String key, boolean required, Object defaultValue) {
        configParamMap.put(key, new ConfigBooleanParam(key, required, defaultValue));
    }

    protected static void addInt(String key, boolean required) {
        configParamMap.put(key, new ConfigIntParam(key, required));
    }

    protected static void addInt(String key, boolean required, Object defaultValue) {
        configParamMap.put(key, new ConfigIntParam(key, required, defaultValue));
    }

    protected static void addLong(String key, boolean required) {
        configParamMap.put(key, new ConfigLongParam(key, required));
    }

    protected static void addLong(String key, boolean required, Object defaultValue) {
        configParamMap.put(key, new ConfigLongParam(key, required, defaultValue));
    }

    protected static void addString(String key, boolean required) {
        configParamMap.put(key, new ConfigStringParam(key, required));
    }

    protected static void addString(String key, boolean required, Object defaultValue) {
        configParamMap.put(key, new ConfigStringParam(key, required, defaultValue));
    }

    protected Boolean getBoolean(String key) {
        return (Boolean) configParamMap.get(key).getValue();
    }

    protected Integer getInt(String key) {
        return (Integer) configParamMap.get(key).getValue();
    }

    protected Long getLong(String key) {
        return (Long) configParamMap.get(key).getValue();
    }

    protected String getString(String key) {
        return (String) configParamMap.get(key).getValue();
    }

}
