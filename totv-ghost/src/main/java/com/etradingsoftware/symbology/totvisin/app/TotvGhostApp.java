/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.app;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.etradingsoftware.symbology.totvisin.common.ec2.TerminateInstanceDeferred;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.lock.LockFile;
import com.etradingsoftware.symbology.totvisin.common.model.TotvApp;
import com.etradingsoftware.symbology.totvisin.common.process.ProcessRunner;
import com.etradingsoftware.symbology.totvisin.common.util.*;
import com.etradingsoftware.symbology.totvisin.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TotvGhostApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvGhostApp.class);

    private static final long QUERY_SERVER_INFO_DELAY = 5000;
    private static final long TERMINATE_INSTANCE_DELAY = 60000;
    private static int MAX_RETRY_ATTEMPT = 1;
    private static int RETRY_INTERVAL_IN_MILLIS = 10000;

    private Config config;

    private AmazonAutoScaling amazonAutoScalingClient;
    private AmazonEC2 amazonEc2Client;

    private String instanceId;
    private LockFile lockFile;
    private ProcessRunner processRunner;

    private String pendingTaskPath = null;
    private String failedTaskPath = null;

    public TotvGhostApp(String configPath) throws ConfigurationException, UnknownHostException {
        config = new Config(configPath);

        amazonAutoScalingClient = AmazonAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonEc2Client = AmazonEC2ClientBuilder.standard().withRegion(config.getAwsRegion()).build();

        lockFile = new LockFile();
        processRunner = new ProcessRunner(TotvUtil.getProcessDir(TotvApp.GHOST, config.getTotvPath()));
//        if (isBadLuck()) {
//            processRunner = new ProcessRunner(TotvUtil.getProcessDir(TotvApp.GHOST, config.getTotvPath()) + "-fail"); // @TODO: testing purposes; to be removed later
//        } else {
//            processRunner = new ProcessRunner(TotvUtil.getProcessDir(TotvApp.GHOST, config.getTotvPath()));
//        }
    }

    public void start() {
        init();

        try {
            performTask();
        } catch (Exception e) {
            LOGGER.error("Exception encountered - {}", e.getMessage(), e);
        }

        // Terminate EC2 instance and decrement autoscale desired capacity
        terminateInstanceDeferred(config.getAutoscalingGhostGroupName(), instanceId, true, TERMINATE_INSTANCE_DELAY);

        LOGGER.info("Waiting for {} seconds before retrieving status of the instances...", QUERY_SERVER_INFO_DELAY / 1000);
        HelperUtil.sleep(QUERY_SERVER_INFO_DELAY);

        // Display EC2 instances info
        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, amazonEc2Client);
    }

    private void init() {
        // Retrieve machine hostname for logging
        String hostname = "UnknownHost";
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            /* do nothing */
        }
        System.setProperty("hostname", hostname);

        LOGGER.info("Started.");

        // Retrieving self EC2 server instance id for EC2 termination
        if (config.getDevInstanceIdOverride() == null) {
            LOGGER.info("Retrieving EC2 instance via {}...", config.getEc2InstanceIdUri());
            String outputContent = null;
            try {
                outputContent = DownloadUtil.getUriContent(config.getEc2InstanceIdUri());
            } catch (Exception e) {
                LOGGER.error("Failed to retrieve EC2 server instance ID! This task won't be able to terminate this server.");
            }

            if (outputContent != null) {
                instanceId = outputContent
                        .replace("\r", "")
                        .replace("\n", "")
                        .trim()
                ;
            } else {
                LOGGER.error("Failed to retrieve EC2 server instance ID! This task won't be able to terminate this server.");
            }
        } else {
            instanceId = config.getDevInstanceIdOverride();
        }
        LOGGER.info("EC2 Instance ID: {}", instanceId);
    }

    private void performTask() throws IOException {
        // Create lock & completed directories, as needed

        // Acquire a task
        findAndPerformTask();
    }

    private void findAndPerformTask() throws IOException {
        // Keep on processing task files one by one until its empty
        while (true) {
            // Create pending directory, as needed
            FileUtil.makeDirs(TotvUtil.getDataPendingDir(TotvApp.GHOST, config.getTotvPath()));

            List<String> tasks = FileUtil.listFiles(TotvUtil.getDataPendingDir(TotvApp.GHOST, config.getTotvPath()));
            LOGGER.info("{} pending tasks found. Acquiring a task...", tasks.size());

            // Find a task and acquire its lock file
            boolean isLockAcquired = false;
            String completedTaskPath = null;

            for (String task : tasks) {
                String taskFileName = FileUtil.getFileName(task);
                String taskLockPath = getTaskLockPath(taskFileName);
                isLockAcquired = lockFile.acquireLock(taskLockPath);
                if (isLockAcquired) {
                    // Create completed and failed directories, as needed
                    FileUtil.makeDirs(TotvUtil.getDataCompletedDir(TotvApp.GHOST, config.getTotvPath()));
                    FileUtil.makeDirs(TotvUtil.getDataFailedDir(TotvApp.GHOST, config.getTotvPath()));

                    pendingTaskPath = task;
                    completedTaskPath = getCompletedTaskPath(taskFileName);
                    failedTaskPath = getFailedTaskPath(taskFileName);
                    break; // We have found a task to process, stop looking for another task files
                } else {
                    LOGGER.info("Task {} is already locked by another process.", taskFileName);
                }
            }

            int retryCount = 0;
            if (isLockAcquired) {
                // Process task w/ retry-on-fail
                while (true) {
                    try {
                        LOGGER.info("Performing task on file {}...", pendingTaskPath);
                        processRunner.executeAll(pendingTaskPath.toString());
                        LOGGER.info("Task completed.");

                        // Move task from pending to completed
                        LOGGER.info("Moving task file from {} to {}...", pendingTaskPath, completedTaskPath);
                        try {
                            Files.move(Paths.get(pendingTaskPath), Paths.get(completedTaskPath), StandardCopyOption.REPLACE_EXISTING);
                        } catch (Exception e) {
                            LOGGER.warn("Failed moving file - {}", e.getMessage());
                        }

                        // Release and delete lock file
                        lockFile.releaseLock();
                        break; // Processing is successful, exit retry-on-fail loop
                    } catch (Exception e) {
                        LOGGER.error("Fatal exception encountered - {}", e.getMessage(), e);
                        if (retryCount++ < MAX_RETRY_ATTEMPT) {
                            LOGGER.warn("Retrying in {} seconds...", RETRY_INTERVAL_IN_MILLIS / 1000);
                        } else {
                            // Move task from pending to failed
                            LOGGER.info("Moving task file from {} to {}...", pendingTaskPath, failedTaskPath);
                            try {
                                Files.move(Paths.get(pendingTaskPath), Paths.get(failedTaskPath), StandardCopyOption.REPLACE_EXISTING);
                            } catch (Exception e2) {
                                LOGGER.warn("Failed moving file - {}", e2.getMessage());
                            }
                            return; // Retry-on-fail reaches max retry attempt, abort!
                        }
                    }

                    HelperUtil.sleep(RETRY_INTERVAL_IN_MILLIS);
                }
            } else {
                LOGGER.warn("No task to acquire.");
                break; // No more task files to process, abort!
            }
        }
    }

    private String getCompletedTaskPath(String taskFilename) {
        return MessageFormat.format("{0}/{1}", TotvUtil.getDataCompletedDir(TotvApp.GHOST, config.getTotvPath()), taskFilename);
    }

    private String getFailedTaskPath(String taskFilename) {
        return MessageFormat.format("{0}/{1}", TotvUtil.getDataFailedDir(TotvApp.GHOST, config.getTotvPath()), taskFilename);
    }

    private String getTaskLockPath(String filenameToLock) {
        return MessageFormat.format("{0}/{1}.lck", TotvUtil.getLockDir(TotvApp.GHOST, config.getTotvPath()), filenameToLock);
    }

    private void terminateInstanceDeferred(String autoscalingGroupName, String instanceId, boolean shouldDecrementDesiredCapacity, long executeAfterInMillis) {
        if (shouldDecrementDesiredCapacity) {
            LOGGER.warn("Terminating instance and decrementing autoscaling desired capacity in {} seconds...", executeAfterInMillis / 1000);
        } else {
            LOGGER.warn("Terminating instance in {} seconds...", executeAfterInMillis / 1000);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.submit(new TerminateInstanceDeferred(amazonAutoScalingClient, autoscalingGroupName, instanceId, shouldDecrementDesiredCapacity, executeAfterInMillis));
        executorService.shutdown();
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Illegal number of arguments. Usage: <configFile>");
            System.err.println("WARNING! If this app is running on an auto-scaled EC2, then this EC2 instance is now a ZOMBIE and MUST BE TERMINATED MANUALLY!");
            System.exit(1);
        }

        try {
            String configPath = args[0];

            TotvGhostApp app = new TotvGhostApp(configPath);
            app.start();
        } catch (Exception e) {
            LOGGER.error("Fatal exception encountered - {}", e.getMessage(), e);
            System.err.println("WARNING! If this app is running on an auto-scaled EC2, then this EC2 instance is now a ZOMBIE and MUST BE TERMINATED MANUALLY!");
            System.exit(1);
        } finally {
            LOGGER.info("Finished.");
        }
    }

//    private boolean isBadLuck() {
//        // @TODO: Testing purposes, to be removed
//        Random rand = new Random(System.currentTimeMillis());
//        return rand.nextInt(2) == 1;
//    }

}
