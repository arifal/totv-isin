/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.config;

import com.etradingsoftware.symbology.totvisin.common.config.TotvIsinConfig;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config extends TotvIsinConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    private static final String AWS_REGION = "aws.region";
    private static final String EC2_INSTANCE_ID_URI = "ec2.instance_id.uri";
    private static final String TOTV_PATH = "totv.path";
    private static final String AUTOSCALING_GHOST_GROUP_NAME = "autoscaling.ghost.group_name";
    private static final String DEV_INSTANCE_ID_OVERRIDE = "dev.instance_id.override";

    static {
        addString(AWS_REGION, false);
        addString(EC2_INSTANCE_ID_URI, true);
        addString(TOTV_PATH, true);
        addString(AUTOSCALING_GHOST_GROUP_NAME, true);
        addString(DEV_INSTANCE_ID_OVERRIDE, false);
    }

    public Config(String configPath) throws ConfigurationException {
        LOGGER.info("Loading application config file: {}", configPath);
        loadFromConfigFile(configPath);
    }

    public String getAwsRegion() {
        return getString(AWS_REGION);
    }

    public String getEc2InstanceIdUri() {
        return getString(EC2_INSTANCE_ID_URI);
    }

    public String getTotvPath() {
        return getString(TOTV_PATH);
    }

    public String getAutoscalingGhostGroupName() {
        return getString(AUTOSCALING_GHOST_GROUP_NAME);
    }

    public String getDevInstanceIdOverride() {
        return getString(DEV_INSTANCE_ID_OVERRIDE);
    }

}
