/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.app;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.etradingsoftware.symbology.totvisin.common.ec2.TerminateInstanceDeferred;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.model.TotvApp;
import com.etradingsoftware.symbology.totvisin.common.process.ProcessRunner;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.DownloadUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.common.util.TotvUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TotvTotoApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvTotoApp.class);

    private static final long QUERY_SERVER_INFO_DELAY = 5000;
    private static final long TERMINATE_INSTANCE_DELAY = 60000;

    private Config config;

    private AmazonAutoScaling amazonAutoScalingClient;
    private AmazonEC2 amazonEc2Client;

    private String instanceId;
    private ProcessRunner processRunner;

    public TotvTotoApp(String configPath) throws ConfigurationException, UnknownHostException {
        config = new Config(configPath);

        amazonAutoScalingClient = AmazonAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonEc2Client = AmazonEC2ClientBuilder.standard().withRegion(config.getAwsRegion()).build();

        processRunner = new ProcessRunner(TotvUtil.getProcessDir(TotvApp.TOTO, config.getTotvPath()));
//        if (isBadLuck()) {
//            processRunner = new ProcessRunner(TotvUtil.getProcessDir(TotvApp.TOTO, config.getTotvPath()) + "-fail"); // @TODO: testing purposes; to be removed later
//        } else {
//            processRunner = new ProcessRunner(TotvUtil.getProcessDir(TotvApp.TOTO, config.getTotvPath()));
//        }
    }

    public void start() {
        init();

        try {
            performTask();
        } catch (Exception e) {
            LOGGER.error("Fatal exception encountered - {}", e.getMessage(), e);
        }

        // Terminate EC2 instance and decrement autoscale desired capacity
        terminateInstanceDeferred(config.getAutoscalingTotoGroupName(), instanceId, true, TERMINATE_INSTANCE_DELAY);

        LOGGER.info("Waiting for {} seconds before retrieving status of the instances...", QUERY_SERVER_INFO_DELAY / 1000);
        HelperUtil.sleep(QUERY_SERVER_INFO_DELAY);

        // Display EC2 instances info
        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, amazonEc2Client);
    }

    private void init() {
        // Retrieve machine hostname for logging
        String hostname = "UnknownHost";
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
                /* do nothing */
        }
        System.setProperty("hostname", hostname);

        LOGGER.info("Started.");

        // Retrieving self EC2 server instance id for EC2 termination
        if (config.getDevInstanceIdOverride() == null) {
            LOGGER.info("Retrieving EC2 instance via {}...", config.getEc2InstanceIdUri());
            String outputContent = null;
            try {
                outputContent = DownloadUtil.getUriContent(config.getEc2InstanceIdUri());
            } catch (Exception e) {
                LOGGER.error("Failed to retrieve EC2 server instance ID! This task won't be able to terminate this server.");
            }

            if (outputContent != null) {
                instanceId = outputContent
                        .replace("\r", "")
                        .replace("\n", "")
                        .trim()
                ;
            } else {
                LOGGER.error("Failed to retrieve EC2 server instance ID! This task won't be able to terminate this server.");
            }
        } else {
            instanceId = config.getDevInstanceIdOverride();
        }
        LOGGER.info("EC2 Instance ID: {}", instanceId);
    }

    private void performTask() throws Exception {
        processRunner.executeAll();
        LOGGER.info("Task completed.");
    }

    private void terminateInstanceDeferred(String autoscalingGroupName, String instanceId, boolean shouldDecrementDesiredCapacity, long executeAfterInMillis) {
        if (shouldDecrementDesiredCapacity) {
            LOGGER.warn("Terminating instance and decrementing autoscaling desired capacity in {} seconds...", executeAfterInMillis / 1000);
        } else {
            LOGGER.warn("Terminating instance in {} seconds...", executeAfterInMillis / 1000);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.submit(new TerminateInstanceDeferred(amazonAutoScalingClient, autoscalingGroupName, instanceId, shouldDecrementDesiredCapacity, executeAfterInMillis));
        executorService.shutdown();
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Illegal number of arguments. Usage: <configFile>");
            System.err.println("WARNING! If this app is running on an auto-scaled EC2, then this EC2 instance is now a ZOMBIE and MUST BE TERMINATED MANUALLY!");
            System.exit(1);
        }

        try {
            String configPath = args[0];

            TotvTotoApp app = new TotvTotoApp(configPath);
            app.start();
        } catch (Exception e) {
            LOGGER.error("Fatal exception encountered - {}", e.getMessage(), e);
            System.err.println("WARNING! If this app is running on an auto-scaled EC2, then this EC2 instance is now a ZOMBIE and MUST BE TERMINATED MANUALLY!");
            System.exit(1);
        } finally {
            LOGGER.info("Finished.");
        }
    }

//    private boolean isBadLuck() {
//        // @TODO: Testing purposes, to be removed
//        Random rand = new Random(System.currentTimeMillis());
//        return rand.nextInt(2) == 1;
//    }

}
