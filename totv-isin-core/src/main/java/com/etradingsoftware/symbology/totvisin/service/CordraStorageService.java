/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.service;

import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.CordraObject;
import com.etradingsoftware.symbology.totvisin.util.cnri.CnriCordraObjectUtil;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.concurrent.TimeUnit;

public class CordraStorageService {

    private static final long MAX_TIME_MS = 30000;

    private MongoClient client;
    private MongoCollection<Document> collection;

    public CordraStorageService(Config config) {
        client = createClient(config.getCordraStorageConnectionUri());
        collection = client.getDatabase(config.getCordraStorageDatabase()).getCollection(config.getCordraStorageCollection());
    }

    public void stop() {
        if (client != null) {
            client.close();
        }
    }

    public CordraObject searchById(String id) {
        Document doc = collection.find(new Document("id", id)).maxTime(MAX_TIME_MS, TimeUnit.MILLISECONDS).first();
        return CnriCordraObjectUtil.documentToCordraObject(doc);
    }

    private MongoClient createClient(String connectionUri) {
        return new MongoClient(new MongoClientURI(connectionUri));
    }

}
