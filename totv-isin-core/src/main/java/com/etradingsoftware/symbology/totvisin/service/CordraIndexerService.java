/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.service;

import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.CordraIndex;
import com.etradingsoftware.symbology.totvisin.util.JsonUtil;
import com.etradingsoftware.symbology.totvisin.util.UriUtil;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class CordraIndexerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CordraIndexerService.class);

    private SolrClient client;

    public CordraIndexerService(Config config) {
        client = createSolrClient(config);
    }

    public void stop() {
        if (client != null) {
            try {
                client.close();
            } catch (IOException e) {
                /* do nothing */
            }
        }
    }

    public List<CordraIndex> searchByIsin(String isin) throws IOException, SolrServerException {
        List<CordraIndex> outputList = new ArrayList<>();

        SolrQuery query = new SolrQuery();
        query.set("q", MessageFormat.format("\\/ISIN\\/ISIN:\"{0}\"", isin));
        query.set("wt", "json");
        query.set("rows", "1000");

        try {
            for (SolrDocument solrDocument : client.query(query).getResults()) {
                String jsonStr = JsonUtil.toJsonString(solrDocument);
                CordraIndex output = (CordraIndex) JsonUtil.toJsonObject(jsonStr, CordraIndex.class);
                outputList.add(output);
            }
        } catch (Exception e) {
            LOGGER.warn("Failed querying CordraIndexer - {}", e.getMessage());
        }

        return outputList;
    }

    private SolrClient createSolrClient(Config config) {
        if (config.getCordraIndexerSolrHost() != null) {
            return new HttpSolrClient.Builder(UriUtil.join(config.getCordraIndexerSolrHost(), config.getCordraIndexerDefaultCollection())).build();
        }
        else if (config.getCordraIndexerSolrZkHost() != null) {
            CloudSolrClient client = new CloudSolrClient.Builder().withZkHost(config.getCordraIndexerSolrZkHost()).build();
            client.setDefaultCollection(config.getCordraIndexerDefaultCollection());
            client.connect();
            return client;
        }

        return null;
    }

}
