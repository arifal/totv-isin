/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task.unused;

import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;
import com.etradingsoftware.symbology.totvisin.model.RecordsConsumer;
import com.etradingsoftware.symbology.totvisin.repository.IsinRecordRepository;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AnnaDsbIsinConsumerService implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnaDsbIsinConsumerService.class);

    private static final Pattern ALL_SECURITIES_PATTERN = Pattern.compile(".*");

    private volatile boolean stopped;

    private Config config;
    private IsinRecordRepository isinRecordRepository;
    private KafkaConsumer<String, String> consumer;
    private RecordsConsumer recordsConsumer;

    public AnnaDsbIsinConsumerService(Config config, IsinRecordRepository isinRecordRepository) {
        this.config = config;
        this.isinRecordRepository = isinRecordRepository;

        recordsConsumer = new RecordsConsumerImpl(isinRecordRepository);
        stopped = false;
    }

    @Override
    public void run() {
        consumer = createConsumer();

        while (!stopped) {
            ConsumerRecords<String, String> records = consumer.poll(Integer.MAX_VALUE);
            if (records != null) {
                recordsConsumer.accept(consumer, records);
            }
        }
    }

    private KafkaConsumer<String, String> createConsumer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", config.getAnnaDsbKafkaBootstrapServers());
        props.put("group.id", config.getAnnaDsbKafkaGroupId());
        props.put("enable.auto.commit", "false");
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(ALL_SECURITIES_PATTERN, new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                LOGGER.info("Partitions revoked: {}", String.join(", ", partitions.stream().map(TopicPartition::toString).collect(Collectors.toList())));
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                LOGGER.info("Partitions assigned: {}", String.join(", ", partitions.stream().map(TopicPartition::toString).collect(Collectors.toList())));
            }
        });

        return consumer;
    }

    class RecordsConsumerImpl implements RecordsConsumer {

        private IsinRecordRepository isinRecordRepository;

        public RecordsConsumerImpl(IsinRecordRepository isinRecordRepository) {
            this.isinRecordRepository = isinRecordRepository;
        }

        @Override
        public void accept(KafkaConsumer consumer, ConsumerRecords<String, String> records) {
            for (ConsumerRecord<String, String> record : records) {
                IsinRecord isinRecord = new IsinRecord();
                isinRecord.setTopic(record.value());
                isinRecord.setPayload(record.value());
                isinRecordRepository.add(isinRecord);
            }
        }

    }

}
