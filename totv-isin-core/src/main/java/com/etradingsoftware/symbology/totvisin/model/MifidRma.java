/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MifidRma {

    @JsonProperty("id")
    private String id;
    @JsonProperty("rma_countryCode")
    private String countryCode;
    @JsonProperty("rma_relevantAuthority")
    private String relevantAuthority;
    @JsonProperty("rma_micCode")
    private String micCode;
    @JsonProperty("rma_name")
    private String name;
    @JsonProperty("rma_modificationDateStr")
    private String modificationDateStr;
    @JsonProperty("rma_status")
    private String status;
    @JsonProperty("rma_instrumentIdentifier")
    private String instrumentIdentifier;
    private String timestamp;
    @JsonProperty("_root_")
    private String root;

    public MifidRma() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRelevantAuthority() {
        return relevantAuthority;
    }

    public void setRelevantAuthority(String relevantAuthority) {
        this.relevantAuthority = relevantAuthority;
    }

    public String getMicCode() {
        return micCode;
    }

    public void setMicCode(String micCode) {
        this.micCode = micCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModificationDateStr() {
        return modificationDateStr;
    }

    public void setModificationDateStr(String modificationDateStr) {
        this.modificationDateStr = modificationDateStr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInstrumentIdentifier() {
        return instrumentIdentifier;
    }

    public void setInstrumentIdentifier(String instrumentIdentifier) {
        this.instrumentIdentifier = instrumentIdentifier;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    @Override
    public String toString() {
        return "MifidRma{" +
                "id='" + id + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", relevantAuthority='" + relevantAuthority + '\'' +
                ", micCode='" + micCode + '\'' +
                ", name='" + name + '\'' +
                ", modificationDateStr='" + modificationDateStr + '\'' +
                ", status='" + status + '\'' +
                ", instrumentIdentifier='" + instrumentIdentifier + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", root='" + root + '\'' +
                '}';
    }

}
