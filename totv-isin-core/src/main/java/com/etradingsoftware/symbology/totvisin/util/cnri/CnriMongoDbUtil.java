/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.util.cnri;

import com.google.gson.*;
import org.bson.Document;

import java.util.List;
import java.util.Map;

public final class CnriMongoDbUtil {

    private static boolean isObjectNumberLong(JsonObject bsonObject) {
        if (bsonObject.has("$numberLong") && bsonObject.entrySet().size() == 1) {
            JsonElement numberLongEl = bsonObject.get("$numberLong");
            if (numberLongEl.isJsonPrimitive() && numberLongEl.getAsJsonPrimitive().isString()) return true;
        }
        return false;
    }

    public static JsonElement removeNumberLongAndPercentDecodeFieldNames(JsonElement bsonElement) {
        if (bsonElement.isJsonObject()) {
            JsonObject bsonObject = bsonElement.getAsJsonObject();
            if (isObjectNumberLong(bsonObject)) {
                JsonElement numberLongEl = bsonObject.get("$numberLong");
                String value = numberLongEl.getAsString();
                return new JsonPrimitive(Long.valueOf(value));
            } else {
                return removeNumberLongAndPercentDecodeFieldNamesForObject(bsonObject);
            }
        } else if (bsonElement.isJsonArray()) {
            JsonArray bsonArray = bsonElement.getAsJsonArray();
            return removeNumberLongAndPercentDecodeFieldNamesForArray(bsonArray);
        } else {
            return bsonElement;
        }
    }

    private static JsonElement removeNumberLongAndPercentDecodeFieldNamesForArray(JsonArray bsonArray) {
        JsonArray newArray = null;
        for (int i = 0; i < bsonArray.size(); i++) {
            JsonElement currentValue = bsonArray.get(i);
            JsonElement newValue = removeNumberLongAndPercentDecodeFieldNames(currentValue);
            if (newArray != null) {
                newArray.add(newValue);
            } else if (newValue != currentValue) {
                newArray = new JsonArray();
                for (int j = 0; j < i; j++) {
                    newArray.add(bsonArray.get(j));
                }
                newArray.add(newValue);
            }
        }
        if (newArray != null) return newArray;
        else return bsonArray;
    }

    private static JsonElement removeNumberLongAndPercentDecodeFieldNamesForObject(JsonObject bsonObject) {
        boolean changed = false;
        for (Map.Entry<String, JsonElement> entry : bsonObject.entrySet()) {
            JsonElement currentValue = entry.getValue();
            JsonElement newValue = removeNumberLongAndPercentDecodeFieldNames(currentValue);
            if (newValue != currentValue) {
                entry.setValue(newValue);
            }
            String currentKey = entry.getKey();
            String newKey = decodeKey(currentKey);
            if (newKey != currentKey) {
                changed = true;
            }
        }
        if (changed) {
            JsonObject newObject = new JsonObject();
            for (Map.Entry<String, JsonElement> entry : bsonObject.entrySet()) {
                String currentKey = entry.getKey();
                JsonElement value = entry.getValue();
                String newKey = decodeKey(currentKey);
                newObject.add(newKey, value);
            }
            return newObject;
        } else {
            return bsonObject;
        }
    }

    public static String removeNumberLongAndPercentDecodeFieldNames(String json) {
        JsonElement jsonElement = new JsonParser().parse(json);
        JsonElement newElement = removeNumberLongAndPercentDecodeFieldNames(jsonElement);
        return newElement.toString();
    }

    public static Document percentEncodeFieldNames(Document doc) {
        boolean changed = false;
        for (Map.Entry<String, Object> entry : doc.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof Document) {
                Object newValue = percentEncodeFieldNames((Document)value);
                if (value != newValue) entry.setValue(newValue);
                value = newValue;
            } else if (value instanceof List) {
                @SuppressWarnings("rawtypes")
                List list = (List) value;
                percentEncodeFieldNames(list);
            }
            String encodedKey = encodeKey(key);
            if (!key.equals(encodedKey)) {
                changed = true;
            }
        }
        if (changed) {
            Document newDoc = new Document();
            for (Map.Entry<String, Object> entry : doc.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                String encodedKey = encodeKey(key);
                newDoc.append(encodedKey, value);
            }
            return newDoc;
        } else {
            return doc;
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static void percentEncodeFieldNames(List list) {
        for (int i = 0; i < list.size(); i++) {
            Object el = list.get(i);
            if (el instanceof List) {
                List sublist = (List) el;
                percentEncodeFieldNames(sublist);
            } else if (el instanceof Document) {
                Document newDoc = percentEncodeFieldNames((Document)el);
                if (newDoc != el) {
                    list.set(i, newDoc);
                }
            }
        }
    }

    public static String encodeKey(String key) {
        if (key.equals("_id")) {
            return "%5Fid";
        }
        String res = key.replace("%", "%25");
        res = res.replace(".", "%2E");
        res = res.replace("\u0000", "%00");
        if (res.startsWith("$")) {
            res = res.replaceFirst("^\\$", "%24");
        }
        return res;
    }

    public static String decodeKey(String key) {
        return decodeURLIgnorePlus(key);
    }

    private static String decodeURLIgnorePlus(String str) {
        byte[] utf8Buf = new byte[str.length()];
        int utf8Loc = 0;
        int strLoc = 0;
        int strLen = str.length();

        while(true) {
            while(strLoc < strLen) {
                char ch = str.charAt(strLoc++);
                if(ch == 37 && strLoc + 2 <= strLen) {
                    utf8Buf[utf8Loc++] = decodeHexByte(str.charAt(strLoc++), str.charAt(strLoc++));
                } else {
                    utf8Buf[utf8Loc++] = (byte)ch;
                }
            }

            try {
                return new String(utf8Buf, 0, utf8Loc, "UTF8");
            } catch (Exception var6) {
                return new String(utf8Buf, 0, utf8Loc);
            }
        }
    }

    private static final byte decodeHexByte(char ch1, char ch2) {
        char n1 = (char)(ch1 >= 48 && ch1 <= 57?ch1 - 48:(ch1 >= 97 && ch1 <= 122?ch1 - 97 + 10:ch1 - 65 + 10));
        char n2 = (char)(ch2 >= 48 && ch2 <= 57?ch2 - 48:(ch2 >= 97 && ch2 <= 122?ch2 - 97 + 10:ch2 - 65 + 10));
        return (byte)(n1 << 4 | n2);
    }

}
