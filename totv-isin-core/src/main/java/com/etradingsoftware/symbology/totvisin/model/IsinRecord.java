/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

public class IsinRecord {

    private String isin;
    private String topic;
    private String payload;

    public IsinRecord(String isin, String topic, String payload) {
        this.isin = isin;
        this.topic = topic;
        this.payload = payload;
    }

    public IsinRecord() {
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "IsinRecord{" +
                "isin='" + isin + '\'' +
                ", topic='" + topic + '\'' +
                ", payload='" + payload + '\'' +
                '}';
    }

}
