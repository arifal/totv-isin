/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task;

import com.etradingsoftware.symbology.totvisin.common.util.ExecutorServiceUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;
import com.etradingsoftware.symbology.totvisin.repository.IsinRecordRepository;
import com.etradingsoftware.symbology.totvisin.task.unused.TotvProcessorTask;
import com.etradingsoftware.symbology.totvisin.task.unused.UTotvProcessorTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class IsinRecordProcessor implements Runnable {

    private static Logger LOGGER = LoggerFactory.getLogger(IsinRecordProcessor.class);

    private static final long POLL_TIMEOUT_IN_MILLIS = 10000;

    private Config config;
    private IsinRecordRepository isinRecordRepository;
    private ExecutorService executorService;

    private volatile boolean stop;
    private volatile boolean terminated;

    public IsinRecordProcessor(Config config, IsinRecordRepository isinRecordRepository) {
        this.config = config;
        this.isinRecordRepository = isinRecordRepository;

        executorService = Executors.newFixedThreadPool(config.getTotvProcessorTaskNumThreads());
        stop = false;
        terminated = false;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("Task started.");

            boolean maxThreadQueueReachedLogged = false;
            while (!stop) {
                IsinRecord isinRecord = null;
                try {
                    if (((ThreadPoolExecutor) executorService).getQueue().size() > config.getTotvProcessorTaskMaxQueuedTasks()) {
                        HelperUtil.sleep(100);
                        if (!maxThreadQueueReachedLogged) {
                            LOGGER.info("WARNING! Max thread queues has been reached.");
                            maxThreadQueueReachedLogged = true;
                        }
                        continue;
                    } else if (maxThreadQueueReachedLogged) {
                        maxThreadQueueReachedLogged = false;
                    }

                    isinRecord = isinRecordRepository.poll(POLL_TIMEOUT_IN_MILLIS, TimeUnit.MILLISECONDS);
                    if (isinRecord != null) {
                        TotvProcessorTask totvProcessorTask = new TotvProcessorTask(isinRecord);
                        UTotvProcessorTask uTotvProcessorTask = new UTotvProcessorTask(isinRecord);

                        executorService.execute(totvProcessorTask);
                        executorService.execute(uTotvProcessorTask);

                        LOGGER.debug("QueuedTasks: {}, IsinRecordQueue Size: {}", ((ThreadPoolExecutor) executorService).getQueue().size(), isinRecordRepository.size());
                    }
                } catch (Exception e) {
                    if (isinRecord != null) {
                        LOGGER.error("Exception encounterd while processing ISIN {} - {}", isinRecord.getIsin(), e.getMessage(), e);
                    } else {
                        LOGGER.error("Exception encounterd - {}", e.getMessage(), e);
                    }
                }
            }
        } finally {
            cleanUp();
            if (isinRecordRepository.size() > 0) {
                LOGGER.warn("WARNING! There were {} queued records that were not processed.", isinRecordRepository.size());
            }
            LOGGER.info("Task stopped.");
            terminated = true;
        }
    }

    public void stop() {
        LOGGER.info("Stopping...");
        stop = true;
    }

    public boolean isTerminated() {
        return terminated && (executorService == null || executorService.isTerminated());
    }

    private void cleanUp() {
        ExecutorServiceUtil.shutdownAndWait(executorService, LOGGER);
    }

}
