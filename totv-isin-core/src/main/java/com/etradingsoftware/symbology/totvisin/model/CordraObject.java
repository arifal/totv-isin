/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CordraObject {

    public String id;
    public String type;
    public JsonElement content;
    public AccessControlList acl;
    public Metadata metadata;
    public List<CordraObjectPayload> payloads;

    private transient List<String> payloadsToDelete = new ArrayList<String>();

    public CordraObject() { }

    public CordraObject(String type, String json) {
        this.type = type;
        setContent(json);
    }

    public CordraObject(String type, JsonElement content) {
        this.type = type;
        this.content = content;
    }

    public CordraObject(String type, Object object) {
        this.type = type;
        Gson gson = new Gson();
        this.content = gson.toJsonTree(object);
    }

    public void setContent(String json) {
        content = new JsonParser().parse(json);
    }

    public void setContent(Object object) {
        Gson gson = new Gson();
        this.content = gson.toJsonTree(object);
    }

    public String getContentAsString() {
        Gson gson = new Gson();
        return gson.toJson(content);
    }

    public <T> T getContent(Class<T> klass) {
        Gson gson = new Gson();
        return gson.fromJson(content, klass);
    }

    public void addPayload(String name, String filename, String mediaType, InputStream in) {
        if (payloads == null) payloads = new ArrayList<>();
        CordraObjectPayload p = new CordraObjectPayload();
        p.name = name;
        p.filename = filename;
        p.mediaType = mediaType;
        p.setInputStream(in);
        payloads.add(p);
    }

    public void deletePayload(String name) {
        payloadsToDelete.add(name);
        removePayloadFromList(name);
    }

    public List<String> getPayloadsToDelete() {
        return payloadsToDelete;
    }

    private void removePayloadFromList(String name) {
        if (payloads == null) return;
        Iterator<CordraObjectPayload> iter = payloads.iterator();
        while (iter.hasNext()) {
            CordraObjectPayload p = iter.next();
            if (p.name.equals(name)) {
                iter.remove();
            }
        }
        if (payloads.isEmpty()) payloads = null;
    }

    public static class AccessControlList {
        public List<String> readers;
        public List<String> writers;
    }

    public static class Metadata {
        public long createdOn;
        public String createdBy;
        public long modifiedOn;
        public String modifiedBy;
        public Boolean isVersion;
        public String versionOf;
        public String publishedBy;
        public Long publishedOn;
        public String remoteRepository;

        public Long txnId; //TODO

        public Map<String, String> internalMetadata; // TODO
    }

    @Override
    public String toString() {
        return "CordraObject{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", content=" + content +
                ", acl=" + acl +
                ", metadata=" + metadata +
                ", payloads=" + payloads +
                ", payloadsToDelete=" + payloadsToDelete +
                '}';
    }

}
