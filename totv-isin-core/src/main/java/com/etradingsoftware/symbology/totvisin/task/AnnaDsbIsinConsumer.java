/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task;

import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;
import com.etradingsoftware.symbology.totvisin.model.RecordsConsumer;
import com.etradingsoftware.symbology.totvisin.repository.IsinRecordRepository;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AnnaDsbIsinConsumer implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnaDsbIsinConsumer.class);

    private static final Pattern ALL_SECURITIES_PATTERN = Pattern.compile(".*");
    private static final long POLL_TIMEOUT_IN_MILLIS = 1000;

    private Config config;
    private KafkaConsumer<String, String> consumer;
    private RecordsConsumer recordsConsumer;

    private volatile boolean stop;
    private volatile boolean terminated;

    public AnnaDsbIsinConsumer(Config config, IsinRecordRepository isinRecordRepository) {
        this.config = config;

        consumer = createConsumer();
        recordsConsumer = new RecordsConsumerImpl(isinRecordRepository);

        stop = false;
        terminated = false;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("Task started.");

            subscribe();
            while (!stop) {
                try {
                    ConsumerRecords<String, String> records = consumer.poll(POLL_TIMEOUT_IN_MILLIS);
                    if (records != null) {
                        recordsConsumer.accept(consumer, records);
                    }
                } catch (WakeupException e) {
                    /* do nothing */
                } catch (Exception e) {
                    LOGGER.error("Exception encounterd - {}", e.getMessage(), e);
                }
            }
        } finally {
            cleanUp();
            LOGGER.info("Task stopped.");
            terminated = true;
        }
    }

    public void stop() {
        LOGGER.info("Stopping...");
        stop = true;
        consumer.wakeup();
    }

    public boolean isTerminated() {
        return terminated;
    }

    private void cleanUp() {
        if (consumer != null) {
            consumer.close();
        }
    }

    private KafkaConsumer<String, String> createConsumer() {
        Properties props = new Properties();

        props.put("bootstrap.servers", config.getAnnaDsbKafkaBootstrapServers());
        props.put("group.id", config.getAnnaDsbKafkaGroupId());
        props.put("enable.auto.commit", "false");
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        return new KafkaConsumer<>(props);
    }

    private void subscribe() {
        consumer.subscribe(ALL_SECURITIES_PATTERN, new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                LOGGER.info("Partitions revoked: {}", String.join(", ", partitions.stream().map(TopicPartition::toString).collect(Collectors.toList())));
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                LOGGER.info("Partitions assigned: {}", String.join(", ", partitions.stream().map(TopicPartition::toString).collect(Collectors.toList())));
            }
        });
    }

    class RecordsConsumerImpl implements RecordsConsumer {

        private IsinRecordRepository isinRecordRepository;

        public RecordsConsumerImpl(IsinRecordRepository isinRecordRepository) {
            this.isinRecordRepository = isinRecordRepository;
        }

        @Override
        public void accept(KafkaConsumer consumer, ConsumerRecords<String, String> records) {
            for (ConsumerRecord<String, String> record : records) {
                IsinRecord isinRecord = new IsinRecord();
                isinRecord.setTopic(record.value());
                isinRecord.setPayload(record.value());
                isinRecordRepository.add(isinRecord);
            }
        }

    }

}
