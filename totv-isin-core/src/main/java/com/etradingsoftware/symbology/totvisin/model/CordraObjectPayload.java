/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

import java.io.InputStream;

public class CordraObjectPayload {

    public String name;
    public String filename;
    public String mediaType;
    public long size;
    private transient InputStream in;

    public void setInputStream(InputStream in) {
        this.in = in;
    }

    public InputStream getInputStream() {
        return in;
    }

}
