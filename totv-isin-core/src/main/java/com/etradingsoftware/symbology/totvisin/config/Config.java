/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.config;

import com.etradingsoftware.symbology.totvisin.common.config.TotvIsinConfig;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config extends TotvIsinConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    private static final String ESMA_REGISTERS_SOLR_HOST = "esma_registers.solr_host";
    private static final String ESMA_REGISTERS_MIFID_RMA_CORE = "esma_registers.mifid_rma_core";
    private static final String ESMA_REGISTERS_MIFID_MTF_CORE = "esma_registers.mifid_mtf_core";
    private static final String ESMA_REGISTERS_MIFID_OTF_CORE = "esma_registers.mifid_otf_core";
    private static final String CORDRA_INDEXER_SOLR_HOST = "cordra_indexer.solr_host";
    private static final String CORDRA_INDEXER_SOLR_ZK_HOST = "cordra_indexer.solr_zk_host";
    private static final String CORDRA_INDEXER_DEFAULT_COLLECTION = "cordra_indexer.default_collection";
    private static final String CORDRA_STORAGE_CONNECTION_URI = "cordra_storage.connection_uri";
    private static final String CORDRA_STORAGE_DATABASE = "cordra_storage.database";
    private static final String CORDRA_STORAGE_COLLECTION = "cordra_storage.collection";
    private static final String ANNA_DSB_KAFKA_BOOTSTRAP_SERVERS = "anna_dsb_kafka.bootstrap_servers";
    private static final String ANNA_DSB_KAFKA_GROUP_ID = "anna_dsb_kafka.group_id";
    private static final String ISIN_RECORDS_QUEUE_SIZE = "isin_records.queue_size";
    private static final String TOTV_PROCESSOR_TASK_NUM_THREADS = "totv_processor_task.num_threads";
    private static final String TOTV_PROCESSOR_TASK_MAX_QUEUED_TASKS = "totv_processor_task.max_queued_tasks";

    static {
        addString(ESMA_REGISTERS_SOLR_HOST, true);
        addString(ESMA_REGISTERS_MIFID_RMA_CORE, true);
        addString(ESMA_REGISTERS_MIFID_MTF_CORE, true);
        addString(ESMA_REGISTERS_MIFID_OTF_CORE, true);
        addString(CORDRA_INDEXER_SOLR_HOST, false);
        addString(CORDRA_INDEXER_SOLR_ZK_HOST, true);
        addString(CORDRA_INDEXER_DEFAULT_COLLECTION, true);
        addString(CORDRA_STORAGE_CONNECTION_URI, true);
        addString(CORDRA_STORAGE_DATABASE, true);
        addString(CORDRA_STORAGE_COLLECTION, true);
        addString(ANNA_DSB_KAFKA_BOOTSTRAP_SERVERS, true);
        addString(ANNA_DSB_KAFKA_GROUP_ID, true);
        addInt(ISIN_RECORDS_QUEUE_SIZE, true);
        addInt(TOTV_PROCESSOR_TASK_NUM_THREADS, true);
        addInt(TOTV_PROCESSOR_TASK_MAX_QUEUED_TASKS, true);
    }

    public Config(String configPath) throws ConfigurationException {
        LOGGER.info("Loading application config file: {}", configPath);
        loadFromConfigFile(configPath);
    }

    public String getEsmaRegistersSolrHost() {
        return getString(ESMA_REGISTERS_SOLR_HOST);
    }

    public String getEsmaRegistersMifidRmaCore() {
        return getString(ESMA_REGISTERS_MIFID_RMA_CORE);
    }

    public String getEsmaRegistersMifidMtfCore() {
        return getString(ESMA_REGISTERS_MIFID_MTF_CORE);
    }

    public String getEsmaRegistersMifidOtfCore() {
        return getString(ESMA_REGISTERS_MIFID_OTF_CORE);
    }

    public String getCordraIndexerSolrHost() {
        return getString(CORDRA_INDEXER_SOLR_HOST);
    }

    public String getCordraIndexerSolrZkHost() {
        return getString(CORDRA_INDEXER_SOLR_ZK_HOST);
    }

    public String getCordraIndexerDefaultCollection() {
        return getString(CORDRA_INDEXER_DEFAULT_COLLECTION);
    }

    public String getCordraStorageConnectionUri() {
        return getString(CORDRA_STORAGE_CONNECTION_URI);
    }

    public String getCordraStorageDatabase() {
        return getString(CORDRA_STORAGE_DATABASE);
    }

    public String getCordraStorageCollection() {
        return getString(CORDRA_STORAGE_COLLECTION);
    }

    public String getAnnaDsbKafkaBootstrapServers() {
        return getString(ANNA_DSB_KAFKA_BOOTSTRAP_SERVERS);
    }

    public String getAnnaDsbKafkaGroupId() {
        return getString(ANNA_DSB_KAFKA_GROUP_ID);
    }

    public int getIsinRecordsQueueSize() {
        return getInt(ISIN_RECORDS_QUEUE_SIZE);
    }

    public int getTotvProcessorTaskNumThreads() {
        return getInt(TOTV_PROCESSOR_TASK_NUM_THREADS);
    }

    public int getTotvProcessorTaskMaxQueuedTasks() {
        return getInt(TOTV_PROCESSOR_TASK_MAX_QUEUED_TASKS);
    }

}
