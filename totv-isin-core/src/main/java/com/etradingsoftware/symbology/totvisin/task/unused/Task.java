/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task.unused;

public abstract class Task {

    private volatile boolean stop;
    private volatile boolean terminated;

    public Task() {
        stop = false;
        terminated = false;
    }

    public void stop() {
        stop = true;
    }

    public boolean isTerminated() {
        return terminated;
    }

}
