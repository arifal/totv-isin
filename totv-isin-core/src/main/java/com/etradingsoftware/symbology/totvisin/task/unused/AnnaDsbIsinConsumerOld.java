/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task.unused;

import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;
import com.etradingsoftware.symbology.totvisin.repository.IsinRecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnnaDsbIsinConsumerOld implements Runnable {

    private static Logger LOGGER = LoggerFactory.getLogger(AnnaDsbIsinConsumerOld.class);

    private IsinRecordRepository isinRecordRepository;

    private volatile boolean stop;
    private volatile boolean terminated;

    public AnnaDsbIsinConsumerOld(IsinRecordRepository isinRecordRepository) {
        this.isinRecordRepository = isinRecordRepository;

        stop = false;
        terminated = false;
    }

    public void stop() {
        LOGGER.info("Stopping...");
        stop = true;
    }

    public boolean isTerminated() {
        return terminated;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("Task started.");

            long ctr = 0;
            while (!stop) {
                try {
                    // @TODO: do something
                    IsinRecord isinRecord = new IsinRecord();
                    isinRecord.setIsin("ANNA-DSB-" + ++ctr);
                    isinRecordRepository.add(isinRecord);
                    HelperUtil.sleep(1);
                } catch (Exception e) {
                    LOGGER.error("Exception encounterd - {}", e.getMessage(), e);
                }
            }
        } finally {
            LOGGER.info("Task stopped.");
            terminated = true;
        }
    }

}
