/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.repository;

import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class IsinRecordRepository {

    private BlockingQueue<IsinRecord> isinRecords;

    public IsinRecordRepository(Config config) {
        isinRecords = new LinkedBlockingQueue<>(config.getIsinRecordsQueueSize());
    }

    public int size() {
        return isinRecords.size();
    }

    public void add(IsinRecord isinRecord) {
        isinRecords.add(isinRecord);
    }

    public IsinRecord poll(long timeout, TimeUnit unit) throws InterruptedException {
        return isinRecords.poll(timeout, unit);
    }

}
