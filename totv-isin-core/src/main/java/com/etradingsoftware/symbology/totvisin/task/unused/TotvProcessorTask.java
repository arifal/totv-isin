/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task.unused;

import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TotvProcessorTask implements Runnable {

    private static Logger LOGGER = LoggerFactory.getLogger(TotvProcessorTask.class);

    private IsinRecord isinRecord;

    public TotvProcessorTask(IsinRecord isinRecord) {
        this.isinRecord = isinRecord;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("[TotvProcessorTask-{}] Processing: {}", Thread.currentThread().getName(), isinRecord);
            HelperUtil.sleep(3000);
        } catch (Exception e) {
            LOGGER.error("[TotvProcessorTask-{}] Fatal exception encountered - {}", Thread.currentThread().getName(), e.getMessage(), e);
        }
    }

}
