/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.service;

import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.MifidMtf;
import com.etradingsoftware.symbology.totvisin.model.MifidOtf;
import com.etradingsoftware.symbology.totvisin.model.MifidRma;
import com.etradingsoftware.symbology.totvisin.util.JsonUtil;
import com.etradingsoftware.symbology.totvisin.util.UriUtil;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EsmaRegistersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EsmaRegistersService.class);

    private HttpSolrClient mifidRmaClient;
    private HttpSolrClient mifidMtfClient;
    private HttpSolrClient mifidOtfClient;

    public EsmaRegistersService(Config config) {
        this.mifidRmaClient = createSolrClient(config.getEsmaRegistersSolrHost(), config.getEsmaRegistersMifidRmaCore());
        this.mifidMtfClient = createSolrClient(config.getEsmaRegistersSolrHost(), config.getEsmaRegistersMifidMtfCore());
        this.mifidOtfClient = createSolrClient(config.getEsmaRegistersSolrHost(), config.getEsmaRegistersMifidOtfCore());
    }

    public void close() {
        if (mifidRmaClient != null) {
            try {
                mifidRmaClient.close();
            } catch (IOException e) {
                /* do nothing */
            }
        }
        if (mifidMtfClient != null) {
            try {
                mifidMtfClient.close();
            } catch (IOException e) {
                /* do nothing */
            }
        }
        if (mifidOtfClient != null) {
            try {
                mifidOtfClient.close();
            } catch (IOException e) {
                /* do nothing */
            }
        }
    }

    public List<MifidRma> queryMifidRma() throws IOException, SolrServerException {
        List<MifidRma> outputList = new ArrayList<>();

        SolrQuery query = new SolrQuery();
        query.set("q", "*.*");
        query.set("wt", "json");
        query.set("rows", "1000");

        try {
            for (SolrDocument solrDocument : mifidRmaClient.query(query).getResults()) {
                String jsonStr = JsonUtil.toJsonString(solrDocument);
                MifidRma output = (MifidRma) JsonUtil.toJsonObject(jsonStr, MifidRma.class);
                outputList.add(output);
            }
        } catch (Exception e) {
            LOGGER.warn("Failed querying MifidRma - {}", e.getMessage());
        }

        return outputList;
    }

    public List<MifidMtf> queryMifidMtf() throws IOException, SolrServerException {
        List<MifidMtf> outputList = new ArrayList<>();

        SolrQuery query = new SolrQuery();
        query.set("q", "*.*");
        query.set("wt", "json");
        query.set("rows", "1000");

        try {
            for (SolrDocument solrDocument : mifidMtfClient.query(query).getResults()) {
                String jsonStr = JsonUtil.toJsonString(solrDocument);
                MifidMtf output = (MifidMtf) JsonUtil.toJsonObject(jsonStr, MifidMtf.class);
                outputList.add(output);
            }
        } catch (Exception e) {
            LOGGER.warn("Failed querying MifidMtf - {}", e.getMessage());
        }

        return outputList;
    }

    public List<MifidOtf> queryMifidOtf() throws IOException, SolrServerException {
        List<MifidOtf> outputList = new ArrayList<>();

        SolrQuery query = new SolrQuery();
        query.set("q", "*.*");
        query.set("wt", "json");
        query.set("rows", "1000");

        try {
            for (SolrDocument solrDocument : mifidOtfClient.query(query).getResults()) {
                String jsonStr = JsonUtil.toJsonString(solrDocument);
                MifidOtf output = (MifidOtf) JsonUtil.toJsonObject(jsonStr, MifidOtf.class);
                outputList.add(output);
            }
        } catch (Exception e) {
            LOGGER.warn("Failed querying MifidOtf - {}", e.getMessage());
        }

        return outputList;
    }

    private HttpSolrClient createSolrClient(String baseUri, String core) {
        return new HttpSolrClient.Builder(UriUtil.join(baseUri, core)).build();
    }

}
