/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.util.cnri;

import com.etradingsoftware.symbology.totvisin.model.CordraObject;
import com.google.gson.Gson;
import org.bson.Document;

public final class CnriCordraObjectUtil {

    private final static Gson gson = new Gson();

    public static CordraObject documentToCordraObject(Document doc) {
        String json = CnriMongoDbUtil.removeNumberLongAndPercentDecodeFieldNames(doc.toJson());
        CordraObject d = gson.fromJson(json, CordraObject.class);
        return d;
    }

}
