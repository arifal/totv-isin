/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.app;

import com.etradingsoftware.symbology.totvisin._delete_later_.SampleIsins;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.util.ExecutorServiceUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.CordraIndex;
import com.etradingsoftware.symbology.totvisin.model.CordraObject;
import com.etradingsoftware.symbology.totvisin.repository.IsinRecordRepository;
import com.etradingsoftware.symbology.totvisin.service.CordraIndexerService;
import com.etradingsoftware.symbology.totvisin.service.CordraStorageService;
import com.etradingsoftware.symbology.totvisin.task.AnnaDsbIsinConsumer;
import com.etradingsoftware.symbology.totvisin.task.EsmaRegistersTaskScheduler;
import com.etradingsoftware.symbology.totvisin.task.IsinRecordProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TotvIsinApplication implements Runnable {

    private final Logger LOGGER = LoggerFactory.getLogger(TotvIsinApplication.class);

    private IsinRecordRepository isinRecordRepository;
    private IsinRecordProcessor isinRecordProcessor;
    private AnnaDsbIsinConsumer annaDsbIsinConsumer;
    private EsmaRegistersTaskScheduler esmaRegistersTaskScheduler;
    private CordraIndexerService cordraIndexerService;
    private CordraStorageService cordraStorageService;
    private ExecutorService executorService;

    private volatile boolean stop;
    private volatile boolean terminated;

    public TotvIsinApplication(String configPath) throws ConfigurationException, MalformedURLException {
        Config config = new Config(configPath);

        isinRecordRepository = new IsinRecordRepository(config);
        isinRecordProcessor = new IsinRecordProcessor(config, isinRecordRepository);
        annaDsbIsinConsumer = new AnnaDsbIsinConsumer(config, isinRecordRepository);
        esmaRegistersTaskScheduler = new EsmaRegistersTaskScheduler(config, isinRecordRepository);
        cordraIndexerService = new CordraIndexerService(config);
        cordraStorageService = new CordraStorageService(config);

        executorService = Executors.newFixedThreadPool(3);

        stop = false;
        terminated = false;
    }

    @Override
    public void run() {
        try {
            LOGGER.info("Started.");

            //executorService.execute(isinRecordProcessor);
            //executorService.execute(annaDsbIsinConsumer);
            //esmaRegistersTaskScheduler.startScheduler();
            for (String isin : SampleIsins.getSampleIsins()) {
                long start = System.currentTimeMillis();
                List<CordraIndex> cordraIndices = cordraIndexerService.searchByIsin("EZ1PRX0HSWV0");
                long cordraIndexServiceElapsedTime = System.currentTimeMillis() - start;

                for (CordraIndex cordraIndex : cordraIndices) {
                    start = System.currentTimeMillis();
                    CordraObject cordraObject = cordraStorageService.searchById(cordraIndices.get(0).getId());
                    long cordraStorageServiceElapsedTime = System.currentTimeMillis() - start;
                    LOGGER.info("ISIN: {}, SearchTimes = [Index: {}, Storage: {}, Total: {}]]", isin, cordraIndexServiceElapsedTime, cordraStorageServiceElapsedTime, (cordraIndexServiceElapsedTime + cordraStorageServiceElapsedTime));
                }
            }

            while (!stop) {
                HelperUtil.sleep(100);
            }
        } catch (Exception e) {
            LOGGER.error("Fatal error - {}", e.getMessage(), e);
        } finally {
            cleanUp();
            LOGGER.info("Stopped.");
            terminated = true;
        }
    }

    public void stop() {
        LOGGER.info("Stopping...");
        stop = true;
    }

    public boolean isTerminated() {
        return terminated;
    }

    private void cleanUp() {
        if (cordraStorageService != null) {
            cordraStorageService.stop();
        }

        if (cordraIndexerService != null) {
            cordraIndexerService.stop();
        }

        if (annaDsbIsinConsumer != null) {
            annaDsbIsinConsumer.stop();
        }

        if (esmaRegistersTaskScheduler != null) {
            esmaRegistersTaskScheduler.stopScheduler();
        }

        if (isinRecordProcessor != null) {
            isinRecordProcessor.stop();
        }

        while (annaDsbIsinConsumer != null && !annaDsbIsinConsumer.isTerminated()) {
            LOGGER.info("Waiting for AnnaDsbIsinConsumer to be finished before exiting...");
            HelperUtil.sleep(5000);
        }

        while (esmaRegistersTaskScheduler != null && !esmaRegistersTaskScheduler.isTerminated()) {
            LOGGER.info("Waiting for EsmaIsinRetriever to be finished before exiting...");
            HelperUtil.sleep(5000);
        }

        while (isinRecordProcessor != null && !isinRecordProcessor.isTerminated()) {
            LOGGER.info("Waiting for IsinRecordProcessor to be finished before exiting...");
            HelperUtil.sleep(5000);
        }

        ExecutorServiceUtil.shutdownAndWait(executorService, LOGGER);
    }

}
