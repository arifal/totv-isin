/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task;

import com.etradingsoftware.symbology.totvisin.common.util.ExecutorServiceUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.IsinRecord;
import com.etradingsoftware.symbology.totvisin.model.MifidMtf;
import com.etradingsoftware.symbology.totvisin.model.MifidOtf;
import com.etradingsoftware.symbology.totvisin.model.MifidRma;
import com.etradingsoftware.symbology.totvisin.repository.IsinRecordRepository;
import com.etradingsoftware.symbology.totvisin.service.EsmaRegistersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class EsmaRegistersTaskScheduler {

    private static Logger LOGGER = LoggerFactory.getLogger(EsmaRegistersTaskScheduler.class);

    private EsmaRegistersService esmaRegistersService;
    private Runnable mifidRmaTask;
    private Runnable mifidMtfTask;
    private Runnable mifidOtfTask;

    private ScheduledExecutorService scheduledExecutorService;

    private volatile boolean terminated;

    public EsmaRegistersTaskScheduler(Config config, IsinRecordRepository isinRecordRepository) throws MalformedURLException {
        esmaRegistersService = new EsmaRegistersService(config);
        mifidRmaTask = new MifidRmaTask(esmaRegistersService);
        mifidMtfTask = new MifidMtfTask(esmaRegistersService);
        mifidOtfTask = new MifidOtfTask(esmaRegistersService, isinRecordRepository);

        scheduledExecutorService = Executors.newScheduledThreadPool(3);

        terminated = false;
    }

    public void startScheduler() {
        LOGGER.info("Starting EsmaIsinRetriever task scheduler...");
        scheduledExecutorService.scheduleAtFixedRate(mifidRmaTask, 0, 1, TimeUnit.DAYS);
        scheduledExecutorService.scheduleAtFixedRate(mifidMtfTask, 0, 1, TimeUnit.DAYS);
        scheduledExecutorService.scheduleAtFixedRate(mifidOtfTask, 0, 1, TimeUnit.DAYS);
    }

    public void stopScheduler() {
        LOGGER.info("Stopping EsmaIsinRetriever task scheduler...");
        cleanUp();
        terminated = true;
    }

    public boolean isTerminated() {
        return terminated;
    }

    private void cleanUp() {
        esmaRegistersService.close();
        ExecutorServiceUtil.shutdownAndWait(scheduledExecutorService, LOGGER);
    }

    /**
     * Regulated Markets Task
     */
    class MifidRmaTask implements Runnable {

        private EsmaRegistersService esmaRegistersService;

        MifidRmaTask(EsmaRegistersService esmaRegistersService) {
            this.esmaRegistersService = esmaRegistersService;
        }

        @Override
        public void run() {
            try {
                LOGGER.info("[MifidRma] Task started.");

                List<MifidRma> mifidRmas =  esmaRegistersService.queryMifidRma();
                for (MifidRma mifidRma : mifidRmas) {
                    LOGGER.info(mifidRma.toString()); // @TODO: dump to somewhere
                }
            } catch (Exception e) {
                LOGGER.error("[MifidRma] Exception encounterd - {}", e.getMessage(), e);
            } finally {
                LOGGER.info("[MifidRma] Task stopped.");
            }
        }

    }

    /**
     * Mulitlateral Trading Facilities Task
     */
    class MifidMtfTask implements Runnable {

        private EsmaRegistersService esmaRegistersService;

        MifidMtfTask(EsmaRegistersService esmaRegistersService) {
            this.esmaRegistersService = esmaRegistersService;
        }

        @Override
        public void run() {
            // @TODO: dump to somewhere
            try {
                LOGGER.info("[MifidMtf] Task started.");

                List<MifidMtf> mifidMtfs =  esmaRegistersService.queryMifidMtf();
                for (MifidMtf mifidMtf : mifidMtfs) {
                    LOGGER.info(mifidMtf.toString()); // @TODO: dump to somewhere
                }
            } catch (Exception e) {
                LOGGER.error("[MifidMtf] Exception encounterd - {}", e.getMessage(), e);
            } finally {
                LOGGER.info("[MifidMtf] Task stopped.");
            }
        }

    }

    /**
     * OTF Task
     */
    class MifidOtfTask implements Runnable {

        private EsmaRegistersService esmaRegistersService;
        private IsinRecordRepository isinRecordRepository;

        MifidOtfTask(EsmaRegistersService esmaRegistersService, IsinRecordRepository isinRecordRepository) {
            this.esmaRegistersService = esmaRegistersService;
            this.isinRecordRepository = isinRecordRepository;
        }

        @Override
        public void run() {
            try {
                LOGGER.info("[MifidOtf] Task started.");

                List<MifidOtf> mifidOtfs =  esmaRegistersService.queryMifidOtf();
                for (MifidOtf mifidOtf : mifidOtfs) {
                    LOGGER.info(mifidOtf.toString()); // @TODO: process this
                    IsinRecord isinRecord = new IsinRecord();
                    isinRecordRepository.add(isinRecord);
                }
            } catch (Exception e) {
                LOGGER.error("[MifidOtf] Exception encounterd - {}", e.getMessage(), e);
            } finally {
                LOGGER.info("[MifidOtf] Task stopped.");
            }
        }

    }

}
