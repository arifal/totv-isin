/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MifidMtf {

    @JsonProperty("id")
    private String id;
    @JsonProperty("mtf_countryCode")
    private String countryCode;
    @JsonProperty("mtf_modificationDateStr")
    private String modificationDateStr;
    @JsonProperty("mtf_relevantAuthority")
    private String relevantAuthority;
    @JsonProperty("mtf_micCode")
    private String micCode;
    @JsonProperty("mtf_name")
    private String name;
    @JsonProperty("mtf_status")
    private String status;
    private String timestamp;
    @JsonProperty("_version_")
    private String version;
    @JsonProperty("_root_")
    private String root;

    public MifidMtf() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getModificationDateStr() {
        return modificationDateStr;
    }

    public void setModificationDateStr(String modificationDateStr) {
        this.modificationDateStr = modificationDateStr;
    }

    public String getRelevantAuthority() {
        return relevantAuthority;
    }

    public void setRelevantAuthority(String relevantAuthority) {
        this.relevantAuthority = relevantAuthority;
    }

    public String getMicCode() {
        return micCode;
    }

    public void setMicCode(String micCode) {
        this.micCode = micCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    @Override
    public String toString() {
        return "MifidMtf{" +
                "id='" + id + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", modificationDateStr='" + modificationDateStr + '\'' +
                ", relevantAuthority='" + relevantAuthority + '\'' +
                ", micCode='" + micCode + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", version='" + version + '\'' +
                ", root='" + root + '\'' +
                '}';
    }

}
