package com.etradingsoftware.symbology.totvisin.aws.rolepolicy;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.*;
import com.etradingsoftware.symbology.totvisin.common.util.AwsRoleUtil;
import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DBAutoScalingRolePolicy {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBAutoScalingRolePolicy.class);

    private AmazonIdentityManagement identityManagementClient;
    private Config config;

    public DBAutoScalingRolePolicy(AmazonIdentityManagement identityManagementClient, Config config) {
        this.identityManagementClient = identityManagementClient;
        this.config = config;
    }

    public void createRolePolicy() throws IOException {
        String roleName = config.getDbAutoscalingRoleName();
        String policyName = config.getDbAutoscalingPolicyName();

        try {
            String rolePolicyDocument = FileUtil.readFromFile(config.getDbAutoscalingRolePath());
            String policyDocument = FileUtil.readFromFile(config.getDbAutoscalingPolicyPath());

            LOGGER.info("Creating Role {}...", roleName);
            CreateRoleRequest createRoleRequest = new CreateRoleRequest();
            createRoleRequest.setRoleName(roleName);
            createRoleRequest.setAssumeRolePolicyDocument(rolePolicyDocument);
            identityManagementClient.createRole(createRoleRequest);

            LOGGER.info("Creating Policy {}...", policyName);
            CreatePolicyRequest createPolicyRequest = new CreatePolicyRequest();
            createPolicyRequest.setPolicyName(policyName);
            createPolicyRequest.setPolicyDocument(policyDocument);
            CreatePolicyResult createPolicyResult = identityManagementClient.createPolicy(createPolicyRequest);

            LOGGER.info("Attaching Role Policy (Role: {}, Policy: {})...", roleName, policyName);
            AttachRolePolicyRequest attachRolePolicyRequest = new AttachRolePolicyRequest();
            attachRolePolicyRequest.setRoleName(roleName);
            attachRolePolicyRequest.setPolicyArn(createPolicyResult.getPolicy().getArn());
            identityManagementClient.attachRolePolicy(attachRolePolicyRequest);
        } catch (Exception e) {
            LOGGER.warn("Failed creating Role Policy (Role: {}, Policy: {}) - {}", roleName, policyName, e.getMessage());
        }
    }

    public void deleteRolePolicy() throws IOException {
        String roleName = config.getDbAutoscalingRoleName();
        String policyName = config.getDbAutoscalingPolicyName();

        try {
            ListAttachedRolePoliciesResult listAttachedRolePoliciesResult = AwsRoleUtil.listAttachedRolePolicies(roleName, identityManagementClient);
            for (AttachedPolicy policy : listAttachedRolePoliciesResult.getAttachedPolicies()) {
                if (policyName.equals(policyName)) {
                    LOGGER.info("Detaching Role Policy (Role: {}, Policy: {})...", roleName);
                    DetachRolePolicyRequest request = new DetachRolePolicyRequest();
                    request.setRoleName(roleName);
                    request.setPolicyArn(policy.getPolicyArn());
                    identityManagementClient.detachRolePolicy(request);
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Failed detaching Role Policy (Role: {}, Policy: {}) - {}", roleName, policyName, e.getMessage());
        }

        try {
            for (Policy policy : identityManagementClient.listPolicies().getPolicies()) {
                if (policy.getPolicyName().equals(policyName)) {
                    LOGGER.info("Deleting Policy {}...", policyName);
                    DeletePolicyRequest deletePolicyRequest = new DeletePolicyRequest();
                    deletePolicyRequest.setPolicyArn(policy.getArn());
                    identityManagementClient.deletePolicy(deletePolicyRequest);
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Failed deleting Policy {} - {}", policyName, e.getMessage());
        }

        try {
            LOGGER.info("Deleting Role {}...", policyName);
            DeleteRoleRequest deleteRoleRequest = new DeleteRoleRequest();
            deleteRoleRequest.setRoleName(roleName);
            identityManagementClient.deleteRole(deleteRoleRequest);
        } catch (Exception e) {
            LOGGER.warn("Failed deleting Role {} - {}", policyName, e.getMessage());
        }
    }

    public void listRolePolicies() {
        ListPoliciesResult listPoliciesResult = identityManagementClient.listPolicies();
        
    }
}
