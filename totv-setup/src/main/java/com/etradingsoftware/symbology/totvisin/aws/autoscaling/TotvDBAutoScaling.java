package com.etradingsoftware.symbology.totvisin.aws.autoscaling;

import com.amazonaws.services.applicationautoscaling.AWSApplicationAutoScaling;
import com.amazonaws.services.applicationautoscaling.AWSApplicationAutoScalingClientBuilder;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.db.DBTableAS;
import com.etradingsoftware.symbology.totvisin.common.constant.Constants;
import com.etradingsoftware.symbology.totvisin.common.util.AwsDBUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.DBTableAutoScalingInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TotvDBAutoScaling {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvDBAutoScaling.class);

    private Config config;

    private AmazonDynamoDB amazonDynamoDB;
    private AWSApplicationAutoScaling awsApplicationAutoScalingClient;
    private AmazonIdentityManagement amazonIdentityManagementClient;
    private AmazonCloudWatch amazonCloudWatchClient;

    private DBTableAS dbTableAS;

    public TotvDBAutoScaling(Config config) {
        this.config = config;

        amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        awsApplicationAutoScalingClient = AWSApplicationAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonIdentityManagementClient = AmazonIdentityManagementClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonCloudWatchClient = AmazonCloudWatchClientBuilder.standard().withRegion(config.getAwsRegion()).build();

        dbTableAS = new DBTableAS(awsApplicationAutoScalingClient, amazonIdentityManagementClient, config);
    }

    public void create() {
        List<DBTableAutoScalingInfo> dbTableAutoScalingInfos = parseDBSettings(config);
        for (DBTableAutoScalingInfo dbTableAutoScalingInfo : dbTableAutoScalingInfos) {
            LOGGER.info("Setting up DB Auto-Scaling for {} - {}", dbTableAutoScalingInfo.getName(), dbTableAutoScalingInfo);
            registerScalableTargets(dbTableAutoScalingInfo);
            configureScalingPolicies(dbTableAutoScalingInfo);
        }
        list();
    }

    public void delete() {
        LOGGER.info("Remove DB Auto-scaling...");
        List<DBTableAutoScalingInfo> dbTableAutoScalingInfos = parseDBSettings(config);
        for (DBTableAutoScalingInfo dbTableAutoScalingInfo : dbTableAutoScalingInfos) {
            LOGGER.info("Removing DB Auto-Scaling for {} - {}", dbTableAutoScalingInfo.getName(), dbTableAutoScalingInfo);
            deleteScalingPolicy(dbTableAutoScalingInfo);
            deregisterScalableTarget(dbTableAutoScalingInfo);
        }
    }

    public void list() {
        LOGGER.info("Listing DB Auto-scaling...");
        List<DBTableAutoScalingInfo> dbTableAutoScalingInfos = parseDBSettings(config);
        for (DBTableAutoScalingInfo dbTableAutoScalingInfo : dbTableAutoScalingInfos) {
            LOGGER.info("Listing DB Auto-Scaling for {}", dbTableAutoScalingInfo.getName());
            AwsDBUtil.describeScalable(dbTableAutoScalingInfo.getName(), amazonDynamoDB, awsApplicationAutoScalingClient, amazonCloudWatchClient);
            AwsDBUtil.showTableAutoscalingInfo(dbTableAutoScalingInfo.getName(), amazonDynamoDB);
        }
        AwsDBUtil.listAllTableCapacityUnits(amazonDynamoDB);
        while (true) {
//            AwsDBUtil.listTableCapacityUnits("dsb_isin", amazonDynamoDB);
            AwsDBUtil.listTableCapacityUnits("markets", amazonDynamoDB);
            AwsDBUtil.listTableCapacityUnits("firds_ref_data", amazonDynamoDB);
            AwsDBUtil.listTableCapacityUnits("last_update", amazonDynamoDB);
            AwsDBUtil.listTableCapacityUnits("underlying", amazonDynamoDB);
            HelperUtil.sleep(30000);
        }
    }

    public void registerScalableTargets(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        try {
            LOGGER.info("Register ScalableTargets for {}...", dbTableAutoScalingInfo.getName());
            dbTableAS.registerScalableTargets(dbTableAutoScalingInfo);
        } catch (Exception e) {
            LOGGER.warn("Failed registering ScalableTarget for {} - {}", dbTableAutoScalingInfo.getName(), e.getMessage());
        }
    }

    public void configureScalingPolicies(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        try {
            LOGGER.info("Configure ScalingPolicies for {}...", dbTableAutoScalingInfo.getName());
                dbTableAS.configureScalingPolicies(dbTableAutoScalingInfo);
            } catch (Exception e) {
            LOGGER.warn("Failed configuring ScalingPolicy {} - {}", dbTableAutoScalingInfo.getName(), e.getMessage());
        }
    }

    public void deregisterScalableTarget(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        try {
            LOGGER.info("Unregister ScalableTarget for {}...", dbTableAutoScalingInfo.getName());
            dbTableAS.deregisterScalableTargets(dbTableAutoScalingInfo);
        } catch (Exception e) {
            LOGGER.warn("Failed unregistering ScalableTarget for {} - {}", dbTableAutoScalingInfo.getName(), e.getMessage());
        }
    }

    public void deleteScalingPolicy(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        try {
            LOGGER.info("Delete ScalingPolicy for {}...", dbTableAutoScalingInfo.getName());
            dbTableAS.configureScalingPolicies(dbTableAutoScalingInfo);
        } catch (Exception e) {
            LOGGER.warn("Failed deleting ScalingPolicy {} - {}", dbTableAutoScalingInfo.getName(), e.getMessage());
        }
    }

    private List<DBTableAutoScalingInfo> parseDBSettings(Config config) {
        String[] names = config.getDbAutoscalingLionNames().split(Constants.CONFIG_DELIM);
        String[] minWcus = config.getDbAutoscalingLionMinWcus().split(Constants.CONFIG_DELIM);
        String[] maxWcus = config.getDbAutoscalingLionMaxWcus().split(Constants.CONFIG_DELIM);
        String[] minRcus = config.getDbAutoscalingLionMinRcus().split(Constants.CONFIG_DELIM);
        String[] maxRcus = config.getDbAutoscalingLionMaxRcus().split(Constants.CONFIG_DELIM);
        String[] targetUtilizations = config.getDbAutoscalingLionTargetUtilizations().split(Constants.CONFIG_DELIM);
        String[] scaleInCooldowns = config.getDbAutoscalingLionScaleInCooldowns().split(Constants.CONFIG_DELIM);
        String[] scaleOutCooldowns = config.getDbAutoscalingLionScaleOutCooldowns().split(Constants.CONFIG_DELIM);

        assert(names.length == minWcus.length);
        assert(minWcus.length == maxWcus.length);
        assert(maxWcus.length == minRcus.length);
        assert(minRcus.length == maxRcus.length);
        assert(maxRcus.length == targetUtilizations.length);
        assert(targetUtilizations.length == scaleInCooldowns.length);
        assert(scaleInCooldowns.length == scaleOutCooldowns.length);

        List<DBTableAutoScalingInfo> output = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            DBTableAutoScalingInfo dbTableAutoScalingInfo = new DBTableAutoScalingInfo(
                    names[i]
                    , Integer.parseInt(minWcus[i])
                    , Integer.parseInt(maxWcus[i])
                    , Integer.parseInt(minRcus[i])
                    , Integer.parseInt(maxRcus[i])
                    , Integer.parseInt(targetUtilizations[i])
                    , Integer.parseInt(scaleInCooldowns[i])
                    , Integer.parseInt(scaleOutCooldowns[i])
            );
            output.add(dbTableAutoScalingInfo);
        }

        return output;
    }

}
