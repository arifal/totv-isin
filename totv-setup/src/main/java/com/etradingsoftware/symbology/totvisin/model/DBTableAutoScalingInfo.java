package com.etradingsoftware.symbology.totvisin.model;

public class DBTableAutoScalingInfo {

    private String name;
    private int minWcu;
    private int maxWcu;
    private int minRcu;
    private int maxRcu;
    private int tagetUtilization;
    private int scaleInCooldown;
    private int scaleOutCooldown;

    public DBTableAutoScalingInfo(String name, int minWcu, int maxWcu, int minRcu, int maxRcu, int tagetUtilization, int scaleInCooldown, int scaleOutCooldown) {
        this.name = name;
        this.minWcu = minWcu;
        this.maxWcu = maxWcu;
        this.minRcu = minRcu;
        this.maxRcu = maxRcu;
        this.tagetUtilization = tagetUtilization;
        this.scaleInCooldown = scaleInCooldown;
        this.scaleOutCooldown = scaleOutCooldown;
    }

    public String getName() {
        return name;
    }

    public int getMinWcu() {
        return minWcu;
    }

    public int getMaxWcu() {
        return maxWcu;
    }

    public int getMinRcu() {
        return minRcu;
    }

    public int getMaxRcu() {
        return maxRcu;
    }

    public int getTagetUtilization() {
        return tagetUtilization;
    }

    public int getScaleInCooldown() {
        return scaleInCooldown;
    }

    public int getScaleOutCooldown() {
        return scaleOutCooldown;
    }

    @Override
    public String toString() {
        return "DBTableAutoScalingInfo{" +
                "name='" + name + '\'' +
                ", minWcu=" + minWcu +
                ", maxWcu=" + maxWcu +
                ", minRcu=" + minRcu +
                ", maxRcu=" + maxRcu +
                ", tagetUtilization=" + tagetUtilization +
                ", scaleInCooldown=" + scaleInCooldown +
                ", scaleOutCooldown=" + scaleOutCooldown +
                '}';
    }

}

