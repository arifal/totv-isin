/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.db;

import com.amazonaws.services.applicationautoscaling.AWSApplicationAutoScaling;
import com.amazonaws.services.applicationautoscaling.AWSApplicationAutoScalingClientBuilder;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.db.DBTableAS;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.common.util.JsonUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.db.model.DsbIsin;
import com.etradingsoftware.symbology.totvisin.db.model.IsinRecord;
import com.etradingsoftware.symbology.totvisin.db.table.DsbIsinTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TotvGhostDB {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvGhostDB.class);

    private static final String DSB_ISIN_TABLE = "dsb_isinx";
    private static final String LAST_UPDATE_TABLE = "last_updatex";

    private Config config;
    private AmazonDynamoDB amazonDynamoDB;
    private DynamoDBMapper dynamoDBMapper;
    private DynamoDBMapperConfig dynamoDBMapperConfig;
    private AWSApplicationAutoScaling awsApplicationAutoScaling;
    private AmazonIdentityManagement amazonIdentityManagement;
    private AmazonCloudWatch amazonCloudWatch;
    private AmazonEC2 amazonEC2;
    private AmazonAutoScaling amazonAutoScaling;

    private DsbIsinTable dsbIsinTable;
    private DBTableAS dbTableAS;

    private int isinOffset = 0;
    private int dateOffset = 0;

    public TotvGhostDB(String configPath) throws ConfigurationException {
        config = new Config(configPath);
        amazonDynamoDB = AmazonDynamoDBClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);
        dynamoDBMapperConfig = DynamoDBMapperConfig.builder().build();
        awsApplicationAutoScaling = AWSApplicationAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonIdentityManagement = AmazonIdentityManagementClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonCloudWatch = AmazonCloudWatchClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonEC2 = AmazonEC2ClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonAutoScaling = AmazonAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();

        dsbIsinTable = new DsbIsinTable(amazonDynamoDB, dynamoDBMapper, dynamoDBMapperConfig);
        dbTableAS = new DBTableAS(awsApplicationAutoScaling, amazonIdentityManagement, config);
    }

    public void start() throws IOException {
//        taskMonitorTableWcuRcuInfo();
//        setupDB();
//        taskPerformTesting();
//        startPerfTest();
//        deleteDB();
//        dsbIsinTable.updateTable("dsb_isin", 600, 600);
//        dsbIsinTable.updateTable("last_update", 600, 600);
//        dsbIsinTable.updateTable("firds_ref_data", 600, 600);
//        dsbIsinTable.updateTable("firds_transparency_data", 600, 600);
//
//        List<String> instanceIds = new ArrayList<>();
//        instanceIds.add("i-055229c4dac610ec7");
//        instanceIds.add("i-0b47ac8dfea29a488");
//
//        for (String instanceId : instanceIds) {
//            try {
//                TerminateInstanceInAutoScalingGroupRequest request = new TerminateInstanceInAutoScalingGroupRequest();
//                request.setInstanceId(instanceId);
//                request.setShouldDecrementDesiredCapacity(true);
//                amazonAutoScaling.terminateInstanceInAutoScalingGroup(request);
//            } catch (Exception e) {
//                LOGGER.warn("Not terminating instance id {} - {}", instanceId, e.getMessage());
//            }
//        }
//        LOGGER.info("{}", amazonDynamoDB.describeTable("dsb_isin"));
//        LOGGER.info("{}", amazonDynamoDB.describeTable("last_update"));

//        AwsDBUtil.describeScalable(DSB_ISIN_TABLE, amazonDynamoDB, awsApplicationAutoScaling, amazonCloudWatch);
//        deleteTables();
//        AmazonAutoScaling amazonAutoScalingClient = AmazonAutoScalingClientBuilder.standard().withRegion("us-west-2").build();
//        AwsAutoScalingUtil.setAutoScalingGroupDesiredCapacity(config.getAutoscalingGhostGroupName(), 14, amazonAutoScalingClient);
//        AwsAutoScalingUtil.setAutoScalingGroupDesiredCapacity(config.getAutoscalingTotoGroupName(), 1, amazonAutoScalingClient);
        System.exit(0);
    }

    private void setupDB() {
        int initialWcu = 1;
        int initialRcu = 1;

        // Create table with 1 rcu and 1 wcu
        dsbIsinTable.createTable(initialWcu, initialRcu);
    }

    private void deleteTables() {
        List<String> tables = new ArrayList<>();
        tables.add("dsb_isin");
        tables.add("firds_ref_data");
        tables.add("firds_transparency_data");
        tables.add("last_process_date");
        tables.add("last_update");
        tables.add("markets");
        tables.add("underlying");
        for (String table : tables) {
            dsbIsinTable.deleteTable(table);
        }
    }

    private void taskMonitorTableWcuRcuInfo() {
        while(true) {
            listTableInfo();
            HelperUtil.sleep(60000);
        }
    }

    private void taskPerformTesting() throws IOException {
//        int minWcu = 1;
//        int maxWcu = 1;
//        int minRcu = 1;
//        int maxRcu = 1;
//        int targetUtilization = 0;
//        int scaleInCooldown = 0;
//        int scaleOutCooldown = 0;
//
//        DBTableAutoScalingInfo dbTableAutoScalingInfo = new DBTableAutoScalingInfo(
//                DSB_ISIN_TABLE
//                , minWcu
//                , maxWcu
//                , minRcu
//                , maxRcu
//                , targetUtilization
//                , scaleInCooldown
//                , scaleOutCooldown
//        );
//
//        listTableNames();
//
//        // Restart test
//        dsbIsinTable.removeAutoScaling(dbTableAutoScalingInfo);
//
//        // Update table with 1 rcu and 1 wcu
//        dsbIsinTable.updateTable(1, 1);
//        listTableNames();
//
//        // Setup autoscaling
//        dsbIsinTable.setupAutoScaling(dbTableAutoScalingInfo);

        startPerfTest();
    }

    private void startPerfTest() throws IOException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1,0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());

//        final int TOTAL_MESSAGE_COUNT = 10000000;
        final int TOTAL_MESSAGE_COUNT = 100000;
        final long DELAY_PER_SEND = 1000;
        final long CHANGE_INTERVAL = 5 * 60 * 1000;

        long start = System.currentTimeMillis();
        int messagePerSend = 2;

        LOGGER.info("Generating {} messages", TOTAL_MESSAGE_COUNT);
        List<DsbIsin> generatedMessages = generateRecords(TOTAL_MESSAGE_COUNT);

        int c = 0;
        int maxC = 12;
        List<Future<DsbIsinDB>> futures = new ArrayList<>();
        while (c <= maxC) {
            long batchStart = System.currentTimeMillis();
            if (System.currentTimeMillis() - start > CHANGE_INTERVAL) {
                start = System.currentTimeMillis();
                if (c >= 6) {
                    messagePerSend /= 2;
                } else {
                    messagePerSend *= 2;
                }
                c++;
            }
            if (c > maxC) {
                break;
            }

            LOGGER.info("Batching {} messages...", messagePerSend);
            List<DsbIsin> messagesToSend = new ArrayList<>();
            for (int i = 0; i < messagePerSend; i++) {
                messagesToSend.add(generatedMessages.remove(0));
            }
            DsbIsinDB dsbIsinDb = new DsbIsinDB(messagesToSend, dynamoDBMapper);
            while (executor.getActiveCount() > 0) {
                HelperUtil.sleep(2);
            }
            futures.add((Future<DsbIsinDB>) executor.submit(dsbIsinDb));

            long batchElapsedTime = System.currentTimeMillis() - batchStart;
            long delayPerSend = batchElapsedTime > 0 ? DELAY_PER_SEND - batchElapsedTime : DELAY_PER_SEND;
            HelperUtil.sleep(delayPerSend);
        }
        LOGGER.info("Finished");
        executor.shutdown();
    }

    private void start_old() throws IOException {
        describeTable(DSB_ISIN_TABLE);
        int rcu = 2;
        int wcu = 2;
        dsbIsinTable.updateTable("", rcu, wcu);
        describeTable(DSB_ISIN_TABLE);
//        dsbIsinTable.deleteTable();
        perfTestSavingToDBv2(rcu, wcu, 1000, 100);

//        perfTestSavingToDBv2(rcu, wcu, 10000, 10);
//        perfTestSavingToDBv2(rcu, wcu, 10000, 100);
//        perfTestSavingToDBv2(rcu, wcu, 10000, 1000);
//        perfTestSavingToDBv2(rcu, wcu, 100000, 10);
//        perfTestSavingToDBv2(rcu, wcu, 100000, 100);
//        perfTestSavingToDBv2(rcu, wcu, 100000, 1000);
//        perfTestSavingToDBv2(rcu, wcu, 100000, 10000);

//        int count = 1;
//        for (int i = 0; i < 8; i++) {
//            perfTestSavingToDB(1, 1, count);
//            perfTestSavingToDB(2, 2, count);
//            perfTestSavingToDB(4, 4, count);
//            perfTestSavingToDB(8, 8, count);
//            perfTestSavingToDB(16, 16, count);
//            perfTestSavingToDB(32, 32, count);
//            perfTestSavingToDB(64, 64, count);
//            count *= 10;
//        }

//        perfTestSavingToDB(1, 1, 1000);
//        perfTestSavingToDB(1, 1, 10000);
//        createTable();
//        listTableNames();
//        deleteTables();
//        for (int i = 0; i < 1; i++) {
//            insertData(i);
//        }
//        QueryResult result = listTableRows();
//        for (Map<String, AttributeValue> item : result.getItems()) {
//            LOGGER.info(item.toString());
//        }

//        ListTablesResult result = dynamoDBAsyncClient.listTables();
//        LOGGER.info(String.valueOf(result.getTableNames()));
    }

    private void perfTestSavingToDBv2(long readCapacityUnits, long writeCapacityUnits, int count, int batchLimit) throws IOException {
        LOGGER.info("=== PERF-TEST Saving {} records to DB. Capacity Units [read: {}, write: {}] ===", count, readCapacityUnits, writeCapacityUnits);
//        dsbIsinTable.deleteTable();
        waitForDbUntil(DSB_ISIN_TABLE, false);
        dsbIsinTable.createTable(readCapacityUnits, writeCapacityUnits);
        waitForDbUntil(DSB_ISIN_TABLE, true);
        describeTable(DSB_ISIN_TABLE);
        List<DsbIsin> records = generateRecords(count);
        List<DsbIsin> batch = new ArrayList<>();
        long start = System.currentTimeMillis();
        for (DsbIsin record : records) {
            batch.add(record);
            if (batch.size() >= batchLimit) {
                insertRecords(batch);
                batch.clear();
            }
        }
        if (batch.size() > 0) {
            insertRecords(batch);
            batch.clear();
        }
        long elapsedTime = System.currentTimeMillis() - start;
        LOGGER.info("*** Total elapsed time for {} records with batchLimit of {} to send is {} ms", count, batchLimit, elapsedTime);
//        deleteRecords(records);
        countRecords(count);
        describeTable(DSB_ISIN_TABLE);
        //deleteTables(DSB_ISIN_TABLE);
    }

    private void perfTestSavingToDB(long readCapacityUnits, long writeCapacityUnits, int count) throws IOException {
        LOGGER.info("=== PERF-TEST Saving {} records to DB. Capacity Units [read: {}, write: {}] ===", count, readCapacityUnits, writeCapacityUnits);
//        dsbIsinTable.deleteTable();
        waitForDbUntil(DSB_ISIN_TABLE, false);
        dsbIsinTable.createTable(readCapacityUnits, writeCapacityUnits);
        waitForDbUntil(DSB_ISIN_TABLE, true);
        List<DsbIsin> records = generateRecords(count);
        insertRecords(records);
//        deleteRecords(records);
        countRecords(count);
    }

    private List<DsbIsin> generateRecords(int count) throws IOException {
        List<DsbIsin> output = new ArrayList<>();

        String isinRecordRaw = FileUtil.readFromFile("src/main/resources/isin-record.sample");
        IsinRecord isinRecord = (IsinRecord) JsonUtil.toJsonObject(isinRecordRaw, IsinRecord.class);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate date = LocalDate.now();
        int dateAddCounter = 0;
        for (int i = 0; i < count; i++) {
            DsbIsin dsbIsin = new DsbIsin();
            dsbIsin.setISIN(isinRecord.getIsin().get("ISIN"));
            dsbIsin.setAssetClass(isinRecord.getHeader().get("AssetClass"));
            dsbIsin.setInstrumentType(isinRecord.getHeader().get("InstrumentType"));
            dsbIsin.setRecords(isinRecord.toString());
            dsbIsin.setISIN(String.format("EZ%010d", i + isinOffset));
            dsbIsin.setDate(dtf.format(date));
            if (i % 30 == 0) {
                date = date.plusDays(1 + dateOffset);
                dateAddCounter += 1;
            }
            if (i % 10000 == 0) {
                LOGGER.info("Record #{}: {}", i, dsbIsin);
            }
            output.add(dsbIsin);
        }

        isinOffset += count;
        dateOffset += dateAddCounter;

        return output;
    }

    private void insertRecords(List<DsbIsin> records) {
        try {
            long start = System.currentTimeMillis();
            List<DynamoDBMapper.FailedBatch> failedBatches = dynamoDBMapper.batchSave(records);
            while (failedBatches != null && failedBatches.size() > 0) {
                LOGGER.warn("Failed saving {} records - {}", failedBatches.size(), failedBatches.toString());
                LOGGER.warn("Failed Batch #1 - {}", failedBatches.get(0).toString());
                LOGGER.info("Retrying {} failed records...", failedBatches.size());
                for (DynamoDBMapper.FailedBatch failedBatch : failedBatches) {
                    for (Map.Entry<String, List<WriteRequest>> entry : failedBatch.getUnprocessedItems().entrySet()) {
                        LOGGER.info("Resending batch: {} records...", entry.getValue().size());
                        dynamoDBMapper.batchSave(entry.getValue());
                    }
                }
                failedBatches = dynamoDBMapper.batchSave(failedBatches);
            }
            long elapsedTime = System.currentTimeMillis() - start;
            LOGGER.info("* Took {} ms to write {} records to DB.", elapsedTime, records.size());
        } catch (Exception e) {
            LOGGER.error("Exception encountered - {}", e.getMessage(), e);
        }
    }

    private void deleteRecords(List<DsbIsin> records) {
        dynamoDBMapper.batchDelete(records);
    }

    private void countRecords(int count) {
        ScanRequest request = new ScanRequest();
        request.setTableName("DSB-ISIN");
        ScanResult result = amazonDynamoDB.scan(request);
        int ctr = 0;
        while (result.getCount() < count && ctr < 10) {
            LOGGER.info("Record count: {} / {}", result.getCount(), count);
            HelperUtil.sleep(1000);
            ctr++;
        }
        LOGGER.info("Record count: {}", result.getCount());
    }

    private void waitForDbUntil(String dbName, boolean exists) {
        final long DELAY = 1000;
        if (exists) {
            while (!dsbIsinTable.tableExists(dbName) || !isTableActive(dbName)) {
                HelperUtil.sleep(DELAY);
            }
        } else {
            while (dsbIsinTable.tableExists(dbName)) {
                HelperUtil.sleep(DELAY);
            }
        }
    }

    private boolean isTableActive(String dbName) {
        return TableStatus.ACTIVE.toString().equals(amazonDynamoDB.describeTable(dbName).getTable().getTableStatus());
    }

    private DescribeTableResult describeTable(String tableName) {
        return amazonDynamoDB.describeTable(tableName);
//        LOGGER.info("Describe Table: {}", result.toString());
    }

    private void listTableInfo() {
        ListTablesResult result = amazonDynamoDB.listTables();
//        LOGGER.info("Tables: {}", result.getTableNames());
        for (String tableName : result.getTableNames()) {
            DescribeTableResult describeTableResult = describeTable(tableName);
            LOGGER.info("Table: {}, RCU: {}, WCU: {}"
                    , tableName
                    , describeTableResult.getTable().getProvisionedThroughput().getReadCapacityUnits()
                    , describeTableResult.getTable().getProvisionedThroughput().getWriteCapacityUnits()
            );
        }
    }

    private void listTableNames() {
        ListTablesResult result = amazonDynamoDB.listTables();
        LOGGER.info("Tables: {}", result.getTableNames());
        for (String tableName : result.getTableNames()) {
            DescribeTableResult describeTableResult = describeTable(tableName);
            LOGGER.info("DescribeTable result: {}", describeTableResult);
        }
    }

    private QueryResult listTableRows() {
        QueryRequest request = new QueryRequest();
        request.setTableName("totvSampleTable");
        request.setKeyConditionExpression("#Age BETWEEN :10 AND :15");
        return amazonDynamoDB.query(request);
    }

    private PutItemResult insertData2(int index) {
        String data = "{\"Header\":{\"AssetClass\":\"Commodities\",\"InstrumentType\":\"Option\",\"UseCase\":\"Option\",\"Level\":\"InstRefDataReporting\"},\"Attributes\":{\"NotionalCurrency\":\"EUR\",\"ExpiryDate\":\"2200-03-30\",\"OptionType\":\"OPTL\",\"OptionExerciseStyle\":\"AMER\",\"ValuationMethodorTrigger\":\"Digital Barrier\",\"DeliveryType\":\"OPTL\",\"BaseProduct\":\"OEST\",\"TransactionType\":\"OPTN\",\"FinalPriceType\":\"OTHR\",\"ReferenceRate\":\"ELECTRICITY-DAY-AHEAD-HOURLY-POWERNEXT\",\"PriceMultiplier\":$priceMultiplier,\"SubProduct\":\"\",\"AdditionalSubProduct\":\"\"},\"ISIN\":{\"ISIN\":\"EZ64T1DN29X6\",\"Status\":\"New\",\"StatusReason\":\"\",\"LastUpdateDateTime\":\"2017-08-26T17:35:29\"},\"TemplateVersion\":1,\"Derived\":{\"CommodityDerivativeIndicator\":\"TRUE\",\"UnderlyingAssetType\":\"Other\",\"IssuerorOperatoroftheTradingVenueIdentifier\":\"NA\",\"FullName\":\"Commodities Option OEST EUR 22000330\",\"ShortName\":\"NA/O OEST OPTL EUR 22000330\",\"ClassificationType\":\"HTMHGE\"}}";
        data = data.replace("$priceMultiplier", String.valueOf(index));

//        Map<String, AttributeValue> item = new HashMap<>();
//        item.put("Name", new AttributeValue("John{0,number,#}"));
//        item.put("Age", new AttributeValue(String.valueOf(index)));

        PutItemRequest request = new PutItemRequest();
        request.setTableName("totvSampleTable");
//        request.setItem(item);
        request.setReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL);
        return amazonDynamoDB.putItem(request);
    }

    class DsbIsinDB implements Runnable {

        private final Logger LOGGER = LoggerFactory.getLogger(DsbIsinDB.class);

        private List<DsbIsin> records;
        private DynamoDBMapper dynamoDBMapper;

        private DsbIsinDB(List<DsbIsin> records, DynamoDBMapper dynamoDBMapper) {
            this.records = records;
            this.dynamoDBMapper = dynamoDBMapper;
        }

        @Override
        public void run() {
            try {
                long start = System.currentTimeMillis();
                LOGGER.info(" Sending {} messages...", records.size());
                List<DynamoDBMapper.FailedBatch> failedBatches = dynamoDBMapper.batchSave(records);
                if (failedBatches.size() > 0) {
                    LOGGER.warn("Failed saving {} batches...", failedBatches.size());
                }
                for (DynamoDBMapper.FailedBatch batch : failedBatches) {
                    LOGGER.warn("Failed saving {} records from batch - {}", batch.getUnprocessedItems().size(), batch.getException());
                }
                long elapsedTime = System.currentTimeMillis() - start;
                LOGGER.info("* Took {} ms to write {} records to DB.", elapsedTime, records.size());
            } catch (Exception e) {
                LOGGER.error("Exception encountered - {}", e.getMessage(), e);
            }
        }

    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Illegal number of arguments. Usage: <configFile>");
            System.exit(1);
        }

        try {
            String configPath = args[0];
            TotvGhostDB app = new TotvGhostDB(configPath);
            app.start();
        } catch (Exception e) {
            LOGGER.error("Exception encountered - {}", e.getMessage(), e);
        }
    }

}
