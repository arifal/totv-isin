package com.etradingsoftware.symbology.totvisin.db.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

@DynamoDBTable(tableName = "DSB_ISIN_TEST")
public class DsbIsin {
    private String ISIN;
    private String date;
    private String assetClass;
    private String instrumentType;
    private String records;

    public DsbIsin(String ISIN, String date, String assetClass, String instrumentType, String records) {
        this.ISIN = ISIN;
        this.date = date;
        this.assetClass = assetClass;
        this.instrumentType = instrumentType;
        this.records = records;
    }

    public DsbIsin() {}

    @DynamoDBHashKey(attributeName = "ISIN")
    public String getISIN() {
        return ISIN;
    }

    public void setISIN(String ISIN) {
        this.ISIN = ISIN;
    }

    @DynamoDBRangeKey(attributeName = "Date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @DynamoDBIndexRangeKey(localSecondaryIndexName = "AssetClassIndex")
    public String getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(String assetClass) {
        this.assetClass = assetClass;
    }

    @DynamoDBIndexRangeKey(localSecondaryIndexName = "InstrumentTypeIndex")
    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    @DynamoDBAttribute(attributeName = "records")
    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "DsbIsin{" +
                "ISIN='" + ISIN + '\'' +
                ", date='" + date + '\'' +
                ", assetClass='" + assetClass + '\'' +
                ", instrumentType='" + instrumentType + '\'' +
                ", records='" + records + '\'' +
                '}';
    }

}
