/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.constant;

public final class Constants {

    public static final String ENCODING = "UTF-8";

    public static final String AUTO_SCALING_GROUP_TAG_RESOURCE_TYPE = "auto-scaling-group";
    public static final String AUTO_SCALING_GROUP_TAG_ROLE_KEY = "Role";

}
