package com.etradingsoftware.symbology.totvisin.db.table;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DeleteTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.UpdateTableRequest;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.TotvDBAutoScaling;
import com.etradingsoftware.symbology.totvisin.db.model.DsbIsin;
import com.etradingsoftware.symbology.totvisin.model.DBTableAutoScalingInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DsbIsinTable {

    public static final String DSB_ISIN = "DSB_ISIN_TEST";

    private static final Logger LOGGER = LoggerFactory.getLogger(DsbIsinTable.class);

    private DynamoDBMapper dynamoDBMapper;
    private AmazonDynamoDB amazonDynamoDBClient;
    private DynamoDBMapperConfig dynamoDBMapperConfig;
    private TotvDBAutoScaling totvDBAutoScaling;

    public DsbIsinTable(AmazonDynamoDB amazonDynamoDBClient, DynamoDBMapper dynamoDBMapper, DynamoDBMapperConfig dynamoDBMapperConfig) {
        this.amazonDynamoDBClient = amazonDynamoDBClient;
        this.dynamoDBMapper = dynamoDBMapper;
        this.dynamoDBMapperConfig = dynamoDBMapperConfig;
    }

    public void createTable(long readCapacityUnits, long writeCapacityUnits) {
//        Collection<AttributeDefinition> attributeDefinitions = new ArrayList<>();
//        attributeDefinitions.add(new AttributeDefinition("Name", "S"));
//        attributeDefinitions.add(new AttributeDefinition("Age", "S"));
//
//        Collection<KeySchemaElement> keySchemaElements = new ArrayList<>();
//        keySchemaElements.add(new KeySchemaElement("Name", KeyType.HASH));
//        keySchemaElements.add(new KeySchemaElement("Age", KeyType.RANGE));
//
//        CreateTableRequest request = new CreateTableRequest();

        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput(readCapacityUnits, writeCapacityUnits);

        CreateTableRequest request = dynamoDBMapper.generateCreateTableRequest(DsbIsin.class, dynamoDBMapperConfig);
        request.setProvisionedThroughput(provisionedThroughput);
        LOGGER.info("Creating table {} (RCU: {}, WCU: {})...", DSB_ISIN, readCapacityUnits, writeCapacityUnits);
        amazonDynamoDBClient.createTable(request);
    }

    public void deleteTable(String tableName) {
        LOGGER.info("Deleting table {}...", tableName);
        if (!tableExists(tableName)) {
            return;
        }

        DeleteTableRequest request = new DeleteTableRequest();
        request.setTableName(tableName);
        amazonDynamoDBClient.deleteTable(request);
    }

    public void updateTable(String tableName, long readCapacityUnits, long writeCapacityUnits) {
        LOGGER.info("Updating table {} (RCU: {}, WCU: {}...", tableName, readCapacityUnits, writeCapacityUnits);
        UpdateTableRequest request = new UpdateTableRequest(
                tableName
                , new ProvisionedThroughput(readCapacityUnits, writeCapacityUnits)
        );
        amazonDynamoDBClient.updateTable(request);
    }

    public void setupAutoScaling(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        totvDBAutoScaling.registerScalableTargets(dbTableAutoScalingInfo);
        totvDBAutoScaling.configureScalingPolicies(dbTableAutoScalingInfo);
    }

    public void removeAutoScaling(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        totvDBAutoScaling.deleteScalingPolicy(dbTableAutoScalingInfo);
        totvDBAutoScaling.deregisterScalableTarget(dbTableAutoScalingInfo);
    }

    public boolean tableExists(String dbName) {
        return amazonDynamoDBClient.listTables().getTableNames().contains(dbName);
    }

}
