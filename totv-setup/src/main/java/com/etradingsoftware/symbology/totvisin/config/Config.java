/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.config;

import com.etradingsoftware.symbology.totvisin.common.config.TotvIsinConfig;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config extends TotvIsinConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    private static final String AWS_REGION = "aws.region";
    private static final String DB_AUTOSCALING_ROLE_NAME = "db_autoscaling.role_name";
    private static final String DB_AUTOSCALING_ROLE_PATH = "db_autoscaling.role_path";
    private static final String DB_AUTOSCALING_POLICY_NAME = "db_autoscaling.policy_name";
    private static final String DB_AUTOSCALING_POLICY_PATH = "db_autoscaling.policy_path";
    private static final String AUTOSCALING_CUCKOO_MAN_CONFIG_NAME = "autoscaling.cuckoo_man.config_name";
    private static final String AUTOSCALING_CUCKOO_MAN_GROUP_NAME = "autoscaling.cuckoo_man.group_name";
    private static final String AUTOSCALING_CUCKOO_MAN_KEY_NAME = "autoscaling.cuckoo_man.key_name";
    private static final String AUTOSCALING_CUCKOO_MAN_INSTANCE_TYPE = "autoscaling.cuckoo_man.instance_type";
    private static final String AUTOSCALING_CUCKOO_MAN_IMAGE_ID = "autoscaling.cuckoo_man.image_id";
    private static final String AUTOSCALING_CUCKOO_MAN_SECURITY_GROUPS = "autoscaling.cuckoo_man.security_groups";
    private static final String AUTOSCALING_CUCKOO_MAN_INSTANCE_PROFILE = "autoscaling.cuckoo_man.instance_profile";
    private static final String AUTOSCALING_CUCKOO_MAN_AVAILABILITY_ZONES = "autoscaling.cuckoo_man.availability_zones";
    private static final String AUTOSCALING_CUCKOO_MAN_VPC_ZONE_IDENTIFIER = "autoscaling.cuckoo_man.vpc_zone_identifier";
    private static final String AUTOSCALING_CUCKOO_MAN_TAG_ROLE = "autoscaling.cuckoo_man.tag_role";
    private static final String AUTOSCALING_CUCKOO_MAN_TERMINATION_POLICIES = "autoscaling.cuckoo_man.termination_policies";
    private static final String AUTOSCALING_CUCKOO_MAN_MIN_SIZE = "autoscaling.cuckoo_man.min_size";
    private static final String AUTOSCALING_CUCKOO_MAN_MAX_SIZE = "autoscaling.cuckoo_man.max_size";
    private static final String AUTOSCALING_CUCKOO_MAN_DESIRED_CAPACITY = "autoscaling.cuckoo_man.desired_capacity";
    private static final String AUTOSCALING_CUCKOO_MAN_PROTECTED_FROM_SCALE_IN = "autoscaling.cuckoo_man.protected_from_scale_in";
    private static final String AUTOSCALING_CUCKOO_MAN_CLOUD_INIT_PATH = "autoscaling.cuckoo_man.cloud_init_path";
    private static final String AUTOSCALING_GHOST_CONFIG_NAME = "autoscaling.ghost.config_name";
    private static final String AUTOSCALING_GHOST_GROUP_NAME = "autoscaling.ghost.group_name";
    private static final String AUTOSCALING_GHOST_KEY_NAME = "autoscaling.ghost.key_name";
    private static final String AUTOSCALING_GHOST_INSTANCE_TYPE = "autoscaling.ghost.instance_type";
    private static final String AUTOSCALING_GHOST_IMAGE_ID = "autoscaling.ghost.image_id";
    private static final String AUTOSCALING_GHOST_SECURITY_GROUPS = "autoscaling.ghost.security_groups";
    private static final String AUTOSCALING_GHOST_INSTANCE_PROFILE = "autoscaling.ghost.instance_profile";
    private static final String AUTOSCALING_GHOST_AVAILABILITY_ZONES = "autoscaling.ghost.availability_zones";
    private static final String AUTOSCALING_GHOST_VPC_ZONE_IDENTIFIER = "autoscaling.ghost.vpc_zone_identifier";
    private static final String AUTOSCALING_GHOST_TAG_ROLE = "autoscaling.ghost.tag_role";
    private static final String AUTOSCALING_GHOST_TERMINATION_POLICIES = "autoscaling.ghost.termination_policies";
    private static final String AUTOSCALING_GHOST_MIN_SIZE = "autoscaling.ghost.min_size";
    private static final String AUTOSCALING_GHOST_MAX_SIZE = "autoscaling.ghost.max_size";
    private static final String AUTOSCALING_GHOST_DESIRED_CAPACITY = "autoscaling.ghost.desired_capacity";
    private static final String AUTOSCALING_GHOST_PROTECTED_FROM_SCALE_IN = "autoscaling.ghost.protected_from_scale_in";
    private static final String AUTOSCALING_GHOST_CLOUD_INIT_PATH = "autoscaling.ghost.cloud_init_path";
    private static final String AUTOSCALING_TOTO_CONFIG_NAME = "autoscaling.toto.config_name";
    private static final String AUTOSCALING_TOTO_GROUP_NAME = "autoscaling.toto.group_name";
    private static final String AUTOSCALING_TOTO_KEY_NAME = "autoscaling.toto.key_name";
    private static final String AUTOSCALING_TOTO_INSTANCE_TYPE = "autoscaling.toto.instance_type";
    private static final String AUTOSCALING_TOTO_IMAGE_ID = "autoscaling.toto.image_id";
    private static final String AUTOSCALING_TOTO_SECURITY_GROUPS = "autoscaling.toto.security_groups";
    private static final String AUTOSCALING_TOTO_INSTANCE_PROFILE = "autoscaling.toto.instance_profile";
    private static final String AUTOSCALING_TOTO_AVAILABILITY_ZONES = "autoscaling.toto.availability_zones";
    private static final String AUTOSCALING_TOTO_VPC_ZONE_IDENTIFIER = "autoscaling.toto.vpc_zone_identifier";
    private static final String AUTOSCALING_TOTO_TAG_ROLE = "autoscaling.toto.tag_role";
    private static final String AUTOSCALING_TOTO_TERMINATION_POLICIES = "autoscaling.toto.termination_policies";
    private static final String AUTOSCALING_TOTO_MIN_SIZE = "autoscaling.toto.min_size";
    private static final String AUTOSCALING_TOTO_MAX_SIZE = "autoscaling.toto.max_size";
    private static final String AUTOSCALING_TOTO_DESIRED_CAPACITY = "autoscaling.toto.desired_capacity";
    private static final String AUTOSCALING_TOTO_PROTECTED_FROM_SCALE_IN = "autoscaling.toto.protected_from_scale_in";
    private static final String AUTOSCALING_TOTO_CLOUD_INIT_PATH = "autoscaling.toto.cloud_init_path";
    private static final String DB_AUTOSCALING_LION_NAMES = "db_autoscaling.lion.names";
    private static final String DB_AUTOSCALING_LION_MIN_WCUS = "db_autoscaling.lion.min_wcus";
    private static final String DB_AUTOSCALING_LION_MAX_WCUS = "db_autoscaling.lion.max_wcus";
    private static final String DB_AUTOSCALING_LION_MIN_RCUS = "db_autoscaling.lion.min_rcus";
    private static final String DB_AUTOSCALING_LION_MAX_RCUS = "db_autoscaling.lion.max_rcus";
    private static final String DB_AUTOSCALING_LION_TARGET_UTILIZATIONS = "db_autoscaling.lion.target_utilizations";
    private static final String DB_AUTOSCALING_LION_SCALE_IN_COOLDOWNS = "db_autoscaling.lion.scale_in_cooldowns";
    private static final String DB_AUTOSCALING_LION_SCALE_OUT_COOLDOWNS = "db_autoscaling.lion.scale_out_cooldowns";
    private static final String AUTOSCALING_TAG_ROLE_KEY = "autoscaling.tag.role_key";
    private static final String AUTOSCALING_TAG_RESOURCE_TYPE = "autoscaling.tag.resource_type";

    static {
        addString(AWS_REGION, true);
        addString(DB_AUTOSCALING_ROLE_NAME, true);
        addString(DB_AUTOSCALING_ROLE_PATH, true);
        addString(DB_AUTOSCALING_POLICY_NAME, true);
        addString(DB_AUTOSCALING_POLICY_PATH, true);
        addString(AUTOSCALING_CUCKOO_MAN_CONFIG_NAME, true);
        addString(AUTOSCALING_CUCKOO_MAN_GROUP_NAME, true);
        addString(AUTOSCALING_CUCKOO_MAN_KEY_NAME, true);
        addString(AUTOSCALING_CUCKOO_MAN_INSTANCE_TYPE, true);
        addString(AUTOSCALING_CUCKOO_MAN_IMAGE_ID, true);
        addString(AUTOSCALING_CUCKOO_MAN_SECURITY_GROUPS, true);
        addString(AUTOSCALING_CUCKOO_MAN_INSTANCE_PROFILE, true);
        addString(AUTOSCALING_CUCKOO_MAN_AVAILABILITY_ZONES, false);
        addString(AUTOSCALING_CUCKOO_MAN_VPC_ZONE_IDENTIFIER, false);
        addString(AUTOSCALING_CUCKOO_MAN_TAG_ROLE, true);
        addString(AUTOSCALING_CUCKOO_MAN_TERMINATION_POLICIES, true);
        addInt(AUTOSCALING_CUCKOO_MAN_MIN_SIZE, true);
        addInt(AUTOSCALING_CUCKOO_MAN_MAX_SIZE, true);
        addInt(AUTOSCALING_CUCKOO_MAN_DESIRED_CAPACITY, true);
        addBoolean(AUTOSCALING_CUCKOO_MAN_PROTECTED_FROM_SCALE_IN, true);
        addString(AUTOSCALING_CUCKOO_MAN_CLOUD_INIT_PATH, true);
        addString(AUTOSCALING_GHOST_CONFIG_NAME, true);
        addString(AUTOSCALING_GHOST_GROUP_NAME, true);
        addString(AUTOSCALING_GHOST_KEY_NAME, true);
        addString(AUTOSCALING_GHOST_INSTANCE_TYPE, true);
        addString(AUTOSCALING_GHOST_IMAGE_ID, true);
        addString(AUTOSCALING_GHOST_SECURITY_GROUPS, true);
        addString(AUTOSCALING_GHOST_INSTANCE_PROFILE, true);
        addString(AUTOSCALING_GHOST_AVAILABILITY_ZONES, false);
        addString(AUTOSCALING_GHOST_VPC_ZONE_IDENTIFIER, false);
        addString(AUTOSCALING_GHOST_TAG_ROLE, true);
        addString(AUTOSCALING_GHOST_TERMINATION_POLICIES, true);
        addInt(AUTOSCALING_GHOST_MIN_SIZE, true);
        addInt(AUTOSCALING_GHOST_MAX_SIZE, true);
        addInt(AUTOSCALING_GHOST_DESIRED_CAPACITY, true);
        addBoolean(AUTOSCALING_GHOST_PROTECTED_FROM_SCALE_IN, true);
        addString(AUTOSCALING_GHOST_CLOUD_INIT_PATH, true);
        addString(AUTOSCALING_TOTO_CONFIG_NAME, true);
        addString(AUTOSCALING_TOTO_GROUP_NAME, true);
        addString(AUTOSCALING_TOTO_KEY_NAME, true);
        addString(AUTOSCALING_TOTO_INSTANCE_TYPE, true);
        addString(AUTOSCALING_TOTO_IMAGE_ID, true);
        addString(AUTOSCALING_TOTO_SECURITY_GROUPS, true);
        addString(AUTOSCALING_TOTO_INSTANCE_PROFILE, true);
        addString(AUTOSCALING_TOTO_AVAILABILITY_ZONES, false);
        addString(AUTOSCALING_TOTO_VPC_ZONE_IDENTIFIER, false);
        addString(AUTOSCALING_TOTO_TAG_ROLE, true);
        addString(AUTOSCALING_TOTO_TERMINATION_POLICIES, true);
        addInt(AUTOSCALING_TOTO_MIN_SIZE, true);
        addInt(AUTOSCALING_TOTO_MAX_SIZE, true);
        addInt(AUTOSCALING_TOTO_DESIRED_CAPACITY, true);
        addBoolean(AUTOSCALING_TOTO_PROTECTED_FROM_SCALE_IN, true);
        addString(AUTOSCALING_TOTO_CLOUD_INIT_PATH, true);
        addString(DB_AUTOSCALING_LION_NAMES, true);
        addString(DB_AUTOSCALING_LION_MIN_WCUS, true);
        addString(DB_AUTOSCALING_LION_MAX_WCUS, true);
        addString(DB_AUTOSCALING_LION_MIN_RCUS, true);
        addString(DB_AUTOSCALING_LION_MAX_RCUS, true);
        addString(DB_AUTOSCALING_LION_TARGET_UTILIZATIONS, true);
        addString(DB_AUTOSCALING_LION_SCALE_IN_COOLDOWNS, true);
        addString(DB_AUTOSCALING_LION_SCALE_OUT_COOLDOWNS, true);
        addString(AUTOSCALING_TAG_ROLE_KEY, true);
        addString(AUTOSCALING_TAG_RESOURCE_TYPE, true);
    }

    public Config(String configPath) throws ConfigurationException {
        LOGGER.info("Loading application config file: {}", configPath);
        loadFromConfigFile(configPath);
    }

    public String getAwsRegion() {
        return getString(AWS_REGION);
    }

    public String getDbAutoscalingRoleName() {
        return getString(DB_AUTOSCALING_ROLE_NAME);
    }

    public String getDbAutoscalingRolePath() {
        return getString(DB_AUTOSCALING_ROLE_PATH);
    }

    public String getDbAutoscalingPolicyName() {
        return getString(DB_AUTOSCALING_POLICY_NAME);
    }

    public String getDbAutoscalingPolicyPath() {
        return getString(DB_AUTOSCALING_POLICY_PATH);
    }

    public String getAutoscalingCuckooManConfigName() {
        return getString(AUTOSCALING_CUCKOO_MAN_CONFIG_NAME);
    }

    public String getAutoscalingCuckooManGroupName() {
        return getString(AUTOSCALING_CUCKOO_MAN_GROUP_NAME);
    }

    public String getAutoscalingCuckooManKeyName() {
        return getString(AUTOSCALING_CUCKOO_MAN_KEY_NAME);
    }

    public String getAutoscalingCuckooManInstanceType() {
        return getString(AUTOSCALING_CUCKOO_MAN_INSTANCE_TYPE);
    }

    public String getAutoscalingCuckooManImageId() {
        return getString(AUTOSCALING_CUCKOO_MAN_IMAGE_ID);
    }

    public String getAutoscalingCuckooManSecurityGroups() {
        return getString(AUTOSCALING_CUCKOO_MAN_SECURITY_GROUPS);
    }

    public String getAutoscalingCuckooManInstanceProfile() {
        return getString(AUTOSCALING_CUCKOO_MAN_INSTANCE_PROFILE);
    }

    public String getAutoscalingCuckooManAvailabilityZones() {
        return getString(AUTOSCALING_CUCKOO_MAN_AVAILABILITY_ZONES);
    }

    public String getAutoscalingCuckooManVpcZoneIdentifier() {
        return getString(AUTOSCALING_CUCKOO_MAN_VPC_ZONE_IDENTIFIER);
    }

    public String getAutoscalingCuckooManTagRole() {
        return getString(AUTOSCALING_CUCKOO_MAN_TAG_ROLE);
    }

    public String getAutoscalingCuckooManTerminationPolicies() {
        return getString(AUTOSCALING_CUCKOO_MAN_TERMINATION_POLICIES);
    }

    public Integer getAutoscalingCuckooManMinSize() {
        return getInt(AUTOSCALING_CUCKOO_MAN_MIN_SIZE);
    }

    public Integer getAutoscalingCuckooManMaxSize() {
        return getInt(AUTOSCALING_CUCKOO_MAN_MAX_SIZE);
    }

    public Integer getAutoscalingCuckooManDesiredCapacity() {
        return getInt(AUTOSCALING_CUCKOO_MAN_DESIRED_CAPACITY);
    }

    public Boolean getAutoscalingCuckooManProtectedFromScaleIn() {
        return getBoolean(AUTOSCALING_CUCKOO_MAN_PROTECTED_FROM_SCALE_IN);
    }

    public String getAutoscalingCuckooManCloudInitPath() {
        return getString(AUTOSCALING_CUCKOO_MAN_CLOUD_INIT_PATH);
    }

    public String getAutoscalingGhostConfigName() {
        return getString(AUTOSCALING_GHOST_CONFIG_NAME);
    }

    public String getAutoscalingGhostGroupName() {
        return getString(AUTOSCALING_GHOST_GROUP_NAME);
    }

    public String getAutoscalingGhostKeyName() {
        return getString(AUTOSCALING_GHOST_KEY_NAME);
    }

    public String getAutoscalingGhostInstanceType() {
        return getString(AUTOSCALING_GHOST_INSTANCE_TYPE);
    }

    public String getAutoscalingGhostImageId() {
        return getString(AUTOSCALING_GHOST_IMAGE_ID);
    }

    public String getAutoscalingGhostSecurityGroups() {
        return getString(AUTOSCALING_GHOST_SECURITY_GROUPS);
    }

    public String getAutoscalingGhostInstanceProfile() {
        return getString(AUTOSCALING_GHOST_INSTANCE_PROFILE);
    }

    public String getAutoscalingGhostAvailabilityZones() {
        return getString(AUTOSCALING_GHOST_AVAILABILITY_ZONES);
    }

    public String getAutoscalingGhostVpcZoneIdentifier() {
        return getString(AUTOSCALING_GHOST_VPC_ZONE_IDENTIFIER);
    }

    public String getAutoscalingGhostTagRole() {
        return getString(AUTOSCALING_GHOST_TAG_ROLE);
    }

    public String getAutoscalingGhostTerminationPolicies() {
        return getString(AUTOSCALING_GHOST_TERMINATION_POLICIES);
    }

    public Integer getAutoscalingGhostMinSize() {
        return getInt(AUTOSCALING_GHOST_MIN_SIZE);
    }

    public Integer getAutoscalingGhostMaxSize() {
        return getInt(AUTOSCALING_GHOST_MAX_SIZE);
    }

    public Integer getAutoscalingGhostDesiredCapacity() {
        return getInt(AUTOSCALING_GHOST_DESIRED_CAPACITY);
    }

    public Boolean getAutoscalingGhostProtectedFromScaleIn() {
        return getBoolean(AUTOSCALING_GHOST_PROTECTED_FROM_SCALE_IN);
    }

    public String getAutoscalingGhostCloudInitPath() {
        return getString(AUTOSCALING_GHOST_CLOUD_INIT_PATH);
    }

    public String getAutoscalingTotoConfigName() {
        return getString(AUTOSCALING_TOTO_CONFIG_NAME);
    }

    public String getAutoscalingTotoGroupName() {
        return getString(AUTOSCALING_TOTO_GROUP_NAME);
    }

    public String getAutoscalingTotoKeyName() {
        return getString(AUTOSCALING_TOTO_KEY_NAME);
    }

    public String getAutoscalingTotoInstanceType() {
        return getString(AUTOSCALING_TOTO_INSTANCE_TYPE);
    }

    public String getAutoscalingTotoImageId() {
        return getString(AUTOSCALING_TOTO_IMAGE_ID);
    }

    public String getAutoscalingTotoSecurityGroups() {
        return getString(AUTOSCALING_TOTO_SECURITY_GROUPS);
    }

    public String getAutoscalingTotoInstanceProfile() {
        return getString(AUTOSCALING_TOTO_INSTANCE_PROFILE);
    }

    public String getAutoscalingTotoAvailabilityZones() {
        return getString(AUTOSCALING_TOTO_AVAILABILITY_ZONES);
    }

    public String getAutoscalingTotoVpcZoneIdentifier() {
        return getString(AUTOSCALING_TOTO_VPC_ZONE_IDENTIFIER);
    }

    public String getAutoscalingTotoTagRole() {
        return getString(AUTOSCALING_TOTO_TAG_ROLE);
    }

    public String getAutoscalingTotoTerminationPolicies() {
        return getString(AUTOSCALING_TOTO_TERMINATION_POLICIES);
    }

    public Integer getAutoscalingTotoMinSize() {
        return getInt(AUTOSCALING_TOTO_MIN_SIZE);
    }

    public Integer getAutoscalingTotoMaxSize() {
        return getInt(AUTOSCALING_TOTO_MAX_SIZE);
    }

    public Integer getAutoscalingTotoDesiredCapacity() {
        return getInt(AUTOSCALING_TOTO_DESIRED_CAPACITY);
    }

    public Boolean getAutoscalingTotoProtectedFromScaleIn() {
        return getBoolean(AUTOSCALING_TOTO_PROTECTED_FROM_SCALE_IN);
    }

    public String getAutoscalingTotoCloudInitPath() {
        return getString(AUTOSCALING_TOTO_CLOUD_INIT_PATH);
    }

    public String getDbAutoscalingLionNames() {
        return getString(DB_AUTOSCALING_LION_NAMES);
    }

    public String getDbAutoscalingLionMinWcus() {
        return getString(DB_AUTOSCALING_LION_MIN_WCUS);
    }

    public String getDbAutoscalingLionMaxWcus() {
        return getString(DB_AUTOSCALING_LION_MAX_WCUS);
    }

    public String getDbAutoscalingLionMinRcus() {
        return getString(DB_AUTOSCALING_LION_MIN_RCUS);
    }

    public String getDbAutoscalingLionMaxRcus() {
        return getString(DB_AUTOSCALING_LION_MAX_RCUS);
    }

    public String getDbAutoscalingLionTargetUtilizations() {
        return getString(DB_AUTOSCALING_LION_TARGET_UTILIZATIONS);
    }

    public String getDbAutoscalingLionScaleInCooldowns() {
        return getString(DB_AUTOSCALING_LION_SCALE_IN_COOLDOWNS);
    }

    public String getDbAutoscalingLionScaleOutCooldowns() {
        return getString(DB_AUTOSCALING_LION_SCALE_OUT_COOLDOWNS);
    }

    public String getAutoscalingTagRoleKey() {
        return getString(AUTOSCALING_TAG_ROLE_KEY);
    }

    public String getAutoscalingTagResourceType() {
        return getString(AUTOSCALING_TAG_RESOURCE_TYPE);
    }

}
