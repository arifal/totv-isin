/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.aws.autoscaling.ec2;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.model.CreateAutoScalingGroupRequest;
import com.amazonaws.services.autoscaling.model.CreateLaunchConfigurationRequest;
import com.amazonaws.services.autoscaling.model.Tag;
import com.amazonaws.util.Base64;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.constant.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CuckooManASG implements EC2AutoScaling {

    private AmazonAutoScaling amazonAutoScalingClient;
    private Config config;

    public CuckooManASG(AmazonAutoScaling amazonAutoScalingClient, Config config) {
        this.amazonAutoScalingClient = amazonAutoScalingClient;
        this.config = config;
    }

    @Override
    public void createLaunchConfig() throws IOException {
        String cloudInit = Base64.encodeAsString(FileUtil.readFromFile(config.getAutoscalingCuckooManCloudInitPath()).getBytes(Constants.ENCODING));

        CreateLaunchConfigurationRequest request = new CreateLaunchConfigurationRequest();
        request.setLaunchConfigurationName(config.getAutoscalingCuckooManConfigName());
        request.setKeyName(config.getAutoscalingCuckooManKeyName());
        request.setInstanceType(config.getAutoscalingCuckooManInstanceType());
        request.setImageId(config.getAutoscalingCuckooManImageId());
        request.setSecurityGroups(HelperUtil.csvToList(config.getAutoscalingCuckooManSecurityGroups()));
        request.setInstanceMonitoring(AwsAutoScalingUtil.generateInstanceMonitoring(true));
        request.setEbsOptimized(false);
        request.setAssociatePublicIpAddress(false);
        request.setIamInstanceProfile(config.getAutoscalingCuckooManInstanceProfile());
        request.setUserData(cloudInit);

        amazonAutoScalingClient.createLaunchConfiguration(request);
    }

    @Override
    public void createAutoScalingGroup() {
        String availabilityZones = config.getAutoscalingCuckooManAvailabilityZones();
        String vpcZoneIdentifier = config.getAutoscalingCuckooManVpcZoneIdentifier();

        List<Tag> tags = new ArrayList<>();
        tags.add(AwsAutoScalingUtil.generateAutoScalingTag(
                config.getAutoscalingCuckooManGroupName()
                , config.getAutoscalingTagResourceType()
                , config.getAutoscalingTagRoleKey()
                , config.getAutoscalingCuckooManTagRole()
        ));

        CreateAutoScalingGroupRequest request = new CreateAutoScalingGroupRequest();
        request.setAutoScalingGroupName(config.getAutoscalingCuckooManGroupName());
        request.setLaunchConfigurationName(config.getAutoscalingCuckooManConfigName());
        if (availabilityZones != null && availabilityZones.trim().length() > 0) {
            request.setAvailabilityZones(HelperUtil.csvToList(availabilityZones));
        }
        if (vpcZoneIdentifier != null && vpcZoneIdentifier.trim().length() > 0) {
            request.setVPCZoneIdentifier(vpcZoneIdentifier);
        }
        request.setMinSize(config.getAutoscalingCuckooManMinSize());
        request.setMaxSize(config.getAutoscalingCuckooManMaxSize());
        request.setDesiredCapacity(config.getAutoscalingCuckooManDesiredCapacity());
        request.setTerminationPolicies(HelperUtil.csvToList(config.getAutoscalingCuckooManTerminationPolicies()));
        request.setTags(tags);
        request.setNewInstancesProtectedFromScaleIn(config.getAutoscalingCuckooManProtectedFromScaleIn());

        amazonAutoScalingClient.createAutoScalingGroup(request);
    }

}
