package com.etradingsoftware.symbology.totvisin.db.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IsinRecord {

    @JsonProperty("Header")
    private Map<String, String> header;
    @JsonProperty("Attributes")
    private Map<String, String> attributes;
    @JsonProperty("ISIN")
    private Map<String, String> isin;
    @JsonProperty("TemplateVersion")
    private String templateVersion;
    @JsonProperty("Derived")
    private Map<String, Object> derived;

    public IsinRecord() {
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Map<String, String> getIsin() {
        return isin;
    }

    public void setIsin(Map<String, String> isin) {
        this.isin = isin;
    }

    public String getTemplateVersion() {
        return templateVersion;
    }

    public void setTemplateVersion(String templateVersion) {
        this.templateVersion = templateVersion;
    }

    public Map<String, Object> getDerived() {
        return derived;
    }

    public void setDerived(Map<String, Object> derived) {
        this.derived = derived;
    }

    @Override
    public String toString() {
        return "IsinRecord{" +
                "header=" + header +
                ", attributes=" + attributes +
                ", isin=" + isin +
                ", templateVersion='" + templateVersion + '\'' +
                ", derived=" + derived +
                '}';
    }

}
