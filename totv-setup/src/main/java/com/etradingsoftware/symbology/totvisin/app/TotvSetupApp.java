/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.app;

import com.etradingsoftware.symbology.totvisin.aws.autoscaling.TotvDBAutoScaling;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.TotvEC2AutoScaling;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TotvSetupApp implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvSetupApp.class);

    private static final String CREATE_EC2_COMMAND = "CREATE_EC2";
    private static final String TERMINATE_EC2_COMMAND = "TERMINATE_EC2";
    private static final String LIST_EC2_COMMAND = "LIST_EC2";

    private static final String CREATE_DB_COMMAND = "CREATE_DB";
    private static final String TERMINATE_DB_COMMAND = "TERMINATE_DB";
    private static final String LIST_DB_COMMAND = "LIST_DB";

    private static final String CREATE_ALL_COMMAND = "CREATE";
    private static final String TERMINATE_ALL_COMMAND = "TERMINATE";
    private static final String LIST_ALL_COMMAND = "LIST";

    private static final String LIST_FAIL = "LISTFAIL"; // TODO: pre-prod code - for testing

    private static final Set<String> COMMANDS;

    static {
        COMMANDS = new HashSet<>();
        COMMANDS.add(CREATE_EC2_COMMAND);
        COMMANDS.add(TERMINATE_EC2_COMMAND);
        COMMANDS.add(LIST_EC2_COMMAND);

        COMMANDS.add(CREATE_DB_COMMAND);
        COMMANDS.add(TERMINATE_DB_COMMAND);
        COMMANDS.add(LIST_DB_COMMAND);

        COMMANDS.add(CREATE_ALL_COMMAND);
        COMMANDS.add(TERMINATE_ALL_COMMAND);
        COMMANDS.add(LIST_ALL_COMMAND);

        COMMANDS.add(LIST_FAIL);
    }

    private Config config;
    private String command;

    private TotvEC2AutoScaling totvEC2AutoScaling;
    private TotvDBAutoScaling totvDBAutoScaling;

    public TotvSetupApp(String configPath, String command) throws ConfigurationException {
        this.config = new Config(configPath);
        this.command = command;

        totvEC2AutoScaling = new TotvEC2AutoScaling(config);
        totvDBAutoScaling = new TotvDBAutoScaling(config);
    }

    public void run() {
        init();

        try {
            LOGGER.info("Started.");

            if (CREATE_ALL_COMMAND.equals(command) || CREATE_EC2_COMMAND.equals(command)) {
                LOGGER.info("Setting up EC2 Auto-scaling...");
                totvEC2AutoScaling.create();
            }
            if (CREATE_ALL_COMMAND.equals(command) || CREATE_DB_COMMAND.equals(command)) {
                LOGGER.info("Setting up DB Auto-scaling...");
                totvDBAutoScaling.create();
            }
            if (TERMINATE_ALL_COMMAND.equals(command) || TERMINATE_EC2_COMMAND.equals(command)) {
                LOGGER.info("Terminate EC2 Auto-scaling...");
                totvEC2AutoScaling.delete();
            }
            if (TERMINATE_ALL_COMMAND.equals(command) || TERMINATE_DB_COMMAND.equals(command)) {
                LOGGER.info("Remove DB Auto-scaling...");
                totvDBAutoScaling.delete();
            }
            if (LIST_ALL_COMMAND.equals(command) || LIST_EC2_COMMAND.equals(command)) {
                LOGGER.info("Listing EC2 Auto-scaling...");
                totvEC2AutoScaling.list();
            }
            if (LIST_ALL_COMMAND.equals(command) || LIST_DB_COMMAND.equals(command)) {
                LOGGER.info("Listing DB Auto-scaling...");
                totvDBAutoScaling.list();
            }
            if (LIST_FAIL.equals(command)) {
                LOGGER.info("Force exit with errors (exitCode: 1)");
                System.exit(1);
            }
        } catch (Exception e) {
            LOGGER.error("Exception encountered - {}", e.getMessage(), e);
        } finally {
            LOGGER.info("Finished.");
        }
    }

    private void init() {
        // Retrieve machine hostname for logging
        String hostname = "UnknownHost";
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            /* do nothing */
        }
        System.setProperty("hostname", hostname);
    }

    public static void main(String[] args) throws ConfigurationException, IOException {
        String command;

        if (args.length < 2) {
            System.out.println("Illegal number of arguments. Usage: <configFile> {CREATE_DB|TERMINATE_DB|LIST_DB|CREATE_EC2|TERMINATE_EC2|LIST_EC2}");
            System.exit(1);
        }

        command = args[1].trim().toUpperCase();
        if (!COMMANDS.contains(command)) {
            System.out.println("Unknown command. Usage: <configFile> {CREATE_DB|TERMINATE_DB|LIST_DB|CREATE_EC2|TERMINATE_EC2|LIST_EC2}");
            System.exit(1);
        }

        ExecutorService service = null;
        try {
            String configPath = args[0];

            TotvSetupApp app = new TotvSetupApp(configPath, command);
            service = Executors.newSingleThreadExecutor();
            service.submit(app);
        } catch (Exception e) {
            LOGGER.error("Exception encountered - {}", e.getMessage(), e);
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }

}
