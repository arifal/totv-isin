package com.etradingsoftware.symbology.totvisin.aws.autoscaling.db;

import com.amazonaws.services.applicationautoscaling.AWSApplicationAutoScaling;
import com.amazonaws.services.applicationautoscaling.model.MetricType;
import com.amazonaws.services.applicationautoscaling.model.ScalableDimension;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.GetRoleResult;
import com.etradingsoftware.symbology.totvisin.common.util.AwsDBUtil;
import com.etradingsoftware.symbology.totvisin.common.util.AwsRoleUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.model.DBTableAutoScalingInfo;

/**
 * DynamoDB Auto-Scaling for DSB-ISIN table
 */
public class DBTableAS {

    private AWSApplicationAutoScaling awsApplicationAutoScalingClient;
    private AmazonIdentityManagement amazonIdentityManagementClient;
    private Config config;

    public DBTableAS(AWSApplicationAutoScaling awsApplicationAutoScalingClient, AmazonIdentityManagement amazonIdentityManagementClient, Config config) {
        this.awsApplicationAutoScalingClient = awsApplicationAutoScalingClient;
        this.amazonIdentityManagementClient = amazonIdentityManagementClient;
        this.config = config;
    }

    public void registerScalableTargets(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        GetRoleResult role = AwsRoleUtil.getRole(config.getDbAutoscalingRoleName(), amazonIdentityManagementClient);

        AwsDBUtil.registerScalableTarget(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableWriteCapacityUnits
                , dbTableAutoScalingInfo.getMinWcu()
                , dbTableAutoScalingInfo.getMaxWcu()
                , role.getRole().getArn()
                , awsApplicationAutoScalingClient
        );

        AwsDBUtil.registerScalableTarget(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableReadCapacityUnits
                , dbTableAutoScalingInfo.getMinRcu()
                , dbTableAutoScalingInfo.getMaxRcu()
                , role.getRole().getArn()
                , awsApplicationAutoScalingClient
        );
    }

    public void configureScalingPolicies(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        AwsDBUtil.configureScalingPolicy(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableWriteCapacityUnits
                , MetricType.DynamoDBWriteCapacityUtilization
                , dbTableAutoScalingInfo.getTagetUtilization()
                , dbTableAutoScalingInfo.getScaleInCooldown()
                , dbTableAutoScalingInfo.getScaleOutCooldown()
                , awsApplicationAutoScalingClient
        );

        AwsDBUtil.configureScalingPolicy(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableReadCapacityUnits
                , MetricType.DynamoDBReadCapacityUtilization
                , dbTableAutoScalingInfo.getTagetUtilization()
                , dbTableAutoScalingInfo.getScaleInCooldown()
                , dbTableAutoScalingInfo.getScaleOutCooldown()
                , awsApplicationAutoScalingClient
        );
    }

    public void deregisterScalableTargets(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        AwsDBUtil.deregisterScalableTarget(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableWriteCapacityUnits
                , awsApplicationAutoScalingClient
        );

        AwsDBUtil.deregisterScalableTarget(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableReadCapacityUnits
                , awsApplicationAutoScalingClient
        );
    }

    public void deleteScalingPolicies(DBTableAutoScalingInfo dbTableAutoScalingInfo) {
        AwsDBUtil.deleteScalingPolicy(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableWriteCapacityUnits
                , awsApplicationAutoScalingClient
        );

        AwsDBUtil.deleteScalingPolicy(
                dbTableAutoScalingInfo.getName()
                , ScalableDimension.DynamodbTableReadCapacityUnits
                , awsApplicationAutoScalingClient
        );
    }

}
