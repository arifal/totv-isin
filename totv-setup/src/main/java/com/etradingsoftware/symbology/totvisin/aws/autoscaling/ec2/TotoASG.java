/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.aws.autoscaling.ec2;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.model.CreateAutoScalingGroupRequest;
import com.amazonaws.services.autoscaling.model.CreateLaunchConfigurationRequest;
import com.amazonaws.services.autoscaling.model.Tag;
import com.amazonaws.util.Base64;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.constant.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TotoASG implements EC2AutoScaling {

    private AmazonAutoScaling amazonAutoScalingClient;
    private Config config;

    public TotoASG(AmazonAutoScaling amazonAutoScalingClient, Config config) {
        this.amazonAutoScalingClient = amazonAutoScalingClient;
        this.config = config;
    }

    @Override
    public void createLaunchConfig() throws IOException {
        String cloudInit = Base64.encodeAsString(FileUtil.readFromFile(config.getAutoscalingTotoCloudInitPath()).getBytes(Constants.ENCODING));

        CreateLaunchConfigurationRequest request = new CreateLaunchConfigurationRequest();
        request.setLaunchConfigurationName(config.getAutoscalingTotoConfigName());
        request.setKeyName(config.getAutoscalingTotoKeyName());
        request.setInstanceType(config.getAutoscalingTotoInstanceType());
        request.setImageId(config.getAutoscalingTotoImageId());
        request.setSecurityGroups(HelperUtil.csvToList(config.getAutoscalingTotoSecurityGroups()));
        request.setInstanceMonitoring(AwsAutoScalingUtil.generateInstanceMonitoring(true));
        request.setEbsOptimized(false);
        request.setAssociatePublicIpAddress(false);
        request.setIamInstanceProfile(config.getAutoscalingTotoInstanceProfile());
        request.setUserData(cloudInit);

        amazonAutoScalingClient.createLaunchConfiguration(request);
    }

    @Override
    public void createAutoScalingGroup() {
        String availabilityZones = config.getAutoscalingTotoAvailabilityZones();
        String vpcZoneIdentifier = config.getAutoscalingTotoVpcZoneIdentifier();

        List<Tag> tags = new ArrayList<>();
        tags.add(AwsAutoScalingUtil.generateAutoScalingTag(
                config.getAutoscalingTotoGroupName()
                , config.getAutoscalingTagResourceType()
                , config.getAutoscalingTagRoleKey()
                , config.getAutoscalingTotoTagRole()
        ));

        CreateAutoScalingGroupRequest request = new CreateAutoScalingGroupRequest();
        request.setAutoScalingGroupName(config.getAutoscalingTotoGroupName());
        request.setLaunchConfigurationName(config.getAutoscalingTotoConfigName());
        if (availabilityZones != null && availabilityZones.trim().length() > 0) {
            request.setAvailabilityZones(HelperUtil.csvToList(availabilityZones));
        }
        if (vpcZoneIdentifier != null && vpcZoneIdentifier.trim().length() > 0) {
            request.setVPCZoneIdentifier(vpcZoneIdentifier);
        }
        request.setMinSize(config.getAutoscalingTotoMinSize());
        request.setMaxSize(config.getAutoscalingTotoMaxSize());
        request.setDesiredCapacity(config.getAutoscalingTotoDesiredCapacity());
        request.setTerminationPolicies(HelperUtil.csvToList(config.getAutoscalingTotoTerminationPolicies()));
        request.setTags(tags);
        request.setNewInstancesProtectedFromScaleIn(config.getAutoscalingTotoProtectedFromScaleIn());

        amazonAutoScalingClient.createAutoScalingGroup(request);
    }

}
