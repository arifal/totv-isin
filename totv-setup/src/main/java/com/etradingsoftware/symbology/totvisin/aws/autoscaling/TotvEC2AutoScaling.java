package com.etradingsoftware.symbology.totvisin.aws.autoscaling;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.autoscaling.model.AutoScalingInstanceDetails;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingInstancesResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.ec2.CuckooManASG;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.ec2.EC2AutoScaling;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.ec2.GhostASG;
import com.etradingsoftware.symbology.totvisin.aws.autoscaling.ec2.TotoASG;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class TotvEC2AutoScaling {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvEC2AutoScaling.class);

    private static final long DESCRIBE_INSTANCE_INTERVAL = 5000;

    private Config config;

    private AmazonAutoScaling amazonAutoScalingClient;
    private AmazonEC2 ec2Client;
    private CuckooManASG cuckooManASG;
    private GhostASG ghostASG;
    private TotoASG totoASG;

    private Set<String> autoScalingGroupNames;

    public TotvEC2AutoScaling(Config config) {
        this.config = config;

        amazonAutoScalingClient = AmazonAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        ec2Client = AmazonEC2ClientBuilder.standard().withRegion(config.getAwsRegion()).build();

        cuckooManASG = new CuckooManASG(amazonAutoScalingClient, config);
        ghostASG = new GhostASG(amazonAutoScalingClient, config);
        totoASG = new TotoASG(amazonAutoScalingClient, config);

        autoScalingGroupNames = new HashSet<>();
        autoScalingGroupNames.add(config.getAutoscalingCuckooManGroupName());
        autoScalingGroupNames.add(config.getAutoscalingGhostGroupName());
        autoScalingGroupNames.add(config.getAutoscalingTotoGroupName());
    }

    public void create() {
        createLaunchConfig(cuckooManASG, config.getAutoscalingCuckooManGroupName());
        createLaunchConfig(ghostASG, config.getAutoscalingGhostGroupName());
        createLaunchConfig(totoASG, config.getAutoscalingTotoGroupName());

        createAutoScalingGroup(totoASG, config.getAutoscalingTotoGroupName());
        createAutoScalingGroup(ghostASG, config.getAutoscalingGhostGroupName());
        createAutoScalingGroup(cuckooManASG, config.getAutoscalingCuckooManGroupName());

        int instanceCount = 0;
        while (instanceCount == 0) {
            HelperUtil.sleep(DESCRIBE_INSTANCE_INTERVAL);

            instanceCount = 0;
            for (AutoScalingInstanceDetails instanceDetails : amazonAutoScalingClient.describeAutoScalingInstances().getAutoScalingInstances()) {
                if (autoScalingGroupNames.contains(instanceDetails.getAutoScalingGroupName())) {
                    instanceCount++;
                }
            }

            LOGGER.info("* Waiting for at least 1 AutoScaling EC2 instance...");
        }

        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, ec2Client);
    }

    public void delete() {
        LOGGER.info("Terminate EC2 Auto-scaling...");
        disableProtectionFromScaleIn(config.getAutoscalingCuckooManGroupName());
        disableProtectionFromScaleIn(config.getAutoscalingGhostGroupName());
        disableProtectionFromScaleIn(config.getAutoscalingTotoGroupName());

        zeroOutAutoScalingGroupDesiredCapacity(config.getAutoscalingTotoGroupName());
        zeroOutAutoScalingGroupDesiredCapacity(config.getAutoscalingGhostGroupName());
        zeroOutAutoScalingGroupDesiredCapacity(config.getAutoscalingCuckooManGroupName());

        int instanceCount = amazonAutoScalingClient.describeAutoScalingInstances().getAutoScalingInstances().size();
        while (instanceCount > 0) {
            HelperUtil.sleep(DESCRIBE_INSTANCE_INTERVAL);

            instanceCount = 0;
            for (AutoScalingInstanceDetails instanceDetails : amazonAutoScalingClient.describeAutoScalingInstances().getAutoScalingInstances()) {
                if (autoScalingGroupNames.contains(instanceDetails.getAutoScalingGroupName())) {
                    instanceCount++;
                }
            }

            instanceCount = amazonAutoScalingClient.describeAutoScalingInstances().getAutoScalingInstances().size();
            LOGGER.info("Waiting for all AutoScaling EC2 instances to be terminated (Current instance count: {})...", instanceCount);
        }

        deleteAutoScalingGroup(config.getAutoscalingTotoGroupName());
        deleteAutoScalingGroup(config.getAutoscalingGhostGroupName());
        deleteAutoScalingGroup(config.getAutoscalingCuckooManGroupName());

        HelperUtil.sleep(5000);

        deleteLaunchConfig(config.getAutoscalingTotoConfigName());
        deleteLaunchConfig(config.getAutoscalingGhostConfigName());
        deleteLaunchConfig(config.getAutoscalingCuckooManConfigName());

        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, ec2Client);
    }

    public void list() {
        LOGGER.info("Listing EC2 Auto-scaling...");
        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, ec2Client);
    }

    private void createLaunchConfig(EC2AutoScaling ec2AutoScaling, String name) {
        try {
            LOGGER.info("Creating LaunchConfiguration for {}...", name);
            ec2AutoScaling.createLaunchConfig();
        } catch (Exception e) {
            LOGGER.warn("Failed creating LaunchConfiguration for {} - {}", name, e.getMessage());
        }
    }

    private void createAutoScalingGroup(EC2AutoScaling ec2AutoScaling, String name) {
        try {
            LOGGER.info("Creating AutoScalingGroup for {}...", name);
            ec2AutoScaling.createAutoScalingGroup();
        } catch (Exception e) {
            LOGGER.warn("Failed creating AutoScalingGroup for {} - {}", name, e.getMessage());
        }
    }

    private void deleteLaunchConfig(String name) {
        try {
            LOGGER.info("Deleting LaunchConfiguration {}...", name);
            AwsAutoScalingUtil.deleteLaunchConfiguration(name, amazonAutoScalingClient);
        } catch (Exception e) {
            LOGGER.warn("Failed deleting LaunchConfiguration {} - {}", name, e.getMessage());
        }
    }

    private void deleteAutoScalingGroup(String name) {
        try {
            LOGGER.info("Deleting AutoScalingGroup {}...", name);
            AwsAutoScalingUtil.deleteAutoScalingGroup(name, amazonAutoScalingClient);
        } catch (Exception e) {
            LOGGER.warn("Failed deleting AutoScalingGroup {} - {}", name, e.getMessage());
        }
    }

    private void disableProtectionFromScaleIn(String autoScalingGroupName) {
        try {
            LOGGER.info("Listing AutoScalingGroup instances...");
            DescribeAutoScalingInstancesResult autoScalingInstancesResult = amazonAutoScalingClient.describeAutoScalingInstances();
            for (AutoScalingInstanceDetails instanceDetails : autoScalingInstancesResult.getAutoScalingInstances()) {
                if (autoScalingGroupName.equals(instanceDetails.getAutoScalingGroupName())
                        && instanceDetails.getProtectedFromScaleIn()
                        ) {
                    LOGGER.info("Disabling AutoScaling instance protection for for AutoScalingGroup {}, InstanceID: {}..."
                            , instanceDetails.getInstanceId()
                            , instanceDetails.getAutoScalingGroupName()
                    );
                    try {
                        AwsAutoScalingUtil.setAutoScalingInstanceProtection(
                                instanceDetails.getAutoScalingGroupName()
                                , instanceDetails.getInstanceId()
                                , false
                                , amazonAutoScalingClient
                        );
                    } catch (Exception e) {
                        LOGGER.warn("Failed setting AutoScaling instance protection for AutoScalingGroup {}, InstanceID: {} - {}"
                                , instanceDetails.getAutoScalingGroupName()
                                , instanceDetails.getInstanceId()
                                , e.getMessage()
                        );
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Failed listing AutoScalingGroup instances - {}", e.getMessage());
        }
    }

    private void zeroOutAutoScalingGroupDesiredCapacity(String autoScalingGroupName) {
        try {
            LOGGER.info("Setting DesiredCapacity to 0 for AutoScalingGroup {}...", autoScalingGroupName);
            AwsAutoScalingUtil.setAutoScalingGroupDesiredCapacity(autoScalingGroupName, 0, amazonAutoScalingClient);
        } catch (Exception e) {
            LOGGER.warn("Failed setting DesiredCapacity for AutoScalingGroup {} - {}", autoScalingGroupName, e.getMessage());
        }
    }

}
