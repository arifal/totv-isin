Project: totv-isin

Modules:
    totv-setup          Setup Totv EC2 and DynamoDB Autoscaling, etc...
    totv-cuckoo-man     Cuckoo Man
    totv-ghost          Ghost
    totv-toto           Toto

Test this weekend
    11k records
    5 ghosts
    max 100 wcu
    set backdown to WCU 1
dsb-isin 1-100

TODO:
x 0. fix errors
x 1. eastify the west
2. initial-deployment to east

improvements
    1. Write to lastUpdate if finished (TOTO)
    2. New role for terminator
    3. [MIFID] Hostname on log file
    4. [MIFID] log to file
    5. [MIFID] Remove AWS credentials on config
    6. [

testing
    1. Append YYYY-mm-dd on pending and completed dirs
    2. ghost process time, toto process time
3. implement toto
    same as ghost
    check it has already run
    lock file
    write completed on efs with date / use dynamoDB for this
4.
    a. add retry on fail (ghost)
    b. move to failed if it failed twice
5. fix server
    add new topic names
    1470=100
    https://github.com/ANNA-DSB/FIX/tree/ToTV/docs
6. fix client
    update topics

TODO-NEW
1. Make Ghost retrieve a new task after the current task finished. Exit if there are no tasks anymore.
2. Last toto run: don't run on twice on the same day
    Find a way to detect toto has failed/success

=====

2017-10-05 13:29:46,087 INFO  thread=pool-1-thread-1 - c.e.s.t.c.u.AwsAutoscalingUtil - EC2 Instance: i-0f5ddc32516ccaab4  10.240.8.202     HEALTHY    Pending       ami-3ed6e745  t2.micro  ETS-Dev  [{GroupName: sgInfrastructure,GroupId: sg-d5825ca5}]  subnet-9287f6c8  vpc-8da1ebf4  Thu Oct 05 13:29:40 CST 2017  []
2017-10-05 13:29:46,088 INFO  thread=pool-1-thread-1 - c.e.s.t.c.u.AwsAutoscalingUtil - EC2 Instance: i-02e7afe28f373ae91  10.240.11.21     HEALTHY    Pending       ami-3ed6e745  t2.micro  ETS-Dev  [{GroupName: sgInfrastructure,GroupId: sg-d5825ca5}]  subnet-9287f6c8  vpc-8da1ebf4  Thu Oct 05 13:29:40 CST 2017  []
2017-10-05 13:29:46,089 INFO  thread=pool-1-thread-1 - c.e.s.t.c.u.AwsAutoscalingUtil - EC2 Instance: i-0b7d64f00d2a62f4f  10.240.5.159     HEALTHY    Pending       ami-3ed6e745  t2.micro  ETS-Dev  [{GroupName: sgInfrastructure,GroupId: sg-d5825ca5}]  subnet-24bba018  vpc-8da1ebf4  Thu Oct 05 13:29:40 CST 2017  []

=====

Kafka Producer / Consumer Test
------------------------------
./kafka-console-consumer.sh --bootstrap-server 10.240.8.209:9092,10.240.6.251:9092,10.240.11.102:9092 --from-beginning --whitelist [a-zA-Z0-9].*
./kafka-console-producer.sh --broker-list 10.240.8.209:9092,10.240.6.251:9092,10.240.11.102:9092 --topic test-2


- Got up to a point from starting 3 cuckoo-man
- Spawning of ghosts, running its tasks and inserting records in the DynamoDB

- Issues:
    - Too many open files on Cuckoo-man
    - Cyra to handle ProvisionException due to throttling


===============================
weekend
===============================
improvements
- cuckooman
    - when 0 tasks, don't spawn ghost -> "Spawning 0 ghosts"

issues
- ghost ran long enough to the point toto was not run because the scheduled time has passed
    - toto has not run
        - however, when toto was run manually, messages on kafka can be seen. So I can safely assume that when toto was run by
          autoscaling then it should run as expected
- ghost faling due to autoscaling does not seem to work
- ghost stopped abruptly:
    2017-10-08 00:25:47,779 INFO  thread=main - c.e.s.t.c.u.ProcessUtil - Input: 00:25:47.778 [main] INFO  c.e.m.t.c.s.DSBService - Successfully saved isin EZ3MRXFHMSN4

# Kill cuckoo-man and ghost apps
#for i in 10.240.4.42 10.240.6.202 10.240.11.114 10.240.8.119 ; do ssh -i ~/ETSDev.pem ec2-user@$i ps -ef | grep 'java' > tmpfile && while read name pid1 pid2 x ; do sudo kill -9 $pid1 && sudo kill -9 $pid2 ; done < tmpfile ; done

2017-10-09 17:29:54,792 ERROR thread=main - c.e.s.t.d.TotvGhostDB - Exception encountered - Subscriber limit exceeded: Provisioned throughput can be decreased 4 times within the same UTC day. Once exceeded, an additional decrease of 1 can be performed every 4 hours. Number of decreases today: 4. Last decrease time: Monday, October 9, 2017 7:44:07 AM UTC. Request time: Monday, October 9, 2017 9:29:54 AM UTC (Service: AmazonDynamoDBv2; Status Code: 400; Error Code: LimitExceededException; Request ID: GV8U4M2EG7ARFRFH68ULPCVSTJVV4KQNSO5AEMVJF66Q9ASUAAJG)
com.amazonaws.services.dynamodbv2.model.LimitExceededException: Subscriber limit exceeded: Provisioned throughput can be decreased 4 times within
 the same UTC day. Once exceeded, an additional decrease of 1 can be performed every 4 hours. Number of decreases today: 4. Last decrease time: Monday, October 9, 2017 7:44:07 AM UTC. Request time: Monday, October 9, 2017 9:29:54 AM UTC (Service: AmazonDynamoDBv2; Status Code: 400; Error Code: LimitExceededException; Request ID: GV8U4M2EG7ARFRFH68ULPCVSTJVV4KQNSO5AEMVJF66Q9ASUAAJG)


- download all files uat
    max wcu = 600
    max rcu = 600

if its not filedownload, how do we split

    cuckooman will look to FIRDS data every few mins
        x Ghost will do this
    create tasks for the day
        - for ESMA

        ssh -i ~/ETSDev.pem ec2-user@10.240.8.209
        cd /opt/kafka/bin
        ./kafka-console-consumer.sh --zookeeper 10.240.11.102:2181,10.240.6.251:2181,10.240.8.209:2181/kafka --whitelist "^(Rates|Credit|Foreign_Exchange|Equity|Commodities|Other|Mixed_Asset|Loan_Lease|Repurchase_Agreements|Securities_Lending|Call_Options|Put_Options|Financial|test-4).*" > ~/subscription-.txt

9am CET and cuckooman will stop to search for new task until later but instead will start toto
and will let toto to extract information into the uncle joe into kafka
as soon as all the extraction has happened and toto has compelted everything on the other side of the kafka
we will need the listener for the kafka so this is simultaneously for toto that will take everything from kafka and push to search engine
we may go with cloud search and we may need perf test
and once all of that has completed CM will see that toto has completed his work
we will need to run once again CM to validate that states have changed from the last update

we need another prob another toto to validate isin sent to kafka
and maybe do it twice to be sure

and once all of that is compelted, CM will again will start to look for files in ESMA
now the fact that we  updating the DB all the time raise the concern, when to backup the DB
I expect the DB to standstill once we compeltee for a day and be able to back it up from another region

it looks to me this is not the case and may need ongoing replication on the DR
and this is availablea apparrently

we have to think carefully what eactly is happening incase a region fails

1000 ghosts
6000 wcu / rcu
80 provision

https://aws.amazon.com/ec2/spot/


[‎10/‎17/‎2017 4:00 PM]  Yuval Cohen:
No Title
wget	https://firds.azureedge.net/firds/FULINS_C_20171015_01of01.zip
wget	https://firds.azureedge.net/firds/FULINS_D_20171015_01of02.zip


[‎10/‎17/‎2017 4:01 PM]  Yuval Cohen:
https://stackoverflow.com/questions/1823264/quickest-way-to-convert-xml-to-json-in-java

curl -g -G "https://registers.esma.europa.eu/solr/esma_registers_firds_files/select" --data-urlencode "q=*" --data-urlencode "wt=csv" --data-urlencode "rows=10000"

cuckooman



Generic Task
1. Curl csv from URL
2. Parse csv to get links
3. Download a link
    4. Unzip file
    5. Convert xml to json
    6. Zip it


Fatal Exception

---------------

new todos:
    ResourceNotFoundException = retry?
    3





    daily starting wed
    kafka 24/7
    push to  lion
    every 5 mins cucukooman check ESMA for records


    daily basis once a day
        list of RM MTF ESMA
        FIRDS

    Limits

PendingTaskGenerator
    DownloadFileList
    ParseFileList
        AnnaDsbFileListParser
        EsmaFileListParser
    CreateTasks

task
    PendingTaskGenerator
parser.filelist
    FileList
    AnnaDsbFileDownloadParser
    EsmaReferenceDataFilesParser



======================

why 1 ghost took 45 hours
rerun with 20%





==================

#1 perf test ebs and efs
    terminate db
    totvGhostDB (deleteTables())
    create db
    start 1 cuckoo-man
    run test in ebs

10.240.7.36


=============================

2017-10-27

Daily Task:
    - Finish EFS perf testing of ToTV
        - Compare perf diff vs local disk and EFS
            How long?
            # of processed records
    - Run task today

Todo:
    Cuckoo-man
        - [ESMA FiRDS] Skip creating task for FULINS if DLTINS exists
        - Move unprocessed files from previous days to today's directory before starting a new task at 9am CET


run full ins of october followed from
DEV env
    October 15 FULINS
    October 16+ DELT = tomorrow




===================================

2017-11-06
To implement:
    - date table - update with last processed date
        from toto?
    - rename application-dev.properties and application.properties to totv-{module_name}.properties
    - refactor

Daily Rep
    - Sent ToTV FIRDS Perf Test via email
    - Will update Toto to change query implementation from single page query to query all pages
    - Q: I'm using the WCU and RCU of the totv-tables. Is this also the same values will used during testing this weekend?


Todo:
    - rerun on fresh db
    - run on delta

==================================

2017-11-15

==========

workerMain
workerKafka
workerLastUpdate

TotvProcessorApp : 1
TotoProcessor : 1                           queryByPage on LastUpdate
    lastUpdateService (for querying)
    worker
    recordBatchHandler : 1
        queryFromLastUpdate(m)
        recordHandler.push(m)
RecordBatchHandler
    worker
    cfiWriters
    :totvGenerator
    totoKafkaProducer
    lastUpdateDao
    underlyingDao
        pollFromQueue(m)
        foreach
            recordProcessor(x)
RecordProcessor
    generateTotv(many threads)
TotvRecordGenerator

===================

TotvProcessorApp
TotoProcessor.process()
    RecordBatchHandler
    LastUpdateService (query)
    mainWorker (recordBatchHandler)
    rbh.push()
RecordBatchHandler
    IsinDataRetriever
    mainWorker(isinDataRetriever)
    ? CFIWriters
    ? TotoKafkaProducer
    idr.push()
IsinDataRetriever
    isinDataProcessor
    dsbIsinDao
    firdsRefDataDao
    isinDataRetrieverWorker(per item)
    isinDataRetrieverTasksWorker(per sub)
    idp.push()
IsinDataProcessor
    totoKafkaProducer
    totvGenerator
totoKafkaProducer
    totvPostProcessor
totvPostProcessor
    lastUpdateBatcWriter
    underlyingBatchWriter
    cfiWriters

    @Value("${record_batch_handler.queue_size}")
    private int queueSize;

    @Value("${isin_data_retriever.queue_size}")
    private int queueSize;

    @Value("${isin_data_retriever.threads}")
    private int threadCount;

    @Value("${isin_data_processor.queue_size}")
    private int queueSize;

    @Value("${isin_data_processor.threads}")
    private int threadCount;

    @Value("${kafka_producer.queue_size}")
    private int queueSize;

    @Value("${kafka_producer.threads}")
    private int threadCount;

    @Value("${totv_post_processor.queue_size}")
    private int queueSize;






    @Value("${worker.thread_queue_size:10000}")
    private int workerThreadQueueSize;

    @Value("${worker.threads:100}")
    private int workerThreads;

    @Value("${record_batch_handler.queue_size:20000}")
    private int recordBatchHandlerQueueSize;

    @Value("${isin_data_retriever.queue_size:10000}")
    private int isinDataRetrieverQueueSize;

    @Value("${isin_data_retriever.threads:50}")
    private int isinDataRetrieverThreadCount;

    @Value("${isin_data_processor.queue_size:10000}")
    private int isinDataProcessorQueueSize;

    @Value("${isin_data_processor.threads:50}")
    private int isinDataProcessorThreadCount;

    @Value("${kafka_producer.queue_size:10000}")
    private int kafkaProducerQueueSize;

    @Value("${kafka_producer.threads:50}")
    private int kafkaProducerThreadCount;

    @Value("${totv_post_processor.queue_size:10000}")
    private int totvPostProcessorQueueSize;

    @Value("${totv_post_processor.threads:50}")
    private int totvPostProcessorThreadCount;

    @Value("${cfi_record_writer.queue_size:10000}")
    private int cfiRecordWriterQueueSize;














worker.thread_queue_size=10000
worker.threads=100
record_batch_handler.queue_size=20000
isin_data_retriever.queue_size=10000
isin_data_retriever.threads=50
isin_data_processor.queue_size=10000
isin_data_processor.threads=50
kafka_producer.queue_size=10000
kafka_producer.threads=50
totv_post_processor.queue_size=10000
totv_post_processor.threads=50
cfi_record_writer.queue_size=10000
