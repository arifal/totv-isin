/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.filelist.impl;

import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.util.DownloadUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.filelist.FileListProcessor;
import com.etradingsoftware.symbology.totvisin.model.FileListSource;
import com.etradingsoftware.symbology.totvisin.model.PendingTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EsmaFileListProcessor implements FileListProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(EsmaFileListProcessor.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String CSV_PATTERN = "(?<timestamp>\\S+),(?<id>\\S+),(?<version>\\S+),(?<downloadLink>\\S+),(?<root>\\S+),(?<publishedInstrumentFileId>\\S+),(?<fileName>\\S+),(?<publicationDate>\\S+),(?<fileType>\\S+)";
    private static final String DOWNLOAD_LINK = "downloadLink";
    private static final String FILE_NAME = "fileName";

    private Config config;
    private DateTimeFormatter dateFormatter;
    private Pattern csvPattern;

    public EsmaFileListProcessor(Config config) {
        this.config = config;
        this.dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        this.csvPattern = Pattern.compile(CSV_PATTERN);
    }

    @Override
    public String download(LocalDate publicationDate) throws IOException {
        String publicationDateStr = MessageFormat.format("&fq=publication_date:%5B{0}T00:00:00Z+TO+{0}T23:59:59Z%5D", publicationDate.format(dateFormatter));
        String downloadPath = MessageFormat.format("{0}{1}", config.getEsmaFileDownloadUri(), publicationDateStr);

        // Download tasks from file download URI
        LOGGER.info("Downloading file list from {}...", downloadPath);
        return DownloadUtil.getUriContent(downloadPath);
    }

    @Override
    public List<PendingTask> parse(String fileList, LocalDate publicationDate) {
        List<PendingTask> pendingTasks = new ArrayList<>();

        Matcher m = csvPattern.matcher(fileList);
        int rowNum = 0;
        while (m.find()) {
            if (rowNum == 0) {
                // Skip header
                ++rowNum;
                continue;
            }

            String downloadUri = m.group(DOWNLOAD_LINK);
            String taskFileName = getTaskFileName(m.group(FILE_NAME));
            pendingTasks.add(new PendingTask(FileListSource.ESMA, downloadUri, taskFileName));
        }

        return pendingTasks;
    }

    private String getTaskFileName(String archiveFile) {
        return archiveFile.replace(".zip", ".task");
    }

    public static void main(String[] args) throws ConfigurationException, IOException {
        Config config = new Config("src/main/resources/conf/totv-cuckoo-man-dev.properties");
        EsmaFileListProcessor proc = new EsmaFileListProcessor(config);
        String content = proc.download(LocalDate.now().minusDays(2));
        List<PendingTask> pendingTasks = proc.parse(content, LocalDate.now());
        System.out.println("Content: " + content);
        for (PendingTask pt : pendingTasks) {
            System.out.println("Parse: " + pt);
        }
    }

}
