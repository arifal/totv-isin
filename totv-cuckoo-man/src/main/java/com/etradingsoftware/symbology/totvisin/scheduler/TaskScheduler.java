/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TaskScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskScheduler.class);

    private static final String UTC = "UTC";

    private String taskName;
    private LocalTime startTime;
    private LocalTime endTime;
    private int intervalInMinutes;
    private int startTimeOffsetInMinutes;
    private int offsetIndex;

    private ZonedDateTime nextRun;

    public TaskScheduler(String taskName, LocalTime startTime, LocalTime endTime, int intervalInMinutes, int startTimeOffsetInMinutes, int offsetIndex) {
        this.taskName = taskName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.intervalInMinutes = intervalInMinutes;
        this.startTimeOffsetInMinutes = startTimeOffsetInMinutes;
        this.offsetIndex = offsetIndex;

        LOGGER.info("[{}] taskName: {}", taskName, taskName);
        LOGGER.info("[{}] startTime: {}", taskName, startTime);
        LOGGER.info("[{}] endTime: {}", taskName, endTime);
        LOGGER.info("[{}] intervalInMinutes: {}", taskName, intervalInMinutes);
        LOGGER.info("[{}] startTimeOffsetInMinutes: {}", taskName, startTimeOffsetInMinutes);
        LOGGER.info("[{}] offsetIndex: {}", taskName, offsetIndex);
    }

    public boolean withinTaskTime() {
        ZonedDateTime startDateTime = ZonedDateTime.of(ZonedDateTime.now().toLocalDate(), startTime, ZoneId.of(UTC));
        ZonedDateTime endDateTime = ZonedDateTime.of(ZonedDateTime.now().toLocalDate(), endTime, ZoneId.of(UTC));
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of(UTC));

        return now.isAfter(startDateTime) && now.isBefore(endDateTime);
    }

    public boolean shouldRunTask() {
        ZonedDateTime startDateTime = ZonedDateTime.of(ZonedDateTime.now().toLocalDate(), startTime, ZoneId.of(UTC));
        ZonedDateTime endDateTime = ZonedDateTime.of(ZonedDateTime.now().toLocalDate(), endTime, ZoneId.of(UTC));
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of(UTC));

        if (now.isAfter(endDateTime)) {
            startDateTime = startDateTime.plusDays(1);
            endDateTime = endDateTime.plusDays(1);
            nextRun = startDateTime.plusMinutes(startTimeOffsetInMinutes * offsetIndex);
        }

        if (nextRun == null) {
            if (now.isBefore(startDateTime)) {
                // Run at scheduled time
                nextRun = startDateTime.plusMinutes(startTimeOffsetInMinutes * offsetIndex);
            } else {
                // Run immediately as possible
                nextRun = now.plusMinutes(startTimeOffsetInMinutes * offsetIndex);
            }
        }

        if (now.isBefore(startDateTime)) {
            LOGGER.info("[{}] Current time is not within processing time ({} - {}). Next Run will be at {}", taskName, startDateTime, endDateTime, nextRun);
            return false;
        }

        if (now.isEqual(nextRun) || now.isAfter(nextRun)) {
            nextRun = now.plusMinutes(intervalInMinutes);
            LOGGER.info("[{}] Executing process. NextRun will be at {}", taskName, nextRun);
            return true;
        } else {
            LOGGER.info("[{}] Next process will be at {}", taskName, nextRun);
            return false;
        }
    }

}
