/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum AssetClass {

    RATES("Rates", "Rates")
    , CREDIT("Credit", "Credit")
    , FOREIGN_EXCHANGE("Foreign_Exchange", "Foreign Exchange")
    , EQUITY("Equity", "Equity")
    , COMMODITIES("Commodities", "Commodities")
    ;

    private String name;
    private String description;

    AssetClass(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "AssetClass{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public static List<String> getNames() {
        return Arrays.stream(AssetClass.values()).map(AssetClass::getName).collect(Collectors.toList());
    }

}
