/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.filelist.impl;

import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.util.DownloadUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.filelist.FileListProcessor;
import com.etradingsoftware.symbology.totvisin.model.AssetClass;
import com.etradingsoftware.symbology.totvisin.model.FileListSource;
import com.etradingsoftware.symbology.totvisin.model.PendingTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class AnnaDsbFileListProcessor implements FileListProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnnaDsbFileListProcessor.class);

    private static final String DATE_FORMAT = "yyyyMMdd";

    private Config config;
    private DateTimeFormatter dateFormatter;

    public AnnaDsbFileListProcessor(Config config) {
        this.config = config;
        this.dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
    }

    @Override
    public String download(LocalDate publicationDate) throws IOException {
        String downloadPath = MessageFormat.format("{0}/{1}", config.getAnnaDsbFileDownloadUri(), getFormattedPublicationDate(publicationDate));

        // Download tasks from file download URI
        LOGGER.info("Downloading file list from {}...", downloadPath);
        return DownloadUtil.getUriContent(downloadPath, config.getCordraUsername(), config.getCordraPassword());
    }

    @Override
    public List<PendingTask> parse(String fileList, LocalDate publicationDate) {
        List<PendingTask> pendingTasks = new ArrayList<>();
        String formattedPublicationDate = getFormattedPublicationDate(publicationDate);

        for (String assetClass : AssetClass.getNames()) {
            if (fileList.contains(assetClass)) {
                String downloadUri = getDownloadUriPath(assetClass, formattedPublicationDate);
                String taskFileName = getTaskFileName(assetClass, formattedPublicationDate);
                pendingTasks.add(new PendingTask(FileListSource.ANNA_DSB, downloadUri, taskFileName));
            }
        }

        return pendingTasks;
    }

    private String getDownloadUriPath(String assetClass, String formattedPublicationDate) {
        return MessageFormat.format(
                "{0}/{1}/{2}/{2}-{1}.records"
                , config.getAnnaDsbFileDownloadUri()
                , formattedPublicationDate
                , assetClass
        );
    }

    private String getTaskFileName(String assetClass, String formattedPublicationDate) {
        return MessageFormat.format("{0}-{1}.task", formattedPublicationDate, assetClass);
    }

    private String getFormattedPublicationDate(LocalDate publicationDate) {
        return publicationDate.minusDays(1).format(dateFormatter);
    }

    public static void main(String[] args) throws ConfigurationException, IOException {
        Config config = new Config("src/main/resources/conf/totv-cuckoo-man-dev.properties");
        AnnaDsbFileListProcessor proc = new AnnaDsbFileListProcessor(config);
        String content = proc.download(LocalDate.now());
        List<PendingTask> pendingTasks = proc.parse(content, LocalDate.now());
        System.out.println("Content: " + content);
        for (PendingTask pt : pendingTasks) {
            System.out.println("Parse: " + pt);
        }
    }

}
