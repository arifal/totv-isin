/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.filelist;

import com.etradingsoftware.symbology.totvisin.model.PendingTask;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface FileListProcessor {

    String download(LocalDate publicationDate) throws IOException;

    List<PendingTask> parse(String fileList, LocalDate publicationDate);

}
