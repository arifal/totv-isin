/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.task;

import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.model.TotvApp;
import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.common.util.TotvUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.filelist.FileListProcessor;
import com.etradingsoftware.symbology.totvisin.filelist.impl.AnnaDsbFileListProcessor;
import com.etradingsoftware.symbology.totvisin.filelist.impl.EsmaFileListProcessor;
import com.etradingsoftware.symbology.totvisin.model.FileListSource;
import com.etradingsoftware.symbology.totvisin.model.PendingTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.List;

public class PendingTaskGenerator implements TotvTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(PendingTaskGenerator.class);

    private Config config;
    private FileListSource source;
    private FileListProcessor processor;

    public PendingTaskGenerator(Config config, FileListSource source, FileListProcessor processor) {
        this.config = config;
        this.source = source;
        this.processor = processor;
    }

    @Override
    public void execute() {
        try {
            LOGGER.info("[{}] Task started.", source.getName());

            LocalDate publicationDate = LocalDate.now();

            // Download file list from source
            String fileList = processor.download(publicationDate);

            // Parse file list result into PendingTask objects
            List<PendingTask> pendingTasks = processor.parse(fileList, publicationDate);

            // Create pending task files based on PendingTask objects
            createPendingTaskFiles(pendingTasks);
        } catch (Exception e) {
            LOGGER.error("Exception occurred - {}", e.getMessage(), e);
        } finally {
            LOGGER.info("[{}] Task ended.", source.getName());
        }
    }

    private void createPendingTaskFiles(List<PendingTask> pendingTasks) throws IOException {
        // Create task files inside pending task directory
        for (PendingTask pendingTask : pendingTasks) {
            String pendingTaskPath = getPendingTaskPath(pendingTask.getTaskFileName());

            boolean fileAlreadyExists = FileUtil.isExists(pendingTaskPath)
                    || FileUtil.isExists(getCompletedTaskPath(pendingTask.getTaskFileName()))
                    || FileUtil.isExists(getFailedTaskPath(pendingTask.getTaskFileName()));

            if (!fileAlreadyExists) {
                LOGGER.info("Creating new task file: {} (PendingTask: {})", pendingTaskPath, pendingTask);
                FileUtil.writeToFile(pendingTask.getDownloadUri(), pendingTaskPath);
            } else {
                LOGGER.info("Task file {} already exists. Skipping... (PendingTask: {})", pendingTaskPath, pendingTask);
            }
        }

    }

    private String getPendingTaskPath(String taskFileName) {
        return MessageFormat.format("{0}/{1}", TotvUtil.getDataPendingDir(TotvApp.GHOST, config.getTotvPath()), taskFileName);
    }

    private String getCompletedTaskPath(String taskFileName) {
        return MessageFormat.format("{0}/{1}", TotvUtil.getDataCompletedDir(TotvApp.GHOST, config.getTotvPath()), taskFileName);
    }

    private String getFailedTaskPath(String taskFileName) {
        return MessageFormat.format("{0}/{1}", TotvUtil.getDataFailedDir(TotvApp.GHOST, config.getTotvPath()), taskFileName);
    }

    public static void main(String[] args) throws ConfigurationException {
        Config config = new Config("src/main/resources/conf/totv-cuckoo-man-dev.properties");
        PendingTaskGenerator gen1 = new PendingTaskGenerator(config, FileListSource.ANNA_DSB, new AnnaDsbFileListProcessor(config));
        PendingTaskGenerator gen2 = new PendingTaskGenerator(config, FileListSource.ESMA, new EsmaFileListProcessor(config));
        gen1.execute();
        gen2.execute();
    }

}
