/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.config;

import com.etradingsoftware.symbology.totvisin.common.config.TotvIsinConfig;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config extends TotvIsinConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    private static final String AWS_REGION = "aws.region";
    private static final String ANNA_DSB_FILE_DOWNLOAD_URI = "anna_dsb.file_download_uri";
    private static final String ESMA_FILE_DOWNLOAD_URI = "esma.file_download_uri";
    private static final String CORDRA_USERNAME = "cordra.username";
    private static final String CORDRA_PASSWORD = "cordra.password";
    private static final String TOTV_PATH = "totv.path";
    private static final String PROCESS_GHOST_START_TIME = "process.ghost.start_time";
    private static final String PROCESS_GHOST_END_TIME = "process.ghost.end_time";
    private static final String PROCESS_TOTO_START_TIME = "process.toto.start_time";
    private static final String PROCESS_TOTO_END_TIME = "process.toto.end_time";
    private static final String PROCESS_INTERVAL_MINUTES = "process.interval.minutes";
    private static final String PROCESS_START_TIME_OFFSET_MINUTES = "process.start_time_offset.minutes";
    private static final String AUTOSCALING_GHOST_GROUP_NAME = "autoscaling.ghost.group_name";
    private static final String AUTOSCALING_TOTO_GROUP_NAME = "autoscaling.toto.group_name";
    private static final String DEV_DATESTR_OVERRIDE = "dev.datestr.override";
    private static final String DEV_PERFORM_TASK_IMMEDIATELY = "dev.perform_task_immediately";

    static {
        addString(AWS_REGION, true);
        addString(ANNA_DSB_FILE_DOWNLOAD_URI, true);
        addString(ESMA_FILE_DOWNLOAD_URI, true);
        addString(CORDRA_USERNAME, true);
        addString(CORDRA_PASSWORD, true);
        addString(TOTV_PATH, true);
        addString(PROCESS_GHOST_START_TIME, true);
        addString(PROCESS_GHOST_END_TIME, true);
        addString(PROCESS_TOTO_START_TIME, true);
        addString(PROCESS_TOTO_END_TIME, true);
        addInt(PROCESS_INTERVAL_MINUTES, true);
        addInt(PROCESS_START_TIME_OFFSET_MINUTES, true);
        addString(AUTOSCALING_GHOST_GROUP_NAME, true);
        addString(AUTOSCALING_TOTO_GROUP_NAME, true);
        addString(DEV_DATESTR_OVERRIDE, false);
        addBoolean(DEV_PERFORM_TASK_IMMEDIATELY, false);
    }

    public Config(String configPath) throws ConfigurationException {
        LOGGER.info("Loading application config file: {}", configPath);
        loadFromConfigFile(configPath);
    }

    public String getAwsRegion() {
        return getString(AWS_REGION);
    }

    public String getAnnaDsbFileDownloadUri() {
        return getString(ANNA_DSB_FILE_DOWNLOAD_URI);
    }

    public String getEsmaFileDownloadUri() {
        return getString(ESMA_FILE_DOWNLOAD_URI);
    }

    public String getCordraUsername() {
        return getString(CORDRA_USERNAME);
    }

    public String getCordraPassword() {
        return getString(CORDRA_PASSWORD);
    }

    public String getTotvPath() {
        return getString(TOTV_PATH);
    }

    public String getProcessGhostStartTime() {
        return getString(PROCESS_GHOST_START_TIME);
    }

    public String getProcessGhostEndTime() {
        return getString(PROCESS_GHOST_END_TIME);
    }

    public String getProcessTotoStartTime() {
        return getString(PROCESS_TOTO_START_TIME);
    }

    public String getProcessTotoEndTime() {
        return getString(PROCESS_TOTO_END_TIME);
    }

    public Integer getProcessIntervalMinutes() {
        return getInt(PROCESS_INTERVAL_MINUTES);
    }

    public Integer getProcessStartTimeOffsetMinutes() {
        return getInt(PROCESS_START_TIME_OFFSET_MINUTES);
    }

    public String getAutoscalingGhostGroupName() {
        return getString(AUTOSCALING_GHOST_GROUP_NAME);
    }

    public String getAutoscalingTotoGroupName() {
        return getString(AUTOSCALING_TOTO_GROUP_NAME);
    }

    public String getDevDatestrOverride() {
        return getString(DEV_DATESTR_OVERRIDE);
    }

    public Boolean getDevPerformTaskImmediately() {
        return getBoolean(DEV_PERFORM_TASK_IMMEDIATELY);
    }

}
