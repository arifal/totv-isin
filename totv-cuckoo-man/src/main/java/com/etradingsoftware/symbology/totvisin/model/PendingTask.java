/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.model;

public class PendingTask {

    private FileListSource source;
    private String downloadUri;
    private String taskFileName;

    public PendingTask(FileListSource source, String downloadUri, String taskFileName) {
        this.source = source;
        this.downloadUri = downloadUri;
        this.taskFileName = taskFileName;
    }

    public FileListSource getSource() {
        return source;
    }

    public String getDownloadUri() {
        return downloadUri;
    }

    public String getTaskFileName() {
        return taskFileName;
    }

    @Override
    public String toString() {
        return "PendingTask{" +
                "source=" + source +
                ", downloadUri='" + downloadUri + '\'' +
                ", taskFileName='" + taskFileName + '\'' +
                '}';
    }

}
