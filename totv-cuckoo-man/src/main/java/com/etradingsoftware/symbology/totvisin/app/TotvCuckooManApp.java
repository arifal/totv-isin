/*******************************************************************************
 *
 * Copyright © 2017 The OTC ISIN Engine Company Limited.
 * All Rights Reserved.
 * No part of this document may be photocopied, reproduced, stored in a retrieval system, or transmitted,
 * in any form or by any means whether, electronic, mechanical,
 * or otherwise without the prior written permission of The OTC ISIN Engine Company Limited.
 * No warranty of accuracy is given concerning the contents of the information contained in this publication.
 *
 *******************************************************************************/
package com.etradingsoftware.symbology.totvisin.app;

import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClientBuilder;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.etradingsoftware.symbology.totvisin.common.exception.ConfigurationException;
import com.etradingsoftware.symbology.totvisin.common.lock.LockFile;
import com.etradingsoftware.symbology.totvisin.common.model.TotvApp;
import com.etradingsoftware.symbology.totvisin.common.util.AwsAutoScalingUtil;
import com.etradingsoftware.symbology.totvisin.common.util.FileUtil;
import com.etradingsoftware.symbology.totvisin.common.util.HelperUtil;
import com.etradingsoftware.symbology.totvisin.common.util.TotvUtil;
import com.etradingsoftware.symbology.totvisin.config.Config;
import com.etradingsoftware.symbology.totvisin.filelist.impl.AnnaDsbFileListProcessor;
import com.etradingsoftware.symbology.totvisin.filelist.impl.EsmaFileListProcessor;
import com.etradingsoftware.symbology.totvisin.model.FileListSource;
import com.etradingsoftware.symbology.totvisin.scheduler.TaskScheduler;
import com.etradingsoftware.symbology.totvisin.task.PendingTaskGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TotvCuckooManApp implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TotvCuckooManApp.class);

    private static final String LOCK_FILENAME = "totv-cuckooman-";

    private static final long TASK_INTERVAL = 60000;
    private static final long QUERY_SERVER_INFO_DELAY = 5000;
    private static final long TOTO_RUN_CHECK_INTERVAL = 1000;

    private Config config;
    private AmazonAutoScaling amazonAutoScalingClient;
    private AmazonEC2 amazonEc2Client;
    private LockFile lockFile;

    private int instanceIndex;
    private TaskScheduler ghostTaskScheduler;
    private TaskScheduler totoTaskScheduler;
    private List<PendingTaskGenerator> pendingTaskGenerators;

    private LocalDate lastTotoRun;

    private boolean isDevMode;

    public TotvCuckooManApp(String configPath) throws ConfigurationException {
        config = new Config(configPath);
        amazonAutoScalingClient = AmazonAutoScalingClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        amazonEc2Client = AmazonEC2ClientBuilder.standard().withRegion(config.getAwsRegion()).build();
        lockFile = new LockFile();

        // TODO: Dev mode only / Pre-production code
        isDevMode = config.getDevPerformTaskImmediately() != null && config.getDevPerformTaskImmediately();
    }

    private void init() {
        // Retrieve machine hostname for logging
        String hostname = "UnknownHost";
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) {
            /* do nothing */
        }
        System.setProperty("hostname", hostname);

        // Acquire instance index thru file-locking
        instanceIndex = getInstanceIndex();

        // Schedule tasks
        this.ghostTaskScheduler = new TaskScheduler(
                "Ghost"
                , HelperUtil.toLocalTime(config.getProcessGhostStartTime())
                , HelperUtil.toLocalTime(config.getProcessGhostEndTime())
                , config.getProcessIntervalMinutes()
                , config.getProcessStartTimeOffsetMinutes()
                , instanceIndex
        );
        this.totoTaskScheduler = new TaskScheduler(
                "Toto"
                , HelperUtil.toLocalTime(config.getProcessTotoStartTime())
                , HelperUtil.toLocalTime(config.getProcessTotoEndTime())
                , config.getProcessIntervalMinutes()
                , config.getProcessStartTimeOffsetMinutes()
                , instanceIndex
        );

        // Initialize file list processors
        pendingTaskGenerators.add(new PendingTaskGenerator(config, FileListSource.ANNA_DSB, new AnnaDsbFileListProcessor(config)));
        pendingTaskGenerators.add(new PendingTaskGenerator(config, FileListSource.ESMA, new EsmaFileListProcessor(config)));
    }

    public void run() {
        init();

        try {
            while (true) {
                try {
                    if (isDevMode) {
                        initiateGhostTask();
                        initiateTotoTask();
                        break; // Run-once only for dev mode
                    } else {
                        if (ghostTaskScheduler.shouldRunTask()) {
                            initiateGhostTask();
                        }
                        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
                        if (lastTotoRun == null || now.toLocalDate().isAfter(lastTotoRun)) {
                            if (totoTaskScheduler.shouldRunTask()) {
                                lastTotoRun = now.toLocalDate();
                                initiateTotoTask();
                            }
                        } else {
                            LOGGER.info("Toto has already run for today. Next run is day after {}", now.toLocalDate());
                        }
                    }

                    HelperUtil.sleep(TASK_INTERVAL);
                } catch (Exception e) {
                    LOGGER.error("Exception encountered - {}", e.getMessage(), e);
                }
            }
        } finally {
            LOGGER.info("Finished.");
            if (lockFile != null) {
                lockFile.releaseLock();
            }
        }
    }

    private void initiateGhostTask() throws IOException {
        LOGGER.info("Initiating operation for Ghosts...");

        // Generate tasks for each generator
        for (PendingTaskGenerator generator : pendingTaskGenerators) {
            generator.execute();
        }

        // Spawn ghost servers based on number of pending tasks
        spawnGhostServers();

        LOGGER.info("Waiting for {} seconds before retrieving status of the instances...", QUERY_SERVER_INFO_DELAY / 1000);
        HelperUtil.sleep(QUERY_SERVER_INFO_DELAY);

        // Display EC2 instances info
        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, amazonEc2Client);

        LOGGER.info("Ghost initiation complete.");
    }

    private void initiateTotoTask() throws IOException {
        LOGGER.info("Initiating operation for Toto...");

        boolean isDevMode = config.getDevPerformTaskImmediately() != null && config.getDevPerformTaskImmediately(); // @TODO: Dev mode
        while (totoTaskScheduler.withinTaskTime() || isDevMode) {
            LOGGER.info("Waiting for all Ghost tasks to be completed...");
            if (areGhostTasksFinished()) {
                // Create completed directory, as needed
                FileUtil.makeDirs(TotvUtil.getDataCompletedDir(TotvApp.GHOST, config.getTotvPath()));

                int completedTaskSize = FileUtil.listFiles(TotvUtil.getDataCompletedDir(TotvApp.GHOST, config.getTotvPath())).size();
                int failedTaskSize  = FileUtil.listFiles(TotvUtil.getDataFailedDir(TotvApp.GHOST, config.getTotvPath())).size();
                LOGGER.info("All ghosts tasks are finished. (Total: {}, Successful: {}, Failed: {}", completedTaskSize + failedTaskSize, completedTaskSize, failedTaskSize);

                if (completedTaskSize > 0) {
                    int spawnDelay = instanceIndex * config.getProcessStartTimeOffsetMinutes() * 60000;
                    if (spawnDelay > 0) {
                        HelperUtil.sleep(spawnDelay);
                    }

                    // Spawn ghost servers based on number of generated tasks
                    spawnTotoServer();
                    break;
                }
            }
            HelperUtil.sleep(TOTO_RUN_CHECK_INTERVAL);
        }

        LOGGER.info("Waiting for {} seconds before retrieving status of the instances...", QUERY_SERVER_INFO_DELAY / 1000);
        HelperUtil.sleep(QUERY_SERVER_INFO_DELAY);

        // Display EC2 instances info
        AwsAutoScalingUtil.displayInstancesInfo(amazonAutoScalingClient, amazonEc2Client);

        LOGGER.info("Toto initiation complete.");
    }

    /**
     * Spawn ghost servers based on number of tasks
     */
    private void spawnGhostServers() throws IOException {
        AutoScalingGroup ghostASG = AwsAutoScalingUtil.describeAutoScalingGroup(config.getAutoscalingGhostGroupName(), amazonAutoScalingClient);

        int taskCount = getPendingTasksCount();
        int ghostCount = ghostASG.getDesiredCapacity();

        // Spawn ghost servers as needed
        if (taskCount > ghostCount) {
            int totalNumGhostsThatShouldBeRunning = Math.min(taskCount, ghostASG.getMaxSize());

            if (totalNumGhostsThatShouldBeRunning != ghostASG.getDesiredCapacity()) {
                LOGGER.info("Spawning {} new {} servers... # of Ghosts: (current) {} -> (new) {}", totalNumGhostsThatShouldBeRunning - ghostCount, TotvApp.GHOST.getName(), ghostCount, totalNumGhostsThatShouldBeRunning);
                AwsAutoScalingUtil.setAutoScalingGroupDesiredCapacity(config.getAutoscalingGhostGroupName(), totalNumGhostsThatShouldBeRunning, amazonAutoScalingClient);
            } else {
                LOGGER.info("No new Ghosts will be spawned. (Total tasks: {}, Ghosts (current/max): {} / {})", taskCount, ghostCount, ghostASG.getMaxSize());
            }
        } else {
            LOGGER.info("No new Ghosts will be spawned. (Total tasks: {}, Ghosts (current/max): {} / {})", taskCount, ghostCount, ghostASG.getMaxSize());
        }
    }

    /**
     * Spawn toto servers based on number of tasks
     */
    private void spawnTotoServer() {
        // Spawn ghost server
        LOGGER.info("Spawning {} server...", TotvApp.TOTO.getName());
        AutoScalingGroup totoAutoScalingGroup = AwsAutoScalingUtil.describeAutoScalingGroup(config.getAutoscalingTotoGroupName(), amazonAutoScalingClient);
        if (totoAutoScalingGroup.getDesiredCapacity() != 1) {
            // We only want to set desired capacity if its not set already
            AwsAutoScalingUtil.setAutoScalingGroupDesiredCapacity(config.getAutoscalingTotoGroupName(), 1, amazonAutoScalingClient);
        }
    }

    private boolean areGhostTasksFinished() throws IOException {
        return getPendingTasksCount() == 0;
    }

    private int getPendingTasksCount() throws IOException {
        // Create pending directory, as needed
        FileUtil.makeDirs(TotvUtil.getDataPendingDir(TotvApp.GHOST, config.getTotvPath()));

        return FileUtil.listFiles(TotvUtil.getDataPendingDir(TotvApp.GHOST, config.getTotvPath())).size();
    }

    private String getInstanceLockPath(String filenameToLock) {
        return MessageFormat.format("{0}/{1}.lck", TotvUtil.getLockDir(TotvApp.CUCKOO_MAN, config.getTotvPath()), filenameToLock);
    }

    /**
     * Get Cuckoo-man's server instance index by locking a file
     */
    private int getInstanceIndex() {
        boolean isLockAcquired = false;

        int fileIndex = -1;
        while (!isLockAcquired) {
            String taskLockPath = getInstanceLockPath(MessageFormat.format("{0}{1,number,00}", LOCK_FILENAME, ++fileIndex));
            isLockAcquired = lockFile.acquireLock(taskLockPath);
        }

        LOGGER.info("Using instance index: {}", fileIndex);
        return fileIndex;
    }

    public static void main(String[] args) throws ConfigurationException, IOException {
        if (args.length < 1) {
            System.out.println("Illegal number of arguments. Usage: <configFile>");
            System.exit(1);
        }

        ExecutorService service = null;
        try {
            String configPath = args[0];

            TotvCuckooManApp app = new TotvCuckooManApp(configPath);
            service = Executors.newSingleThreadExecutor();
            service.submit(app);
        } catch (Exception e) {
            LOGGER.error("Exception encountered - {}", e.getMessage(), e);
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }

}
